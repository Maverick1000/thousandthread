//
//  MapKitViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/12/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import MapKit

class MapKitViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    var location: CLLocation!
    var isRegionSet = false
    let locationManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkLocationAuthorizationStatus()
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            setupMapView()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    private func setupMapView(){
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        mapView.showsUserLocation = true
    }
    
    private func setRegion(){
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude,
                                            longitude: location!.coordinate.longitude)
        let rect = MKCoordinateRegion(center: center, latitudinalMeters: 80 * 1609.34, longitudinalMeters: 80 * 1609.34)
        mapView.setRegion(rect, animated: false)
    }
    
    private func updateOverlay(){
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude,
                               longitude: location!.coordinate.longitude)
        let circle = MKCircle(center: center, radius: 40 * 1609.34)
        mapView.addOverlay(circle)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension MapKitViewController:MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let circleOverlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: circleOverlay)
            circleRenderer.fillColor = UIColor.red
            circleRenderer.alpha = 0.1
            
            return circleRenderer
        }
        return MKPolylineRenderer()
    }
}

extension MapKitViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied) {
            // The user denied authorization
        } else if (status == CLAuthorizationStatus.authorizedWhenInUse) {
             setupMapView()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.location = locations.last //as CLLocation
        mapView.removeOverlays(mapView.overlays)
//        updateOverlay()
        if !isRegionSet {
            isRegionSet = true
            setRegion()
        }
    }
    
}
