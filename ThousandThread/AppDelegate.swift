//
//  AppDelegate.swift
//  ThousandThread
//
//  Created by Paul Walker on 12/31/18.
//  Copyright © 2018 Paul Walker. All rights reserved.
//

import UIKit
import RestKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
        RKlcl_configure_by_name("RestKit/Network", RKlcl_vTrace.rawValue);
        RKlcl_configure_by_name("RestKit/ObjectMapping", RKlcl_vTrace.rawValue);
        
        IQKeyboardManager.shared.enable = true
        
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(ChatMessageViewController.self)       //LoginController.shared.updateLoginStatus(status: false, profileID: 0)
        
//        let myImage = UIImage(named: "Back");
//        UIBarButtonItem.appearance().setBackButtonBackgroundImage(myImage, for: [], barMetrics: .default);
//
       // UIImage *backButtonImage = [[UIImage imageNamed:@"button_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 6)];
        //[[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        //LoginController.shared.updateLoginStatus(status: false, profileID: 1)
        Utility.cleanUpDocumentImageDir()
        
        if LoginController.shared.isLogin(){
            LoginController.shared.getUserProfile { (sccuess) in
                if sccuess{
                    SocketIOManager.shared.establishConnection()
                    self.window?.rootViewController = Utility.requestViewController(identifier: "MainTabViewController", storyborad: "HomeNav")
                    //SocketIOManager.shared.socket.emit("user_id", LoginController.shared.curretSigInUser.requestUserID())
                }else{
                    self.window?.rootViewController = Utility.requestViewController(identifier: "navController", storyborad: "LoginNav")
                }
            }
        }else{
            self.window?.rootViewController = Utility.requestViewController(identifier: "navController", storyborad: "LoginNav")
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("applicationDidEnterBackground")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("applicationWillEnterForeground")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("applicationWillEnterForeground")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("applicationWillTerminate")
        SocketIOManager.shared.closeConnection()
    }
    
    public func presentOnTopWindow(viewController:UIViewController){
        var topController = UIApplication.shared.keyWindow?.rootViewController
        while ((topController?.presentedViewController) != nil) {
            topController = topController?.presentedViewController;
        }
        topController?.present(viewController, animated: true, completion: nil)
    }
    
    public func MainViewAnimation() {
        let storyBoard = UIStoryboard.init(name: "HomeNav", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MainTabViewController")
        UIView.transition(with: self.window!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.window?.rootViewController = viewController
            self.window?.makeKeyAndVisible()
        }, completion:nil)
    }
    
    public func logOutAnimation() {
        let storyBoard = UIStoryboard.init(name: "LoginNav", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "navController")
        UIView.transition(with: self.window!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.window?.rootViewController = viewController
            self.window?.makeKeyAndVisible()
        }, completion: nil)
    }
    
}

