//
//  Constant.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/13/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class Constant {
    static let shared = Constant()
    
    var searchRequest = SearchRequest()
    
    let kUseToken = true
    let kBaseURL = "https://thousandthread.io:3010"
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let kHelveticBaseFont  = "Helvetica"
    
    let kPasswordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{8,10}"
    let kEmailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let kTagPasswordField = 1000
    
    let kMinCharacterCount = 8
    let kMaxCharacterCount = 10
    
    let kLowwerAndUpperCharacter = "^(?=.*[a-z])(?=.*[A-Z])"
    let kOneNumberAndOneSpecialCharacter = "^(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]" //1 Number and 1 Special Character
    let kPasswordRulesViewControllerSegue = "passwordRulesViewController"
    
    
    let kPortraitmage = "portraitmage"
    
    let kUSerID = "USerID"
    let kIsLogin = "isLogin"
    
    var kgenderParamaters = [String]()
    var kcategory = [String]()
    var ksize = [String]()
    var kshoesize = [String]()
    var kpantSize = [String]()
    
    let kTagBaseColor = UIColor(red: 244.0/255.0, green: 182.0/255.0, blue: 145.0/255.0, alpha: 1.0)
    let kUnSelctedTabColor = UIColor(red: 204.0/255.0, green: 202.0/255.0, blue: 200.0/255.0, alpha: 1.0)
    let kCatagoryColor = UIColor(red: 0, green: 12.0/255.0, blue: 142.0/255.0, alpha: 1.0)
    
    let kImagePathFolderName = "ImagePost"
    
    let kSize  = ["XS","S","M","L","XL","XXL"]
    let kCategory = ["Tops","Bottoms","Dresses","Blouse","Shorts","Sweater","T-shirts","Jeans","OuterWear","Skirts","Shoes"]
    let ShoesSize = ["4","4.5/5","5.5/6","6.5/7","7.5/8","8.5/9","9.5/10","10.5/11","11.5/12","12.5/13","13.5/14","14+"]
    let kPantsSize  = ["XS","S","M","L","XL","XXL","0","2","4","6","8","10","12","14","16"]
    
    let TAG_MALE_FEMALE = 0
    let TAG_CATEGORY = 1
    let TAG_SIZE = 2
    let TAG_SHOE_SIZE = 3
    let TAG_PANTS_SIZE = 4
    
    let TAG_REQUEST = 0
    let TAG_CHAT = 1
    let TAG_MONEYP_TRAS = 2
    let TAG_FRIEND_REQUEST = 3
    let TAG_USER_ALL_ITEMS = 4
    let TAG_FRIENDS_CLOSET = 5
    
    let TAG_CURRENTEXCHANGES = 0
    let TAG_EXCHANGEHISTORY = 1
    let TAG_BALANCE = 3
    let TAG_PROMOS = 4
    let TAG_SETTINGS = 5
    let TAG_HELP = 7
    let TAG_LOGOUT = 8
    
    let kMessage_Chat = "chat"
    let kMessage_AD = "ad"
    let kMessage_Friend = "friend"
    
    let kAction_RentRequest = "rentRequest"
    let kAction_rentAccepted = "rentAccepted"
    let kAction_borrowRequest = "borrowRequest"
    let kAction_borrowAccepted = "borrowAccepted"
    let kAction_friendRequest = "friendRequest"
    let kAction_friendAccepted = "friendAccepted"
    
    
    let kPostIncomingMessage = NSNotification.Name("PostIncomingMessage")
}

struct ChatMessage{
    var profileName:String!
    var profileImageName:String!
    var message:String!
    var howOldMessage:String!
}

@objc class RealTimeMessge:NSObject{
    var id:NSNumber!
    var sender_id:NSNumber!
    var recipient_id:NSNumber!
    var ad_id:NSNumber!
    var body:String?
    var type:String!
    var message_set_id:NSNumber!
    var imagePaths:[String]!
}

struct ProfileItemCell{
    var imagePath:String!
    var title:String!
    var priceRent:String!
    var retailPrice:String!
    var size:String!
}

struct Requests{
    var profileImageString:String!
    var requestMessage:String!
}




