//
//  CapturedConfidence.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/25/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import UIKit

struct CapturedConfidence {
    let rate: CGFloat = 0.5
    var rectangle: CGFloat = 0.0
    var face: CGFloat = 0.0
    var focus: CGFloat = 0.0
    var text: CGFloat = 0.0
    var barcode: CGFloat = 0.0
    
    var value: CGFloat {
        return min(rectangle, face, barcode)
    }
    
    mutating func reset() {
        rectangle = 0
        face = 0
        focus = 0
        text = 0
        barcode = 0
    }
    
    mutating func updateRectangle(_ improved: Bool) {
        rectangle = max(min(rectangle + (improved ? +rate : -rate), 1.0), 0.0)
    }
    
    mutating func updateFace(_ improved: Bool) {
        face = max(min(face + (improved ? +rate : -rate), 1.0), 0.0)
    }
    
    mutating func updateFocus(_ improved: Bool) {
        focus = max(min(focus + (improved ? +rate : -rate), 1.0), 0.0)
    }
    
    mutating func updateText(_ improved: Bool) {
        text = max(min(text + (improved ? +rate : -rate), 1.0), 0.0)
    }
    
    mutating func updateBarcode(_ improved: Bool) {
        barcode = max(min(barcode + (improved ? +rate : -rate), 1.0), 0.0)
    }
    
}

