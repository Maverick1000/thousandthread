//
// Copyright 2014 Scott Logic
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit
import GLKit
import AVFoundation
import CoreMedia
import CoreImage
import OpenGLES
import QuartzCore

class CoreImageVideoFilter: NSObject {
    
    var applyFilter: ((CIImage) -> Void)? = nil
    var stillPhotoCallback: ((CIImage?) -> Void)? = nil
    var videoPreview: UIView!
    var videoDisplayViewBounds: CGRect!
    var renderContext: CIContext!
    
    var avSession: AVCaptureSession?
    var sessionQueue: DispatchQueue!
    var photoOutput: AVCapturePhotoOutput!
    var videoOutput: AVCaptureVideoDataOutput!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var detector: CIDetector?
    var cameraPosition: AVCaptureDevice.Position = .unspecified
    var shouldBarcodeRead: Bool = false
    var barcodeValue: String? = nil
    var shouldFilter: BoolDelay = BoolDelay(true)
    
    // MARK: - Life Cycle
    
    required  init (preview: UIView, position: AVCaptureDevice.Position, canBarcodeRead: Bool = false, applyFilterCallback: ((CIImage) -> Void)?) {
        cameraPosition = position
        applyFilter = applyFilterCallback
        videoPreview = preview
        shouldBarcodeRead = canBarcodeRead
        photoOutput = AVCapturePhotoOutput()
        videoOutput = AVCaptureVideoDataOutput()
        sessionQueue = DispatchQueue(label: "AVSessionQueue", attributes: [])
    }
    
    deinit {
        if let session = avSession {
            for input in session.inputs {
                session.removeInput(input)
            }
            
            for output in session.outputs {
                session.removeOutput(output)
            }
        }
        
        stopFiltering()
        avSession = nil
        applyFilter = nil
        sessionQueue = nil
    }
    
    
    // MARK: - Helper Methods
    
    func startFiltering() {
        // Create a session if we don't already have one
        if avSession == nil {
            do {
                avSession = try createAVSession()
            } catch {
                print(error)
            }
        }
        
        // And kick it off
        sessionQueue.async {
            self.avSession?.startRunning()
        }
    }
    
    func stopFiltering() {
        // Stop the av session
        avSession?.stopRunning()
        
        // Remove all AVCaptureMetadataOutput for barcode
        for metadataOutput in avSession?.outputs ?? [] {
            if metadataOutput is AVCaptureMetadataOutput {
                avSession?.removeOutput(metadataOutput)
            }
        }
    }
    
    func createAVSession() throws -> AVCaptureSession {
        // Start session
        let session = AVCaptureSession()
        session.sessionPreset = (cameraPosition == .back ? AVCaptureSession.Preset.hd1920x1080 : AVCaptureSession.Preset.medium)
        
        let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: cameraPosition)!
        if device.isTorchAvailable {
            try device.lockForConfiguration()
            device.torchMode = .off
            device.focusMode = .continuousAutoFocus
            device.unlockForConfiguration()
        }
        
        
        // Input from video camera
        let input = try AVCaptureDeviceInput(device: device)
        session.addInput(input)
        
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.frame = videoPreview.bounds
        videoPreview.layer.addSublayer(previewLayer)
        
        // Output
        session.addOutput(photoOutput)
        
        videoOutput.videoSettings = [ kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
        videoOutput.alwaysDiscardsLateVideoFrames = true
        videoOutput.setSampleBufferDelegate(self, queue: sessionQueue)
        session.addOutput(videoOutput)
        
        // Clear previous BarCode value and add again AVCaptureMetadataOutput for barcode
        barcodeValue = nil
        if shouldBarcodeRead {
            let metadataOutput = AVCaptureMetadataOutput()
            if session.canAddOutput(metadataOutput) {
                session.addOutput(metadataOutput)
                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [.pdf417]
            }
        }
        
        
        return session
    }
    
    @discardableResult
    func flash(mode: AVCaptureDevice.TorchMode) -> Bool {
        let videoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDualCamera], mediaType: AVMediaType.video, position: .unspecified)
        guard let device = videoDeviceDiscoverySession.devices.first else { return false }
        guard device.hasMediaType(.video) && device.hasTorch else { return false }
        
        self.avSession?.beginConfiguration()
        defer { self.avSession?.commitConfiguration() }
        
        do {
            try device.lockForConfiguration()
            device.torchMode = mode
            device.unlockForConfiguration()
            return true
        } catch {
            return false
        }
    }
    
    func isAdjustingFocus() -> Bool? {
        let videoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDualCamera], mediaType: AVMediaType.video, position: .unspecified)
        guard let device = videoDeviceDiscoverySession.devices.first else { return nil }
        return device.isAdjustingFocus
    }
    
    func captureStillPhoto(_ callback: @escaping (_ image: CIImage?) -> Void) {
        if let photoOutputConnection = photoOutput.connection(with: .video) {
            photoOutputConnection.videoOrientation = .landscapeLeft
        }
        
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]
        
        settings.flashMode = .off
        settings.isAutoStillImageStabilizationEnabled = true
        settings.previewPhotoFormat = previewFormat
        photoOutput.capturePhoto(with: settings, delegate: self)
        stillPhotoCallback = callback
    }
}


// MARK: - AVCaptureVideoDataOutputSampleBuffer Delegate
extension CoreImageVideoFilter: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ captureOutput: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        guard shouldFilter.value else { return }
        shouldFilter.set(value: false)
        shouldFilter.set(value: true, afterDelay: 0.4)
        
        
        // Orientation Portrait
        connection.videoOrientation = AVCaptureVideoOrientation.portrait
        if cameraPosition == .front {
            connection.isVideoMirrored = true
        }
        
        // Need to shimmy this through type-hell
        let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        // Force the type change - pass through opaque buffer
        let opaqueBuffer = Unmanaged<CVImageBuffer>.passUnretained(imageBuffer!).toOpaque()
        let pixelBuffer = Unmanaged<CVPixelBuffer>.fromOpaque(opaqueBuffer).takeUnretainedValue()
        
        let sourceImage = CIImage(cvPixelBuffer: pixelBuffer, options: nil)
        applyFilter?(sourceImage)
    }
}


// MARK: - AVCaptureMetadataOutputObjects Delegate
extension CoreImageVideoFilter: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard self.barcodeValue == nil else { return }
        guard let metadataObject = metadataObjects.first else { return }
        guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
        self.barcodeValue = readableObject.stringValue
    }
}


// MARK: - AVCapturePhotoCapture Delegate
extension CoreImageVideoFilter: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard error == nil,
            let photoCallback = stillPhotoCallback,
            let imageData = photo.fileDataRepresentation(),
            let image = CIImage(data: imageData)
            else {
                print("error occur: \(error?.localizedDescription ?? "")")
                self.stillPhotoCallback?(nil)
                self.stillPhotoCallback = nil
                return
        }
        
        let imagePortrait = image.oriented(.right)
        photoCallback(imagePortrait)
        stillPhotoCallback = nil
    }
}
