//
//  Utility.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/7/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class Utility{
    
    static var imageIndex = 0
    
    static let successStatusCode = RKStatusCodeIndexSetForClass(.successful)
    static let clientStatusCode = RKStatusCodeIndexSetForClass(.clientError)
    
    static func requestViewController(identifier:String, storyborad:String) -> UIViewController{
        let storyboard = UIStoryboard(name: storyborad, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
    
    static func reqeustReviewCommentViewController(pageRowResponse:ItemPageRowResponse) ->ReviewCommentViewController{
        return ReviewCommentViewController(pageRowResponse, nibName: "ReviewCommentViewController", bundle: nil);
    }
    
    static func isTextFieldEmpty(textField:UITextField) ->Bool{
        let isEmpty = textField.text?.isEmpty ?? false
        return isEmpty
    }
    
    static func matches(for regex: String, in text: String) -> [String] {
        do {
            let reg = try NSRegularExpression(pattern: regex)
            let results = reg.matches(in: text,
                                      range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    static func createUserAvailability(fromCell:TimeAddedCell) ->UserAvailability{
        return  UserAvailability.init(day: fromCell.weekDayTextField.text ?? "",
                                      startTime: fromCell.availableStartTimeTextField.text ?? "",
                                      endTime: fromCell.availableEndTimeTextField.text ?? "")
    }
    
    static func roundValue(value:NSNumber, decimalPlace:Int) ->String{
        let numberFromatter = NumberFormatter()
        numberFromatter.maximumFractionDigits = decimalPlace
        return numberFromatter.string(from: value) ?? "0"
    }
    
    static func requestWeekDays() ->[String]{
        return ["Monday","Tesuday","Wednesday","Thrusdays","Friday","Saturday","Sunday"]
    }
    
    static func requestImageDocument() ->URL?{
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsDirectory.appendingPathComponent(Constant.shared.kPortraitmage)
    }
    
    static func requestImageDocument(atPath:String) ->UIImage?{
        let imageData = try!Data(contentsOf: URL(string: atPath)!)
        return UIImage(data: imageData, scale: 0.5)
    }
    
    static  func unavailableDateRange(startDate:Date, endDate:Date) ->String{
        let startDateFormatter = DateFormatter()
        let endDateFormatter = DateFormatter()
        
        startDateFormatter.dateFormat = "MMM d"
        endDateFormatter.dateFormat = "d"
        
        let startDateString = startDateFormatter.string(from: startDate)
        let endDateString = endDateFormatter.string(from: endDate)
        
        return "\(startDateString)-\(endDateString)"
    }
    
    static func requestPostAddImageFilePaths() -> [URL]{
        var filePaths:[URL]!
        let fileManager = FileManager.default
        
        var documentsDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        documentsDirectory.appendPathComponent(Constant.shared.kImagePathFolderName)
        
        do {
            filePaths = try fileManager.contentsOfDirectory(at: documentsDirectory,
                                                            includingPropertiesForKeys: nil, options: [])
        } catch {
            print("Could not clear temp folder: \(error)")
        }
        return filePaths
    }
    
    static func requestImageDocumentPath(atPath:String) ->URL?{
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let imagePath = documentsDirectory.appendingPathComponent(atPath)
        return imagePath
    }
    
    static func loadImage(itemImageView:UIImageView, atPath:String){
        itemImageView.downloaded(from: atPath, size: itemImageView.frame.size) {(success) in
            if success{
                //itemImageView.stopAnimating()
            }
        }
    }
    
    static func loadImage(atPath:String,completionHandler:@escaping(Bool, UIImage?)->()){
        if let url = URL(string: atPath){
            var urlRequest = URLRequest(url: url)
            if Constant.shared.kUseToken {
                urlRequest.httpMethod = "GET"
                if let token = KeyChainManager.shared.getValue(key: "token") {
                    urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                }
            }
            URLSession.shared.dataTask(with: urlRequest) { data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else {return}
                DispatchQueue.main.async() {
                    completionHandler(true,image)
                }
            }.resume()
        }
    }
    
    static func cleanUpDocumentImageDir(){
        let fileManager = FileManager.default
        let documentsDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = documentsDirectory.appendingPathComponent(Constant.shared.kImagePathFolderName)
        do {
            let filePaths = try fileManager.contentsOfDirectory(at: fileURL,
                                                                includingPropertiesForKeys: nil, options: [])
            for filePath in filePaths {
                try fileManager.removeItem(at: filePath)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
    static func resizeImage(image:UIImage, targetSize:CGSize) -> UIImage? {
        let originalSize = image.size
        
        let widthRatio = targetSize.width / originalSize.width
        let heightRatio = targetSize.height / originalSize.height
        
        let ratio = min(widthRatio, heightRatio)
        
        let newSize = CGSize(width: originalSize.width * ratio, height: originalSize.height * ratio)
        
        // preparing rect for new image size
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        image.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}

extension Date {
    struct Formatter {
        static let utcFormatter: DateFormatter = {
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss'Z'"
            dateFormatter.timeZone = TimeZone(identifier: "GMT")
            
            return dateFormatter
        }()
        
        static let localFormatter: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mma"
            return dateFormatter
        }()
        
        static let localHourFormatter: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hha"
            return dateFormatter
        }()
    }
    
    var dateToUTC: String {
        return Formatter.utcFormatter.string(from: self)
    }
    
    var localHour:String{
        return Formatter.localHourFormatter.string(from: self)
    }
    
    var localTime: String {
        return Formatter.localFormatter.string(from: self)
    }
}

extension String {
    struct Formatter {
        static let utcFormatter: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssz"
            
            return dateFormatter
        }()
    }
    
    var dateFromUTC: Date? {
        return Formatter.utcFormatter.date(from: self)
    }
}

extension UIImage {
    
    class func from(ciImage: CIImage?) -> UIImage? {
        guard let ciImage = ciImage else { return nil }
        var image = UIImage(ciImage: ciImage)
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, 2)
        defer { UIGraphicsEndImageContext() }
        UIImage(ciImage: ciImage).draw(in: CGRect(origin: .zero, size: image.size))
        guard let redraw = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        return redraw
    }
    
    func saveToDocuments(filename:String, ext:String) {
        let fileManager  = FileManager.default
        let documentsDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        var fileURL = documentsDirectory.appendingPathComponent(Constant.shared.kImagePathFolderName)
        do {
            try fileManager.createDirectory(atPath: fileURL.path,
                                            withIntermediateDirectories: true, attributes: nil)
        } catch {
            print("error create Directory:", error)
        }
        
        fileURL = fileURL.appendingPathComponent(filename).appendingPathExtension(ext)
        
        if let data = self.jpegData(compressionQuality: 1.0) {
            do {
                try data.write(to: fileURL)
            } catch {
                print("error saving file to documents:", error)
            }
        }
    }
    
    func crop(cropRect:CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(cropRect.size, false, self.scale)
        let origin = CGPoint(x: cropRect.origin.x * CGFloat(-1), y: cropRect.origin.y * CGFloat(-1))
        self.draw(at: origin)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        return result
    }
    
    func squareImage(newSize:CGSize) -> UIImage{
        var ratio:CGFloat!
        var delta:CGFloat!
        var offset:CGPoint!
        
        let sz = CGSize(width: newSize.width, height: newSize.height)
        if (size.width > size.height) {
            ratio = newSize.width / size.width;
            delta = (ratio * size.width - ratio * size.height);
            offset = CGPoint(x: delta/2, y: 0);
        }
        else{
            ratio = newSize.width / size.height;
            delta = (ratio * size.height - ratio * size.width);
            offset = CGPoint(x: 0, y: delta/2);
        }
        
        let clipRect = CGRect(x:-offset.x, y:-offset.y,
                              width:(ratio * size.width) + delta,
                              height:(ratio * size.height) + delta);
        
        UIGraphicsBeginImageContextWithOptions(sz, true, 0.0);
        UIRectClip(clipRect);
        draw(in: clipRect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return newImage!;
    }
    
    func rotatedImageWithTransform(_ rotation: CGAffineTransform, croppedToRect rect: CGRect) -> UIImage {
        let rotatedImage = rotatedImageWithTransform(rotation)
        
        let scale = rotatedImage.scale
        let cropRect = rect.applying(CGAffineTransform(scaleX: scale, y: scale))
        
        let croppedImage = rotatedImage.cgImage?.cropping(to: cropRect)
        let image = UIImage(cgImage: croppedImage!, scale: self.scale, orientation: rotatedImage.imageOrientation)
        return image
    }
    
    fileprivate func rotatedImageWithTransform(_ transform: CGAffineTransform) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, true, scale)
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: size.width / 2.0, y: size.height / 2.0)
        context?.concatenate(transform)
        context?.translateBy(x: size.width / -2.0, y: size.height / -2.0)
        draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rotatedImage!
    }
}

extension UIImageView {
    func downloaded(from url: URL, size:CGSize, contentMode mode: UIView.ContentMode = .scaleAspectFit,
                    completionHandler:@escaping(Bool) ->() ){  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        
        var urlRequest = URLRequest(url: url)
        if Constant.shared.kUseToken {
            urlRequest.httpMethod = "GET"
            if let token = KeyChainManager.shared.getValue(key: "token") {
                urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
        }
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                if size == CGSize.zero {
                    self.image = image
                    self.frame.size = CGSize(width: image.size.width, height: image.size.height)
                }else{
                    self.image = image.squareImage(newSize: size)
                }
                completionHandler(true)
            }
            }.resume()
    }
    
    func downloaded(from link: String, size:CGSize, contentMode mode: UIView.ContentMode = .scaleAspectFit,
                    completionHandler:@escaping(Bool) ->()) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, size:size, contentMode: mode, completionHandler:completionHandler)
    }
}
