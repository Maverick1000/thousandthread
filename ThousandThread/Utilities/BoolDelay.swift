//
//  BoolDelay.swift
//  M23
//
//  Created by Syed Atif Jamil on 2/12/18.
//  Copyright © 2018 Chinna Addepally. All rights reserved.
//

import Foundation

/// BoolDelay is wraper object for Bool which can change value after some delay.
/// When new value set then it will cancel previous delay request to keep latest changes
open class BoolDelay: NSObject {
    
    public private(set) var value: Bool
    
    public required init(_ value: Bool = false) {
        self.value = value
    }
    
    @objc private func setInternal(value: NSNumber) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.value = value.boolValue
    }
    
    /// It set bool value. If delay is define then it will set bool value after delay.
    /// Whenever setting the value it cancel previous request to keep latest changes
    ///
    /// - Parameters:
    ///   - value: Bool value
    ///   - delay: if define change bool value after delay
    open func set(value: Bool, afterDelay delay: TimeInterval = 0) {
        if delay == 0 {
            setInternal(value: NSNumber(value: value))
        } else {
            DispatchQueue.main.async {
                NSObject.cancelPreviousPerformRequests(withTarget: self)
                self.perform(#selector(self.setInternal(value:)), with: NSNumber(value: value), afterDelay: delay)
            }
        }
    }
}
