//
//  Protocol.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/2/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit


protocol LocationTimeViewControllerProocol:class {
    func onCellTapGesture(cellIndex:IndexPath)
}

protocol  DatePickerViewItemSelector:class {
    func userSelectedDatePickerItem(selectedItem:Date)
}

protocol CustomPickerViewItemSelector:class {
    func userSelectedPickerItem(selectedItem:String)
}

protocol LandingViewControllerProtocol:class {
    func showReviewAndComentView(selectedIndexPath:IndexPath)
    func showSelectedProfile(selectedIndexPath: IndexPath)
}

protocol PageNavgationProtocol:class {
    func goBack()
}

protocol SearchCollapsedTableViewCellProtocol:class{
    func onCollapsedCell(indexPath:IndexPath)
}

public protocol ImagePickerDelegate: class {
    func didSelect(image: UIImage?)
}

public protocol ProfileItemCollectionViewControllerDelegte:class{
    func requetCurrentSelectedTab() -> Int
}

public protocol PageViewControllerDelegate:class{
    func pageViewControllerHasChange(pageIndex: Int)
}

