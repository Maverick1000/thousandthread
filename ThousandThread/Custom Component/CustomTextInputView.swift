//
//  CustomTextInputView.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/10/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit


class CustomTextInputView : UIView{
    @IBOutlet weak var textInput: UITextField!
    
    @IBInspectable
    var _placeholderText: String! {
        didSet {
            textInput.placeholder = _placeholderText
        }
    }
    
    @IBInspectable
    var placeholderColor: UIColor! {
        didSet {
            textInput.attributedPlaceholder =  NSAttributedString(string: self._placeholderText != nil ?
                    self._placeholderText! : "", attributes:[NSAttributedString.Key.foregroundColor: placeholderColor!])
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public func requetTextField() ->UITextField{
        return textInput
    }
    
    public func setTextFieldDelegate(delegate:UITextFieldDelegate){
        textInput.delegate = delegate
    }
    public func setTextField(tagValue:Int){
        textInput.tag = tagValue
    }
    
    public func isSecureTextEntry(isSecureTextEntry:Bool){
        textInput.isSecureTextEntry = isSecureTextEntry
    }
    
    private func commonInit(){
        
        let view = loadViewFromNib()
        addSubview(view!)
        view?.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: -15))
        addConstraint(NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0))
        
        layer.borderWidth = 1
        layer.cornerRadius = 5
        layer.borderColor = UIColor.black.cgColor
        
        addInputAccessoryView()
    }
    
    private func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomTextInputView", bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
    private func addInputAccessoryView(){
        let toolBar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 35))
        
        let doneToolBarItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneWithTextInput))
        doneToolBarItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name:Constant.shared.kHelveticBaseFont, size: 14) as Any,
                                                NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        
        doneToolBarItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name:Constant.shared.kHelveticBaseFont, size: 14) as Any,
                                                NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
        
        toolBar.barStyle = .default
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                         doneToolBarItem]
        toolBar.sizeToFit()
        
        textInput.inputAccessoryView = toolBar
        textInput.keyboardDistanceFromTextField = 10
    }
    
    @objc func doneWithTextInput() {
        textInput.resignFirstResponder()
    }
    
    private func setSecureTextEntry(){
        textInput.isSecureTextEntry = true
    }
    
    public func isValidPassword() ->Bool{
        //Minimum 8 and Maximum 10 characters at least 1 Uppercase Alphabet,
        //1 Lowercase Alphabet, 1 Number and 1 Special Character:
        return NSPredicate(format: "SELF MATCHES %@", Constant.shared.kPasswordRegex).evaluate(with: textInput.text)
    }
    
    public func isValidEmail() -> Bool {
        return NSPredicate(format:"SELF MATCHES %@", Constant.shared.kEmailRegEx).evaluate(with: textInput.text)
    }
    
    public func getPasswordRegxMatches(text:String) -> [String]{
        return Utility.matches(for: ".{8,10}", in: text)
    }
}

