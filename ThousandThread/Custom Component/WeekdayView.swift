//
//  WeekdayView.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/2/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class WeekdayView: UIView {
    @IBOutlet weak var pickerView: UIPickerView!
    
    private var data = [String]()
    private var weekDayDelegate:CustomPickerViewItemSelector?
    
    public func setupData(data:[String]){
        self.data = data
        pickerView.reloadAllComponents()
    }
    
    public func setWeekDayDelegate(delegate:CustomPickerViewItemSelector){
        weekDayDelegate = delegate
    }
}

extension WeekdayView:UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
}

extension WeekdayView:UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        weekDayDelegate?.userSelectedPickerItem(selectedItem: data[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
}
