//
//  TitleView.swift
//  ThousandThread
//
//  Created by Paul Walker on 4/16/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class TitleView: UIView {
    
    @IBOutlet weak var backButton: UIButton!
    
    public var delegate:PageNavgationProtocol!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        let view = loadViewFromNib()
        addSubview(view!)
        view?.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0))
    }
    
    private func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TitleView", bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }

    public func showBackButton(isShow:Bool){
        backButton.isHidden = isShow
    }
    
    @IBAction func onTouchGoBackButton(_ sender: Any) {
         delegate.goBack()
    }
    
}
