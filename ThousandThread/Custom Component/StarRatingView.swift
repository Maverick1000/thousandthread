//
//  StarRatingView.swift
//  ThousandThread
//
//  Created by Paul Walker on 3/31/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class StarRatingView : UIView {
    
    @IBOutlet weak var starStackView: UIStackView!
    @IBOutlet private weak var starRatingImage: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        let view = loadViewFromNib()
        addSubview(view!)
        view?.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0))
        
    }
    
    private func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "StarRatingView", bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
    public func setAlignment(aligment:UIStackView.Alignment){
        starStackView.alignment = aligment
    }
    
    public func setStarRating(starRating:Double){
        var imageRating:UIImage!
        
        switch starRating {
        case 0.0:
            imageRating = UIImage(named: "rating-star0")
        case 0.5:
            imageRating = UIImage(named: "rating-star05")
        case 1.0:
            imageRating = UIImage(named: "rating-star1")
        case 1.5:
            imageRating = UIImage(named: "rating-star15")
        case 2.0:
            imageRating = UIImage(named: "rating-star2")
        case 2.5:
            imageRating = UIImage(named: "rating-star25")
        case 3.0:
            imageRating = UIImage(named: "rating-star3")
        case 3.5:
            imageRating = UIImage(named: "rating-star35")
        case 4.0:
            imageRating = UIImage(named: "rating-star4")
        case 4.5:
            imageRating = UIImage(named: "rating-star45")
        case 5.0:
            imageRating = UIImage(named: "rating-star5")
        default:
            imageRating = UIImage(named: "rating-star0")
        }
        
        starRatingImage.image = imageRating;
    }
}
