//
//  HeaderView.swift
//  ThousandThread
//
//  Created by Paul Walker on 6/20/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class HeaderView: UIView {

    @IBOutlet weak var headerTitle: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
}
