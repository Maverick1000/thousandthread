//
//  DatePickerView.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/6/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class DatePickerView:UIView{
    
    private var datePickerDelegate:DatePickerViewItemSelector?
    
    public func setDatePickerDelegate(delegate:DatePickerViewItemSelector){
        datePickerDelegate = delegate
    }
    
    @IBAction func userSelectedTime(_ sender: Any) {
        let dataPickerView = sender as! UIDatePicker
        print(dataPickerView.date)
        datePickerDelegate?.userSelectedDatePickerItem(selectedItem: dataPickerView.date)
    }
}

