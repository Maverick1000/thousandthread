//
//  AlertController.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/7/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class AlertController{
    static let alertShard = AlertController();
    
    public func showAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        Constant.shared.appDelegate.presentOnTopWindow(viewController: alert)
    }
}
