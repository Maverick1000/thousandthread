//
//  SearchCollapsedTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/6/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class SearchCollapsedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var expandableButton: UIButton!
    
    var indexPath:IndexPath!
    var delegate:SearchCollapsedTableViewCellProtocol!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func userTouchExpanableButton(_ sender: Any) {
        delegate?.onCollapsedCell(indexPath:indexPath)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
