//
//  GroupCheckButton.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/6/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class GroupCheckButton: UIButton {
    
    @IBOutlet var otherButtons: [GroupCheckButton]!
    
    static var groupModifing = false
    
    private var multipleSelectionEnabled:Bool!
    var isMultipleSelectionEnabled:Bool{
        get{
            return multipleSelectionEnabled
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addTarget(self, action: #selector(onUserSelected), for: UIControl.Event.touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addTarget(self, action: #selector(onUserSelected), for: UIControl.Event.touchUpInside)
    }
    
    func setMultipleSelectionEnabled(multipleSelectionEnabled:Bool){
        if GroupCheckButton.groupModifing == false{
            GroupCheckButton.groupModifing = true
            for button in otherButtons{
                button.multipleSelectionEnabled = multipleSelectionEnabled
            }
             GroupCheckButton.groupModifing = false
        }
        self.multipleSelectionEnabled = multipleSelectionEnabled
    }
    
    @objc func onUserSelected(){
        if isMultipleSelectionEnabled{
            setSelected(selected: !self.isSelected)
        }else{
            setSelected(selected: true)
        }
    }
    
    func selectedButton() -> GroupCheckButton?{
        if !isMultipleSelectionEnabled{
            if isSelected{
                return self
            }else{
                for button in otherButtons{
                    if button.isSelected{
                        return button
                    }
                }
            }
        }
        return nil;
    }
    
    func selectedBuittons() -> [GroupCheckButton]{
        var selectedButtons = [GroupCheckButton]()
        if isSelected{
            selectedButtons.append(self)
        }
        for button in otherButtons{
            if button.isSelected{
                selectedButtons.append(button)
            }
        }
        return selectedButtons
    }
    
    func deselectOtherButtons(){
        for button in otherButtons{
            button.isSelected = false
        }
    }
    
    func setSelected(selected:Bool){
        super.isSelected = selected
        if isMultipleSelectionEnabled == false && selected{
            deselectOtherButtons()
        }
    }
}
