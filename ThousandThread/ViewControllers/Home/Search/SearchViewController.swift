//
//  SearchViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/5/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class SearchViewController: BaseViewController {
    
    @IBOutlet weak var searchGoButton: UIButton!
    @IBOutlet weak var searchText: UITextField!
    
    
    @IBOutlet weak var genderGroupCheckButton: GroupCheckButton!
    @IBOutlet weak var categroryGroupCheckButton: GroupCheckButton!
    
    @IBOutlet weak var ShoeSizeExpanderButton: UIButton!
    @IBOutlet weak var pantSizeExpanderButton: UIButton!
    @IBOutlet weak var shoeSizeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var pantSizeViewHeight: NSLayoutConstraint!
    
    private var isShoeSizeViewCollapsed = false
    private var isPantSizeViewCollapsed = false
    private var showSizeActualHeight = CGFloat(0.0)
    private var pantsSizeActualHeight = CGFloat(0.0)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        searchText.layer.borderWidth = 1
        searchText.layer.cornerRadius = 5
        searchText.layer.borderColor = UIColor.black.cgColor
        
        searchGoButton.layer.borderWidth = 1
        searchGoButton.layer.cornerRadius = 5
        searchGoButton.layer.borderColor = UIColor.black.cgColor
        
        showSizeActualHeight = shoeSizeViewHeight.constant
        pantsSizeActualHeight = pantSizeViewHeight.constant
        
        shoeSizeViewHeight.constant = CGFloat(0.0)
        pantSizeViewHeight.constant = CGFloat(0.0)
        
        genderGroupCheckButton.setMultipleSelectionEnabled(multipleSelectionEnabled: false)
        categroryGroupCheckButton.setMultipleSelectionEnabled(multipleSelectionEnabled: true)
    }
    
    @IBAction func DoSearchRequest(_ sender: Any) {
        if let text = searchText.text {
            Constant.shared.searchRequest.seaStr = text
            
            NetworkManger.shared.prefromSearchRequest(searchRequest:Constant.shared.searchRequest )
            { [weak self] (success, itemPageResponse, searchResultError) in
                if success {
                    let VC = Utility.requestViewController(identifier: "ItemCollectionView", storyborad: "HomeNav") as! ItemCollectionViewController
                    VC.shouldSearchResutles = true
                    VC.processSearchReults(pageRespone: itemPageResponse!)
                    self?.navigationController?.pushViewController(VC, animated: true)
                }else{
                    AlertController.alertShard.showAlert(title: "Search Error", message: "No items could be found.")
                }
            }
        }else{
            AlertController.alertShard.showAlert(title: "Search", message: "Please enter some text into search field.")
        }
    }
    
    @IBAction func onShoeSizeExpaneOrCollapsed(_ sender: Any) {
        if isShoeSizeViewCollapsed == false{
            shoeSizeViewHeight.constant = showSizeActualHeight
            isShoeSizeViewCollapsed = true
            ShoeSizeExpanderButton.setTitle("-", for: .normal)
        }else{
            shoeSizeViewHeight.constant = CGFloat(0.0)
            ShoeSizeExpanderButton.setTitle("+", for: .normal)
            isShoeSizeViewCollapsed = false
        }
    }
    
    @IBAction func onPantSizeExpaneOrCollapsed(_ sender: Any) {
        if isPantSizeViewCollapsed == false{
            pantSizeViewHeight.constant = pantsSizeActualHeight
            pantSizeExpanderButton.setTitle("-", for: .normal)
            isPantSizeViewCollapsed = true
        }else{
            pantSizeViewHeight.constant = CGFloat(0.0)
            pantSizeExpanderButton.setTitle("+", for: .normal)
            isPantSizeViewCollapsed = false
        }
    }
    
    @IBAction func userSelectGroupCheckButton(_ sender: GroupCheckButton) {
        switch sender.tag {
        case Constant.shared.TAG_MALE_FEMALE:
            if let value = sender.selectedButton()?.titleLabel?.text?.trimmingCharacters(in: .whitespacesAndNewlines){
                Constant.shared.searchRequest.productGender = value
            }
            
        case Constant.shared.TAG_CATEGORY:
            var categorySelections = [String]()
            let selectedButtons = sender.selectedBuittons()
            for button in selectedButtons {
                if let value = button.titleLabel?.text?.trimmingCharacters(in: .whitespacesAndNewlines){
                    categorySelections.append(value)
                }
            }
            Constant.shared.searchRequest.category = categorySelections.joined(separator: ",")
            
            
        default:
            break
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
