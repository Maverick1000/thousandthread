//
//  BaseTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/10/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    var cellisSelected = false
    
    @IBOutlet private weak var selectionLabel: UILabel!
    
    private var cellIndexPath:IndexPath?
    public weak var delagate:LocationTimeViewControllerProocol?
    
    public func setLocationTimeViewController(delagate:LocationTimeViewControllerProocol){
        self.delagate = delagate
    }
    
    public func setCellIndexPath(indexPath:IndexPath){
        cellIndexPath = indexPath
    }
    
    public func setSelectionLabel(){
        switch cellisSelected {
        
        case true:
           selectionLabel.text = "X"
        
        case false:
           selectionLabel.text = ""
        }
    }
    
    @objc func onCellTap(gesture:UITapGestureRecognizer){
        if let indexPath = cellIndexPath {
            delagate?.onCellTapGesture(cellIndex: indexPath)
        }
    }
    
}
