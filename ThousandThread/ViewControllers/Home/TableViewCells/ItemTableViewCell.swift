//
//  ItemTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 3/1/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceRentLabel: UILabel!
    @IBOutlet weak var imageDownLoadIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var tagStackView: UIStackView!
    
    public var indexPath:IndexPath!
    private var selectedItem:ItemPageRowResponse!
    private var delegate:LandingViewControllerProtocol?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        itemImageView.image = nil;
        for view in tagStackView.arrangedSubviews{
            tagStackView.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
        indexPath = []
        //imageDownLoadIndicatorView.isHidden = false;
    }
    
    public func setupCell(delegate:LandingViewControllerProtocol?, item:ItemPageRowResponse){
        setupCell(item)
        selectedItem = item
        self.delegate = delegate
    }
    
    private func setupCell(_ item:ItemPageRowResponse){
        
        userNameLabel.text = item.requestUserName()
        titleLabel.text = item.requestItemTitle()
        
        priceRentLabel.text = "$ \(Utility.roundValue(value:item.requestRentPrice(), decimalPlace: 2))";
        
        sizeLabel.text = "SIZE: \(item.requestSize())"
        brandLabel.text = item.requestBrand()
        
        priceLabel.text = "$ \(Utility.roundValue(value:  item.requestPrice(), decimalPlace: 2))"
        
        addTagString(tags: item.requestTags())
        
        if item.reqeustUserPhotoPath().isEmpty {
            NetworkManger.shared.requestUser(userID:
                                item.reqeustUserID()) {[weak self] (success, profile) in
                 let string = profile?.requestCoverphoto() ?? ""
                 self?.loadUserImage(atPath: Constant.shared.kBaseURL + string)
            }
            
        }else{
            loadUserImage(atPath: Constant.shared.kBaseURL + item.reqeustUserPhotoPath())
        }
        
        userImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onUserImageClick(gestureRecognizer:))))
        loadItemImage(atPath: Constant.shared.kBaseURL + item.requestImagePath())
        
        
        itemImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onImageClickForReview(gestureRecognizer:))))
        
        itemImageView.layer.cornerRadius = 7
        itemImageView.layer.borderWidth = 2
        itemImageView.layer.borderColor = UIColor.black.cgColor
    }
    
    @objc private func onImageClickForReview(gestureRecognizer:UITapGestureRecognizer){
        delegate?.showReviewAndComentView(selectedIndexPath: indexPath)
    }
    @objc private func onUserImageClick(gestureRecognizer:UITapGestureRecognizer){
        delegate?.showSelectedProfile(selectedIndexPath: indexPath)
    }
    
    private func loadUserImage(atPath:String){
        userImageView.downloaded(from: atPath,size: userImageView.frame.size) {(success) in
            if success{
                //                self?.imageDownLoadIndicatorView.stopAnimating()
            }
        }
    }
    
    private func loadItemImage(atPath:String){
        imageDownLoadIndicatorView.startAnimating()
        itemImageView.downloaded(from: atPath, size: itemImageView.frame.size) {[weak self] (success) in
            if success{
                self?.imageDownLoadIndicatorView.stopAnimating()
            }
        }
    }
    
    private func addTagString(tags:[String]){
        for value in tags{
            tagStackView.addArrangedSubview(createTagLabel(text:value))
        }
    }
    
    private func createTagLabel(text:String) ->UILabel{
        let label = UILabel(frame: .zero)
        label.font = UIFont(name: "Helvetica", size: 13)
        label.textColor = Constant.shared.kCatagoryColor
        label.numberOfLines = 0
        label.text = "# \(text)"
        label.sizeToFit()
        return label
    }
}
