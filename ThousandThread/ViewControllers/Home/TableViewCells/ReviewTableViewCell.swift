//
//  ReviewTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 4/15/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var reviewRating: StarRatingView!
    @IBOutlet weak var reveiwComment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        reviewRating.setStarRating(starRating: 0)
        reveiwComment.text = ""
    }
    
    func setupCell(review:Review){
        reviewRating.setAlignment(aligment:.leading)
        reviewRating.setStarRating(starRating: review.requestRating())
        reveiwComment.text = review.requestComments()
    }
    
}
