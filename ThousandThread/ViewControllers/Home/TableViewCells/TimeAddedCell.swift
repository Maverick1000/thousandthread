//
//  TimeAddedCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/2/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class TimeAddedCell: BaseTableViewCell {
    
    @IBOutlet weak var availableStartTimeTextField: UITextField!
    @IBOutlet weak var availableEndTimeTextField: UITextField!
    @IBOutlet weak var weekDayTextField: UITextField!
    
    private var onCellTapGesture:UITapGestureRecognizer!
    private var selectedTextField:UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addInputAccessoryView()
        
        if onCellTapGesture == nil{
            onCellTapGesture = UITapGestureRecognizer(target: self, action: #selector(onCellTap(gesture:)))
            addGestureRecognizer(onCellTapGesture)
        }
    }
    
    public func setWeekDayAccessoryView(view:UIView){
        weekDayTextField.inputView = view
    }
    
    public func setAvailableTimeTextFieldccessoryView(view:UIView){
        availableStartTimeTextField.inputView = view
        availableEndTimeTextField.inputView = view
    }
    
    private func addInputAccessoryView(){
        let toolBar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 35))
        
        let doneToolBarItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneWithTextInput))
        doneToolBarItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name:Constant.shared.kHelveticBaseFont, size: 14) as Any,
                                                NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        
        doneToolBarItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name:Constant.shared.kHelveticBaseFont, size: 14) as Any,
                                                NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
        
        toolBar.barStyle = .default
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                         doneToolBarItem]
        toolBar.sizeToFit()
        
        weekDayTextField.inputAccessoryView = toolBar
        availableStartTimeTextField.inputAccessoryView = toolBar
        availableEndTimeTextField.inputAccessoryView = toolBar
    }
    
    @objc func doneWithTextInput() {
        if selectedTextField == nil{
            weekDayTextField.resignFirstResponder()
        }else{
            selectedTextField.resignFirstResponder()
        }
    }
}

extension TimeAddedCell:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
}

extension TimeAddedCell:CustomPickerViewItemSelector{
    func userSelectedPickerItem(selectedItem: String) {
        weekDayTextField.text = selectedItem
    }
}

extension TimeAddedCell:DatePickerViewItemSelector{
    func userSelectedDatePickerItem(selectedItem:Date){
        
        if selectedTextField == availableStartTimeTextField{
            availableStartTimeTextField.text = selectedItem.localTime
        }else{
            availableEndTimeTextField.text = selectedItem.localTime
        }
    }
}
