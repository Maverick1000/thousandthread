//
//  Slide.swift
//  ThousandThread
//
//  Created by Paul Walker on 6/23/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class Slide: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageDownLoadIndicatorView:UIActivityIndicatorView!
    
    var imagePath:String!
    var laodFromDisk = false
    
    override func draw(_ rect: CGRect) {
        
        imageView.layer.cornerRadius = 7
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.black.cgColor
        if laodFromDisk {
            imageView.image = Utility.requestImageDocument(atPath: imagePath)
        }else{
            loadItemImage(atPath: Constant.shared.kBaseURL + imagePath)
        }
    }
    
    private func loadItemImage(atPath:String){
        imageDownLoadIndicatorView.startAnimating()
        imageView.downloaded(from: atPath, size: imageView.frame.size) {[weak self] (success) in
            if success{
                self?.imageDownLoadIndicatorView.stopAnimating()
            }
        }
    }
}
