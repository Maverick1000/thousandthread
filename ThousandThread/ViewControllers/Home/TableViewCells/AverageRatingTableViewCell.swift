//
//  AverageRatingTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 6/20/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class AverageRatingTableViewCell: UITableViewCell {

    @IBOutlet weak var averageRating: StarRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        averageRating.setStarRating(starRating: 0)
    }
    
    func setupCell(starRating:Double){
        averageRating.setStarRating(starRating:starRating)
    }
}
