//
//  PostingListConfrimViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/7/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class PostingListConfrimViewController: BaseViewController {
    var numberOfImagesSaved = 0
    var postItem:ItemPageRowResponse!
    
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceRentLabel: UILabel!
    @IBOutlet weak var additionalnstructions: UITextView!
    @IBOutlet weak var unavailableDate:UITextView!
    
    var imageScrollViewController:ImageScrollViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        itemTitle.text = postItem.requestItemTitle()
        priceRentLabel.text = "$ \(postItem.requestRentPrice())"
        priceLabel.text = "$ \(postItem.requestPrice())"
        additionalnstructions.text = postItem.requestNotes()
        
        if postItem.requestNotAvailStartDt() != nil &&
            postItem.requestNotAvailEndDt() != nil{
            unavailableDate.text = Utility.unavailableDateRange(startDate:
                        postItem.requestNotAvailStartDt()!, endDate: postItem.requestNotAvailEndDt()!)
        }
        
        imageScrollViewController.setupCellSlideScrollView()
    }
    
    @IBAction func userTouchListItem(_ sender: Any) {
        var imagesData = [Data]()
        for imagePath in Utility.requestPostAddImageFilePaths(){
            do{
                let imageBytes = try Data(contentsOf: imagePath)
                if let image = UIImage(data: imageBytes) {
                    if let jpegimageData = image.jpegData(compressionQuality: 1.0){
                        imagesData.append(jpegimageData)
                    }
                }
            }catch{
                print("could not upload post item")
            }
        }
        NetworkManger.shared.postItem(profile: LoginController.shared.curretSigInUser,
                                      images: imagesData, item: postItem) { (sucess) in
                                        if sucess{
                                            self.navigationController?.popToRootViewController(animated: true)
                                        }
        }
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "imageScrollViewController"{
            imageScrollViewController = segue.destination as? ImageScrollViewController
        }
    }
}

