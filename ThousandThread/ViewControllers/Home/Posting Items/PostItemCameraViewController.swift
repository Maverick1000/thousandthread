//
//  PostItemCameraViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/15/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import AVFoundation
import IoniconsKit
import BSImagePicker
import Photos

class PostItemCameraViewController: BaseViewController {
    
    private var input: AnyObject?
    private var captureSession:AVCaptureSession!
    private var currentCamera:AVCaptureDevice.Position!
    private var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    private var videoOutput: AVCaptureVideoDataOutput!
    private var photoOutput: AVCapturePhotoOutput!
    private var sessionQueue: DispatchQueue!
    
    private var SelectedAssets = [PHAsset]()
    private var photoArray = [UIImage]()
    private var stillPhotoCallback: ((CIImage?) -> Void)? = nil
    
    @IBOutlet weak var previewCameraLayer: UIView!
    @IBOutlet weak var cancelImageView: UIImageView!
    @IBOutlet weak var takePhotoImage: UIImageView!
    @IBOutlet weak var photoAblum: UIImageView!
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var cropAreaView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentCamera = .back
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        currentCamera = .back
        
        do{
            captureSession = try createAVSession(position: currentCamera)
            //captureSession.append( try createAVSession(position: .front))
        }catch{
            print("Capture Session failed")
        }
        
        
        cancelImageView.image = UIImage.ionicon(with: .iosCloseEmpty,
                                                textColor: UIColor.white , size: CGSize(width: 55, height: 55))
        
        takePhotoImage.image = UIImage.ionicon(with: .iosCircleFilled,
                                               textColor: UIColor.white, size: CGSize(width: 50, height: 50))
        
        photoAblum.image = UIImage.ionicon(with: .iosFolderOutline,
                                           textColor: UIColor.white, size: CGSize(width: 50, height: 50))
        
        cameraImage.image = UIImage.ionicon(with: .iosReverseCameraOutline,
                                            textColor: UIColor.white, size: CGSize(width: 50, height: 50))
        
        cancelImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(close)))
        takePhotoImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tatkePicture)))
        photoAblum.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPhotosAlbum)))
        
        cameraImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(reverseCamera)))
        
        cropAreaView.layer.borderColor = UIColor.white.cgColor
        cropAreaView.layer.borderWidth = 2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // And kick it of
        sessionQueue.async {
            self.captureSession.startRunning()
        }
    }
    
    @objc private func close(gestureRecognize:UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func showPhotosAlbum() {
        
        let vc = BSImagePickerViewController()
        
        self.bs_presentImagePickerController(vc, animated: true, select: { (assest: PHAsset) -> Void in
        },
                                             deselect: { (assest: PHAsset) -> Void in
                                                
        }, cancel: { (assest: [PHAsset]) -> Void in
            
        }, finish: { (assest: [PHAsset]) -> Void in
            
            for i in 0..<assest.count
            {
                self.SelectedAssets.append(assest[i])
            }
            
            self.convertAssetToImages()
            
        }, completion: nil)
    }
    
    @objc private func tatkePicture(){
        
        captureStillPhoto({ [weak self] (image) in
            guard let detectedImage = image else {
                return
            }
            let controller = CropViewController()
            controller.delegate = self
            controller.image = UIImage(ciImage: detectedImage)
            
            let navController = UINavigationController(rootViewController: controller)
            self?.present(navController, animated: true, completion: nil)
        })
    }
    
    @objc private func reverseCamera(){
        do{
            if currentCamera == .back {
                currentCamera = .front
                captureSession = try createAVSession(position: currentCamera)
            }else{
                currentCamera = .back
                captureSession = try createAVSession(position: currentCamera)
            }
        }catch{
            print("Capture Session failed")
        }
        
        sessionQueue.async {
            self.captureSession.startRunning()
        }
    }
    
    private func createAVSession(position:AVCaptureDevice.Position)  throws -> AVCaptureSession{
        
        let session = AVCaptureSession()
        
        if let videoCaptureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: position){
            do {
                input = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                print("video device error")
            }
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session )
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame.size = view.frame.size
            previewCameraLayer.layer.addSublayer(videoPreviewLayer!)
            
            session.addInput(input as! AVCaptureInput)
            sessionQueue = DispatchQueue(label: "AVSessionQueue", attributes: [])
            
            photoOutput = AVCapturePhotoOutput()
            session.addOutput(photoOutput)
        }
        return session
    }
    
    private func captureStillPhoto(_ callback: @escaping (_ image: CIImage?) -> Void) {
        if let photoOutputConnection = photoOutput.connection(with: .video) {
            photoOutputConnection.videoOrientation = .landscapeLeft
        }
        
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]
        
        settings.flashMode = .off
        settings.isAutoStillImageStabilizationEnabled = true
        settings.previewPhotoFormat = previewFormat
        photoOutput.capturePhoto(with: settings, delegate: self)
        stillPhotoCallback = callback
    }
    
    private func imageFromSampleBuffer(sampleBuffer: CMSampleBuffer) -> CIImage? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
        let ciImage = CIImage(cvPixelBuffer: imageBuffer)
        return ciImage
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//extension PostItemCameraViewController: AVCaptureVideoDataOutputSampleBufferDelegate{
//    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
//        guard let ciImage = imageFromSampleBuffer(sampleBuffer: sampleBuffer) else { return }
//        DispatchQueue.main.async { [unowned self] in
//            //self.captureSelfPic(image: ciImage)
//        }
//    }
//}

extension PostItemCameraViewController{
    
    func convertAssetToImages() -> Void {
        
        if SelectedAssets.count != 0{
            
            for i in 0..<SelectedAssets.count{
                
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                
                var thumbnail = UIImage()
                
                option.isSynchronous = true
                
                manager.requestImage(for: SelectedAssets[i], targetSize: CGSize(width: 500, height: 500),
                                     contentMode: .aspectFill, options: option, resultHandler: {(result,info) -> Void in
                    thumbnail = result!
                })
                
                let data = thumbnail.jpegData(compressionQuality: 1)
                let newImage = UIImage(data: data!)
                newImage?.saveToDocuments(filename: "image_\(Utility.imageIndex)", ext: "jpg")
                
               Utility.imageIndex += 1
            }
        }
    }
}


extension PostItemCameraViewController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard error == nil,
            let photoCallback = stillPhotoCallback,
            let imageData = photo.fileDataRepresentation(),
            let image = CIImage(data: imageData)
            else {
                print("error occur: \(error?.localizedDescription ?? "")")
                self.stillPhotoCallback?(nil)
                self.stillPhotoCallback = nil
                return
        }
        let imagePortrait = image.oriented(.right)
        photoCallback(imagePortrait)
        stillPhotoCallback = nil
    }
}

extension PostItemCameraViewController:CropViewControllerDelegate{
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        let imagefilename = "image_\(Utility.imageIndex)"
        image.saveToDocuments(filename: imagefilename, ext: "jpg")
        Utility.imageIndex += 1
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
