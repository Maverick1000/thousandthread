//
//  DateHeaderCollectionReusableView.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/9/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import JTAppleCalendar

class DateHeaderCollectionReusableView: JTAppleCollectionReusableView {
    @IBOutlet var monthTitle: UILabel!
}
