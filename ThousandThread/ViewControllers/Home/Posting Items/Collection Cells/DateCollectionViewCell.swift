//
//  DateCollectionViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/9/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import JTAppleCalendar

class DateCollectionViewCell: JTAppleCell {
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var selectedView: UIView!
}
