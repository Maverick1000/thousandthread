//
//  PostlistingViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 6/19/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class PostlistingViewController: BaseViewController {
    
    var imageIndex = 0
    var postItem:ItemPageRowResponse!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var viewContianer: UIView!
    @IBOutlet weak var additionalInstructionsTextView: UITextView!
    
    @IBOutlet weak var nameOfItem: CustomTextInputView!
    @IBOutlet weak var priceListPerDay: CustomTextInputView!
    @IBOutlet weak var rentalPrice: CustomTextInputView!
    @IBOutlet weak var catergory: CustomTextInputView!
    @IBOutlet weak var size: CustomTextInputView!
    
    @IBOutlet weak var brand: CustomTextInputView!
    @IBOutlet weak var gender: CustomTextInputView!
    @IBOutlet weak var hashtags: CustomTextInputView!
    @IBOutlet weak var instructions: UITextView!
    @IBOutlet weak var addPhotoLabel: UILabel!
    
    var calendarViewController: CalanderViewController!
    
    var imageScrollViewController:ImageScrollViewController!
    var postItemCameraViewController :PostItemCameraViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "post listing"
        setupView()
        
        if postItemCameraViewController != nil {
            imageScrollViewController.lastPictureIndex = Utility.imageIndex
            
            imageScrollViewController.setupCellSlideScrollView()
            imageScrollViewController.pageControl.isHidden = false
            
            addPhotoLabel.isHidden = true
            imageButton.isHidden = true
        }else{
            addPhotoLabel.isHidden = false
            imageButton.isHidden = false
            imageScrollViewController.pageControl.isHidden = true
        }
        
    }
    
    @IBAction func openCameraButton(_ sender: Any) {
        postItemCameraViewController = Utility.requestViewController(identifier:
            "postItemCameraViewController", storyborad: "HomeNav") as? PostItemCameraViewController
        present(postItemCameraViewController, animated: true, completion: nil)
    }
    
    @IBAction func onSelectedNextButton(_ sender: Any) {
        
        if validateInputFields(){
            postItem = ItemPageRowResponse()
            
            if let user = LoginController.shared.curretSigInUser{
                postItem.setValue(NSNumber(value: user.requestUserID()), forKey: "user_id")
            }
            
            let category_text = catergory.requetTextField().text ?? ""
            let nameOfItem_text = nameOfItem.requetTextField().text ?? ""
            let size_text = size.requetTextField().text ?? ""
            let brand_text = brand.requetTextField().text ?? ""
            let gener_text = gender.requetTextField().text ?? ""
            
            var rentalPricetext_Dollar:NSNumber!
            var retalPricetext_Dollar:NSNumber!
            
            if let rentalPricetext_text = priceListPerDay.requetTextField().text {
                rentalPricetext_Dollar = NSNumber(value: Double(rentalPricetext_text) ?? 0.0)
            }
            
            if let retalPricetext_text = rentalPrice.requetTextField().text {
                retalPricetext_Dollar = NSNumber(value: Double(retalPricetext_text) ?? 0.0)
            }
            
            postItem.setValue(nameOfItem_text, forKey: "title")
            postItem.setValue(category_text, forKey: "category")
            postItem.setValue(rentalPricetext_Dollar, forKey: "priceRent")
            postItem.setValue(retalPricetext_Dollar, forKey: "price")
            postItem.setValue(size_text, forKey: "size")
            postItem.setValue(brand_text, forKey: "brand")
            postItem.setValue(gener_text, forKey: "productGender")
            postItem.setValue(calendarViewController.calendarView.selectedDates[0], forKey: "notAvailStartDt")
            postItem.setValue(calendarViewController.calendarView.selectedDates[calendarViewController.calendarView.selectedDates.count - 1], forKey: "notAvailEndDt")
            
            getHashtags()
            getNote()
            
            performSegue(withIdentifier: "postlistConfrim", sender: nil)
        }
    }
    
    private func setupView(){
        viewContianer.layer.cornerRadius = 7
        viewContianer.layer.borderWidth = 1
        viewContianer.layer.borderColor = UIColor.black.cgColor
        
        additionalInstructionsTextView.layer.cornerRadius = 5
        additionalInstructionsTextView.layer.borderWidth = 1
        additionalInstructionsTextView.layer.borderColor = UIColor.black.cgColor
        
        imageButton.layer.cornerRadius = imageButton.frame.width / 2
        
        postItem = ItemPageRowResponse()
    }
    
    private func getHashtags(){
        if let hashtags_text = hashtags.requetTextField().text {
            let tags = hashtags_text.split(separator: "#")
            for(index, tag) in tags.enumerated(){
                if tag.isEmpty == false{
                    postItem.setValue(tag, forKey: "tag\(index+1)")
                }
            }
        }
    }
    
    private func getNote(){
         if let instructions_text = instructions.text {
             postItem.setValue(instructions_text, forKey: "note")
        }
    }
    
    private func validateInputFields() ->Bool{
        var isValid = true
        if nameOfItem.requetTextField().text!.isEmpty{
            isValid = false
        }
        
        if rentalPrice.requetTextField().text!.isEmpty{
            isValid = false
        }
        
        if priceListPerDay.requetTextField().text!.isEmpty{
            isValid = false
        }
        
        if catergory.requetTextField().text!.isEmpty{
            isValid = false
        }
        
        if size.requetTextField().text!.isEmpty{
            isValid = false
        }
        
        return isValid
    }
    
    // MARK: - Navigation
    
    //In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "postlistConfrim"{
            let VC = segue.destination  as! PostingListConfrimViewController
            VC.postItem = postItem
            VC.numberOfImagesSaved = imageIndex
        }else if segue.identifier == "CalendarViewController"{
            calendarViewController = segue.destination as? CalanderViewController
        }else if segue.identifier == "imageScrollViewController" {
            imageScrollViewController = segue.destination as? ImageScrollViewController
        }
    }
}

extension PostlistingViewController:ImagePickerDelegate{
    func didSelect(image: UIImage?) {
        image?.saveToDocuments(filename: "image_\(Utility.imageIndex)", ext: "jpg")
        imageView.image = image
        imageButton.isHidden = true
        Utility.imageIndex += 1
    }
}
