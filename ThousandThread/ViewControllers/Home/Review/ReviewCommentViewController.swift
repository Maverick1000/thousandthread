//
//  ReviewCommentViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 3/31/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class ReviewCommentViewController: BaseViewController {
    
    @IBOutlet weak var reviewTableViewCell:UITableView!
    
    var selectedImage:UIImage!
    var userProfileImage:UIImage!
    private var reviews:[Review]!
    private var pageRowResponse:ItemPageRowResponse!
    
    init(_ pageRowResponse:ItemPageRowResponse,nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.pageRowResponse = pageRowResponse
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reviewTableViewCell.register(UINib(nibName:"ItemTableViewCell", bundle: nil),
                                     forCellReuseIdentifier: "ItemTableViewCell")
        
        reviewTableViewCell.register(UINib(nibName: "AverageRatingTableViewCell", bundle: nil),
                                     forCellReuseIdentifier: "averageRatingTableViewCell")
        
        reviewTableViewCell.register(UINib(nibName: "ReviewTableViewCell", bundle: nil),
                                     forCellReuseIdentifier: "reviewTableViewCell")
        let id = self.pageRowResponse.reqeustItemID()
        NetworkManger.shared.requestReviewComments(id:id){ [weak self] (succes, reviews) in
            if succes {
                self?.reviews = reviews
                self?.reviewTableViewCell.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "rating and reviews"
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
}

extension ReviewCommentViewController:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = loadViewFromNib()
        if section == 1 {
            view?.headerTitle.text = "Average Rating:"
        }else if section == 2{
            view?.headerTitle.text = "Comments:"
        }
        return view
    }
    
    private func loadViewFromNib() -> HeaderView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "HeaderView", bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? HeaderView
    }
}


extension ReviewCommentViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if section == 0 || section == 1 {
            count = 1
        }else if section == 2{
            if let count = reviews?.count{
                return count
            }
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell!
        
        if indexPath.section == 0{
            cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell") as! ItemTableViewCell
            
            let itemTableViewCell = cell as! ItemTableViewCell
            itemTableViewCell.setupCell(delegate: nil, item:  self.pageRowResponse)
            
        }else if indexPath.section == 1{
            cell = tableView.dequeueReusableCell(withIdentifier: "averageRatingTableViewCell") as! AverageRatingTableViewCell
            
            let averageRatingTableViewCell = cell as! AverageRatingTableViewCell
            averageRatingTableViewCell.setupCell(starRating: pageRowResponse.requestReviewAverage())
            
        }else if indexPath.section == 2 {
            cell = tableView.dequeueReusableCell(withIdentifier: "reviewTableViewCell") as! ReviewTableViewCell
            
            let reviewTableViewCell = cell as! ReviewTableViewCell
            reviewTableViewCell.setupCell(review: reviews[indexPath.row])
        }
        
        cell.selectionStyle = .none
        return cell
    }
}
