//
//  LandingViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 3/1/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LandingViewController: BaseViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var items:[ItemPageRowResponse]!
    private var pageRespone:ItemPageResponse!
    
    private var bViewDidDisappear = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        bViewDidDisappear = false;
        
        navigationItem.title = "thousandthread"
        
        let nib = UINib(nibName: "ItemTableViewCell", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "Item")
        
        tableView.estimatedRowHeight = 120;
        tableView.rowHeight = UITableView.automaticDimension
        
        if pageRespone == nil{
            items = [ItemPageRowResponse]()
            loadFirstPageOfItems()
        }else{
            if items.count > 0 {
                items.removeAll()
                loadFirstPageOfItems()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        bViewDidDisappear = true;
    }
    
    private func loadFirstPageOfItems(){
        startAnimating(type:.ballRotateChase)
        ItemResquestQueue.shared.requestFirstPageOfItems { [weak self] (success, pageRespose) in
            self?.stopAnimating()
            if success{
                self?.pageRespone = pageRespose
                self?.items.append(contentsOf: pageRespose.requestItems())
                self?.tableView.reloadData()
            }
        }
    }
    
    private func loadPageNumber(page:Int){
        startAnimating(type:.ballRotateChase)
        ItemResquestQueue.shared.requestPageOItems(atPage: page) { [weak self] (success, pageRespose) in
             self?.stopAnimating()
            if success{
                self?.items.append(contentsOf: pageRespose.requestItems())
                self?.tableView.reloadData()
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension LandingViewController:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension LandingViewController: UITableViewDataSourcePrefetching
{
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if indexPath.row == self.items.count - 1{
                if pageRespone.totalItems() > items.count{
                    loadPageNumber(page: pageRespone.requestNextPage())
                }
            }
        }
    }
}

extension LandingViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(bViewDidDisappear){
            return 0
        }
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Item", for: indexPath) as! ItemTableViewCell
        cell.selectionStyle = .none
        cell.indexPath = indexPath
        cell.setupCell(delegate: self, item: items[indexPath.row])
        return cell
    }
}

extension LandingViewController:LandingViewControllerProtocol{
    func showReviewAndComentView(selectedIndexPath:IndexPath){
        if items.count > 0 {
            let selectedCell = tableView.cellForRow(at: selectedIndexPath) as! ItemTableViewCell
            let VC = Utility.reqeustReviewCommentViewController(pageRowResponse: items![selectedIndexPath.row])
            VC.userProfileImage = selectedCell.userImageView.image
            //VC.selectedImage = selectedCell.itemImageView?.image
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    func showSelectedProfile(selectedIndexPath:IndexPath){
       
        let selectedtemPageRowResponse = items[selectedIndexPath.row]
        let VC = Utility.requestViewController(identifier: "otherProfileViewController", storyborad: "HomeNav") as! OtherProfileViewController
        VC.selectedUserProfileID = selectedtemPageRowResponse.reqeustUserID()
       
        if LoginController.shared.curretSigInUser.requestUserID()
            != selectedtemPageRowResponse.reqeustUserID(){
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
}
