//
//  ProfileTableViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/24/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    let profileOptions = ["Current Exchanges","Exchange History",
                           "","Balance","Promos","Settings","","Help","Logout"]
    
    let profileImageOptions = ["CurrentExchanges","ExchangeHistory","","Balance",
                                "Promos","Settings","","Help","Logout"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let nib = UINib(nibName: "SettingsTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "profile")
    }
    
    //TableViewDelegates
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileOptions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profile", for: indexPath) as! SettingsTableViewCell
        let optionString = profileOptions[indexPath.row]
        let optionsImageName = profileImageOptions[indexPath.row]
        
        configureCellTapGesture(index: indexPath.row, cell: cell)
        cell.configureCell(optionName: optionString, optionImageName: optionsImageName)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func configureCellTapGesture(index:Int, cell:UITableViewCell){
        let tapTempGesture:UITapGestureRecognizer!
        
        switch index {
        case Constant.shared.TAG_CURRENTEXCHANGES:
            tapTempGesture = UITapGestureRecognizer(target: self,
                                                    action: #selector(userSelectedCurrentExchanges))
            cell.addGestureRecognizer(tapTempGesture)
        case Constant.shared.TAG_EXCHANGEHISTORY:
             tapTempGesture = UITapGestureRecognizer(target: self,
                                                     action: #selector(userSelectedExchangeHistory))
            cell.addGestureRecognizer(tapTempGesture)
        case Constant.shared.TAG_BALANCE:
            tapTempGesture = UITapGestureRecognizer(target: self,
                                                    action: #selector(userSelectedBalance))
            cell.addGestureRecognizer(tapTempGesture)
        case Constant.shared.TAG_PROMOS:
            tapTempGesture = UITapGestureRecognizer(target: self,
                                                    action: #selector(userSelectedPromos))
            cell.addGestureRecognizer(tapTempGesture)
        case Constant.shared.TAG_SETTINGS:
            tapTempGesture = UITapGestureRecognizer(target: self,
                                                    action: #selector(userSelectedSettings))
            cell.addGestureRecognizer(tapTempGesture)
        case Constant.shared.TAG_HELP:
            tapTempGesture = UITapGestureRecognizer(target: self,
                                                    action: #selector(userSelectedHelp))
            cell.addGestureRecognizer(tapTempGesture)
        case Constant.shared.TAG_LOGOUT:
            tapTempGesture = UITapGestureRecognizer(target: self,
                                                    action: #selector(userSelectedLogOut))
            cell.addGestureRecognizer(tapTempGesture)
        default:
            tapTempGesture = UITapGestureRecognizer(target: self,
                                                    action: #selector(userSelectedLogOut))
            cell.addGestureRecognizer(tapTempGesture)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc private func userSelectedCurrentExchanges(trapGestureRecognizer:UITapGestureRecognizer){
        
    }
    
    @objc private func userSelectedExchangeHistory(trapGestureRecognizer:UITapGestureRecognizer){
        
    }
    
    @objc private func userSelectedBalance(trapGestureRecognizer:UITapGestureRecognizer){
        
    }
    
    @objc private func userSelectedPromos(trapGestureRecognizer:UITapGestureRecognizer){
        
    }
    
    @objc private func userSelectedSettings(trapGestureRecognizer:UITapGestureRecognizer){
        
    }
    
    @objc private func userSelectedHelp(trapGestureRecognizer:UITapGestureRecognizer){
        
    }
    
    @objc private func userSelectedLogOut(trapGestureRecognizer:UITapGestureRecognizer){
        LoginController.shared.updateLoginStatus(status: false,
                                                 profileID: LoginController.shared.curretSigInUser.requestUserID())
        Constant.shared.appDelegate.logOutAnimation()
        LoginController.shared.curretSigInUser = nil
    }
}
