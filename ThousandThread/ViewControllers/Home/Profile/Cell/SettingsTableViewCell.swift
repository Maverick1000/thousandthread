//
//  ProfileTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/24/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var profileOptionsLabel: UILabel!
    @IBOutlet weak var profileOptionImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(optionName:String, optionImageName:String){
        
        profileOptionsLabel.text = optionName
        profileOptionImageView.image = UIImage(named: optionImageName)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
