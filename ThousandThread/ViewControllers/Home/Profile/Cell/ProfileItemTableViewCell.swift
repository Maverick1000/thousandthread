//
//  ProfileItemTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/29/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class ProfileItemTableViewCell: UICollectionViewCell {
    
    @IBOutlet weak var item: UIImageView!
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var priceRent: UILabel!
    @IBOutlet weak var retailCost: UILabel!
    @IBOutlet weak var size: UILabel!
    @IBOutlet weak var imageDownLoadIndicatorView:UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        item.image = nil
    }
    
    func setupCell(cellData:ProfileItemCell){
        title.text = cellData.title
        priceRent.text = cellData.priceRent
        retailCost.text = cellData.retailPrice
        size.text = cellData.size
        
        item.layer.borderWidth = 2
        item.layer.cornerRadius = 5
        item.layer.borderColor = UIColor.black.cgColor
        
        imageDownLoadIndicatorView.startAnimating()
        item.downloaded(from: cellData.imagePath,size: item.frame.size) { [weak self](success) in
            if success{
                self?.imageDownLoadIndicatorView.stopAnimating()
            }
        }
    }
}
