//
//  OtherProfileViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/31/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import IoniconsKit

class OtherProfileViewController: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileScreenNameLabel: UILabel!
    @IBOutlet weak var numberOfItemsLabel: UILabel!
    @IBOutlet weak var nubmerOfFollowersLabel: UILabel!
    @IBOutlet weak var numberOfFollowingLabel: UILabel!
    @IBOutlet weak var allSelectedView: UIView!
    @IBOutlet weak var friendsSelectedView: UIView!
    
    @IBOutlet weak var userAllButton: UIButton!
    @IBOutlet weak var firendsClosetButton: UIButton!
    
    var selectedUserProfileID: Int!
    private var selectedUserProfile:UserProfile!
    
    private var selectedTab:Int!
    private var pageViewController:PageViewController!
    private var itemPageResponse:ItemPageResponse!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedTab = Constant.shared.TAG_USER_ALL_ITEMS
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NetworkManger.shared.requestUser(userID: selectedUserProfileID) { [ weak self ] (success, userProfile) in
            if success {
                if userProfile != nil{
                    self?.selectedUserProfile = userProfile
                    
                    self?.setupView()
                    self?.updateCurrentSelectedPage()
                    
                    let userID = LoginController.shared.curretSigInUser.requestUserID()
                    NetworkManger.shared.requestItems(forId: userID) { [weak self]  (success,itemPageResponse ) in
                        if success {
                            let value = itemPageResponse!.totalItems()
                            self?.itemPageResponse = itemPageResponse
                            DispatchQueue.main.async {
                                self?.numberOfItemsLabel.text = "\(value)"
                            }
                        }else{
                            DispatchQueue.main.async {
                                self?.numberOfItemsLabel.text = "0"
                            }
                        }
                    }
                    
                    NetworkManger.shared.requestUserWhoFollowMe(userID: userID)
                    { [weak self] (success, socialPageResponse) in
                        
                        if success{
                            let value = socialPageResponse!.totalRows.intValue
                            DispatchQueue.main.async {
                                self?.nubmerOfFollowersLabel.text = "\(value)"
                            }
                        }else{
                            DispatchQueue.main.async {
                                self?.nubmerOfFollowersLabel.text = "0"
                            }
                        }
                    }
                    
                    NetworkManger.shared.requestWhoFollowByMe(userID: userID)
                    { [weak self] (success, socialPageResponse) in
                        if success{
                            let value = socialPageResponse!.totalRows.intValue
                            DispatchQueue.main.async {
                                self?.nubmerOfFollowersLabel.text = "\(value)"
                            }
                        }else{
                            DispatchQueue.main.async {
                                self?.nubmerOfFollowersLabel.text = "0"
                            }
                        }
                    }
                    
                    NetworkManger.shared.requestUserWhoFollowMe(userID: userID)
                    { [weak self] (success, socialPageResponse) in
                        if success{
                            let value = socialPageResponse!.totalRows.intValue
                            DispatchQueue.main.async {
                                self?.numberOfFollowingLabel.text = "\(value)"
                            }
                        }else{
                            DispatchQueue.main.async {
                                self?.numberOfFollowingLabel.text = "0"
                            }
                        }
                    }
                    
                }
            }
        }
    }
    
    private func setupView(){
        navigationItem.title = selectedUserProfile.requestScreenName()
        profileNameLabel.text = "\(selectedUserProfile.requestFristName()) \(selectedUserProfile.requestLastName())"
        
        let photoPath = selectedUserProfile.requestCoverphoto()
        Utility.loadImage(itemImageView: profileImage,
                          atPath: Constant.shared.kBaseURL + "/" + photoPath )
        
        profileImage.layer.borderWidth = 1
        profileImage.layer.borderColor = UIColor.black.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        
        navigationItem.rightBarButtonItem =  UIBarButtonItem(image:  UIImage.ionicon(with: .iosChatbubbleOutline,
                                                textColor: UIColor.white, size: CGSize(width: 50, height: 50)), style: .plain, target: self, action: #selector(userChatBubble))
        requestUserItemlist()

    }
    
    @objc private func userChatBubble(){
        //ChatMessageTableViewController
        let VC = Utility.requestViewController(identifier: "chatMessageTableViewController", storyborad: "HomeNav") as! ChatMessageViewController
        //VC.chatUserProfie = selectedUserProfile
        navigationController?.pushViewController(VC, animated: true)
    }
    
    
    private func requestUserItemlist(){
        let VC =  pageViewController.orderedViewControllers[0] as! ItemCollectionViewController
        VC.requestLoginUserItemList(userID: selectedUserProfileID)
    }
    
    private func requestUserFriendsClosetItemlist(){
        let VC =  pageViewController.orderedViewControllers[1] as! ItemCollectionViewController
        VC.requestLoginUserFriendClosset(userID: selectedUserProfileID)
    }
    
    private func updateCurrentSelectedPage(){
        updateTabBarSelection(tabBarIndex: selectedTab)
    }
    
    @IBAction func userSelectedAll(_ sender: Any) {
        selectedTab = Constant.shared.TAG_USER_ALL_ITEMS
        pageViewController.gotoPage(index: 0)
        updateCurrentSelectedPage()
        requestUserItemlist()
        
    }
    
    @IBAction func userSelectedFriendCloset(_ sender: Any) {
        selectedTab = Constant.shared.TAG_FRIENDS_CLOSET
        pageViewController.gotoPage(index: 1)
        updateCurrentSelectedPage()
        requestUserFriendsClosetItemlist()
    }
    
    private func updateTabBarSelection(tabBarIndex:Int){
        switch tabBarIndex {
        case Constant.shared.TAG_USER_ALL_ITEMS:
            firendsClosetButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
            friendsSelectedView.backgroundColor =  Constant.shared.kUnSelctedTabColor
            
            userAllButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.normal)
            allSelectedView.backgroundColor = Constant.shared.kTagBaseColor
            
        case Constant.shared.TAG_FRIENDS_CLOSET:
            userAllButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
            allSelectedView.backgroundColor = Constant.shared.kUnSelctedTabColor
            
            firendsClosetButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.normal)
            friendsSelectedView.backgroundColor = Constant.shared.kTagBaseColor
            
        default:
            firendsClosetButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
            friendsSelectedView.backgroundColor = UIColor.black
            
            userAllButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.normal)
            allSelectedView.backgroundColor = Constant.shared.kTagBaseColor
            
        }
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "onPageViewController"{
            pageViewController = segue.destination as? PageViewController
            pageViewController.pageViewControllerDelegate = self
            pageViewController.addPageViewControlls(viewControllers: ["ItemCollectionView","ItemCollectionView"])
            pageViewController.setupPageViewControllTabIndex(viewControllerIndex: 0, pageIndex: Constant.shared.TAG_USER_ALL_ITEMS)
            pageViewController.setupPageViewControllTabIndex(viewControllerIndex: 1, pageIndex: Constant.shared.TAG_FRIENDS_CLOSET)
            pageViewController.setupFirstViewConroller()
        }
    }
}

extension OtherProfileViewController:PageViewControllerDelegate{
    func pageViewControllerHasChange(pageIndex: Int) {
        switch pageIndex {
        case Constant.shared.TAG_USER_ALL_ITEMS:
            requestUserItemlist()
            selectedTab = Constant.shared.TAG_USER_ALL_ITEMS
            updateTabBarSelection(tabBarIndex: pageIndex)
            
        case Constant.shared.TAG_FRIENDS_CLOSET:
            requestUserFriendsClosetItemlist()
            selectedTab = Constant.shared.TAG_FRIENDS_CLOSET
            updateTabBarSelection(tabBarIndex: pageIndex)
            
        default:
            requestUserItemlist()
            selectedTab = Constant.shared.TAG_USER_ALL_ITEMS
            updateTabBarSelection(tabBarIndex: pageIndex)
        }
    }
}
