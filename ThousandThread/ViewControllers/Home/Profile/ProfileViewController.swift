//
//  ProfileViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/29/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileScreenNameLabel: UILabel!
    @IBOutlet weak var numberOfItemsLabel: UILabel!
    @IBOutlet weak var nubmerOfFollowersLabel: UILabel!
    @IBOutlet weak var numberOfFollowingLabel: UILabel!
    @IBOutlet weak var allSelectedView: UIView!
    @IBOutlet weak var friendsSelectedView: UIView!
    
    @IBOutlet weak var userAllButton: UIButton!
    @IBOutlet weak var firendsClosetButton: UIButton!
    
    private var selectedTab:Int!
    private var pageViewController:PageViewController!
    private var itemPageResponse:ItemPageResponse!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let userID = LoginController.shared.curretSigInUser.requestUserID()
        NetworkManger.shared.requestItems(forId: userID) { [weak self]  (success,itemPageResponse ) in
            if success {
                let value = itemPageResponse!.totalItems()
                self?.itemPageResponse = itemPageResponse
                DispatchQueue.main.async {
                    self?.numberOfItemsLabel.text = "\(value)"
                }
            }else{
                DispatchQueue.main.async {
                    self?.numberOfItemsLabel.text = "0"
                }
            }
        }
        
        NetworkManger.shared.requestUserWhoFollowMe(userID: userID)
        { [weak self] (success, socialPageResponse) in
            
            if success{
                let value = socialPageResponse!.totalRows.intValue
                DispatchQueue.main.async {
                    self?.nubmerOfFollowersLabel.text = "\(value)"
                }
            }else{
                DispatchQueue.main.async {
                    self?.nubmerOfFollowersLabel.text = "0"
                }
            }
        }
        
        NetworkManger.shared.requestWhoFollowByMe(userID: userID)
        { [weak self] (success, socialPageResponse) in
            if success{
                let value = socialPageResponse!.totalRows.intValue
                DispatchQueue.main.async {
                    self?.nubmerOfFollowersLabel.text = "\(value)"
                }
            }else{
                DispatchQueue.main.async {
                    self?.nubmerOfFollowersLabel.text = "0"
                }
            }
        }
        
        NetworkManger.shared.requestUserWhoFollowMe(userID: userID)
        { [weak self] (success, socialPageResponse) in
            if success{
                let value = socialPageResponse!.totalRows.intValue
                DispatchQueue.main.async {
                    self?.numberOfFollowingLabel.text = "\(value)"
                }
            }else{
                DispatchQueue.main.async {
                    self?.numberOfFollowingLabel.text = "0"
                }
            }
        }
        
        setupView()
        selectedTab = Constant.shared.TAG_USER_ALL_ITEMS
        updateCurrentSelectedPage()
    }
    
    private func setupView(){
        if let currentUser = LoginController.shared.curretSigInUser{
            navigationItem.title = currentUser.requestScreenName()
            profileNameLabel.text = currentUser.requestFristName() + " " + currentUser.requestLastName()
            profileScreenNameLabel.text = currentUser.requestScreenName()
            
            let photoPath = currentUser.requestCoverphoto()
            profileImage.layer.borderWidth = 1
            profileImage.layer.borderColor = UIColor.black.cgColor
            profileImage.layer.cornerRadius = profileImage.frame.height / 2
            
            Utility.loadImage(itemImageView: profileImage,
                              atPath: Constant.shared.kBaseURL + "/" + photoPath )
            
            requestUserItemlist()
        }
    }
    
    private func requestUserItemlist(){
        let VC =  pageViewController.orderedViewControllers[0] as! ItemCollectionViewController
         if let currentUser = LoginController.shared.curretSigInUser{
            VC.requestLoginUserItemList(userID: currentUser.requestUserID())
        }
    }
    
    private func requestUserFriendsClosetItemlist(){
        let VC = pageViewController.orderedViewControllers[1] as! ItemCollectionViewController
        if let currentUser = LoginController.shared.curretSigInUser{
            VC.requestLoginUserFriendClosset(userID: currentUser.requestUserID())
        }
    }
    
    private func updateCurrentSelectedPage(){
        updateTabBarSelection(tabBarIndex: selectedTab)
    }
    
    @IBAction func userSelectedAll(_ sender: Any) {
        selectedTab = Constant.shared.TAG_USER_ALL_ITEMS
        pageViewController.gotoPage(index: 0)
        updateCurrentSelectedPage()
        requestUserItemlist()
        
    }
    
    @IBAction func userSelectedFriendCloset(_ sender: Any) {
        selectedTab = Constant.shared.TAG_FRIENDS_CLOSET
        pageViewController.gotoPage(index: 1)
        updateCurrentSelectedPage()
        requestUserFriendsClosetItemlist()
    }
    
    private func updateTabBarSelection(tabBarIndex:Int){
        switch tabBarIndex {
        case Constant.shared.TAG_USER_ALL_ITEMS:
            firendsClosetButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
            friendsSelectedView.backgroundColor =  Constant.shared.kUnSelctedTabColor
            
            userAllButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.normal)
            allSelectedView.backgroundColor = Constant.shared.kTagBaseColor
            
        case Constant.shared.TAG_FRIENDS_CLOSET:
            userAllButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
            allSelectedView.backgroundColor = Constant.shared.kUnSelctedTabColor
            
            firendsClosetButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.normal)
            friendsSelectedView.backgroundColor = Constant.shared.kTagBaseColor
            
        default:
            firendsClosetButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
            friendsSelectedView.backgroundColor = UIColor.black
            
            userAllButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.normal)
            allSelectedView.backgroundColor = Constant.shared.kTagBaseColor
            
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "onPageViewController"{
            pageViewController = segue.destination as? PageViewController
            pageViewController.pageViewControllerDelegate = self
            pageViewController.addPageViewControlls(viewControllers: ["ItemCollectionView","ItemCollectionView"])
            pageViewController.setupPageViewControllTabIndex(viewControllerIndex: 0, pageIndex: Constant.shared.TAG_USER_ALL_ITEMS)
            pageViewController.setupPageViewControllTabIndex(viewControllerIndex: 1, pageIndex: Constant.shared.TAG_FRIENDS_CLOSET)
            pageViewController.setupFirstViewConroller()
        }
    }
}

extension ProfileViewController:PageViewControllerDelegate{
    func pageViewControllerHasChange(pageIndex: Int) {
        switch pageIndex {
        case Constant.shared.TAG_USER_ALL_ITEMS:
            selectedTab = Constant.shared.TAG_USER_ALL_ITEMS
            updateTabBarSelection(tabBarIndex: pageIndex)
            requestUserItemlist()
            
        case Constant.shared.TAG_FRIENDS_CLOSET:
            selectedTab = Constant.shared.TAG_FRIENDS_CLOSET
            updateTabBarSelection(tabBarIndex: pageIndex)
            requestUserFriendsClosetItemlist()
            
        default:
            selectedTab = Constant.shared.TAG_USER_ALL_ITEMS
            updateTabBarSelection(tabBarIndex: pageIndex)
            requestUserItemlist()
        }
    }
}

