//
//  RequestOrChatViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/22/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import BadgeSwift

class RequestOrChatViewController: BaseViewController {
    
    @IBOutlet weak var requestButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var requestViewBar: UIView!
    @IBOutlet weak var chatViewBar: UIView!
    @IBOutlet weak var numberofNewRequestLabel: BadgeSwift!
    @IBOutlet weak var numberofNewChatLabel: BadgeSwift!
    
    var cellTag = 0;
    var numberofChats = 0
    var numberofRequsets = 0
    var pageViewController:PageViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pageViewController.addPageViewControlls(viewControllers: ["RequestViewController","ChatViewController"])
        
        requestButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
        chatButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
        requestButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.selected)
        chatButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.selected)
        
        if cellTag == Constant.shared.TAG_REQUEST {
            pageViewController.setupPageViewControllTabIndex(viewControllerIndex: 0, pageIndex: Constant.shared.TAG_REQUEST)
        }else{
            pageViewController.setupPageViewControllTabIndex(viewControllerIndex: 0, pageIndex: Constant.shared.TAG_FRIEND_REQUEST)
        }
        
        pageViewController.setupPageViewControllTabIndex(viewControllerIndex: 1, pageIndex: Constant.shared.TAG_CHAT)
        
        updateViewCompoents()
    }
    
    private func updateViewCompoents(){
        switch cellTag {
        case Constant.shared.TAG_REQUEST,
             Constant.shared.TAG_FRIEND_REQUEST:
            requestButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.normal)
            requestViewBar.backgroundColor = Constant.shared.kTagBaseColor
            
            chatButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
            chatViewBar.backgroundColor = Constant.shared.kUnSelctedTabColor
            pageViewController.gotoPage(index: 0)
            
        case Constant.shared.TAG_CHAT:
            requestButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
            requestViewBar.backgroundColor = Constant.shared.kUnSelctedTabColor
            
            chatButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.normal)
            chatViewBar.backgroundColor = Constant.shared.kTagBaseColor
            pageViewController.gotoPage(index: 1)
        default:
            requestButton.setTitleColor(Constant.shared.kTagBaseColor, for: UIControl.State.normal)
            requestViewBar.backgroundColor = Constant.shared.kTagBaseColor
            
            chatButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
            chatViewBar.backgroundColor = UIColor.black
            pageViewController.gotoPage(index: 0)
        }
        
        if numberofRequsets > 0{
            numberofNewRequestLabel.text = "\(numberofRequsets)"
            numberofNewRequestLabel.isHidden = false
        }else{
            numberofNewRequestLabel.isHidden = true
        }
        
        if numberofChats > 0{
            numberofNewChatLabel.text = "\(numberofChats)"
            numberofNewChatLabel.isHidden = false
        }else{
            numberofNewChatLabel.isHidden = true
        }
    }
    
    @IBAction func userSelectedResquests(_ sender: Any) {
        if cellTag == Constant.shared.TAG_REQUEST {
            cellTag = Constant.shared.TAG_REQUEST
        }else{
            cellTag = Constant.shared.TAG_FRIEND_REQUEST
        }
        updateViewCompoents()
    }
    
    @IBAction func userSelectedChat(_ sender: Any) {
        cellTag = Constant.shared.TAG_CHAT
        updateViewCompoents()
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "onRequestOrChatPageView"{
            pageViewController = segue.destination as? PageViewController
            pageViewController.setupFirstViewConroller()
        }
    }
}
