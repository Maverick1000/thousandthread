//
//  ChatMessageViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/25/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import IoniconsKit

class ChatMessageViewController: UIViewController {
    
    @IBOutlet weak var pictureStackView: UIStackView!
    @IBOutlet weak var userSelectedCamera: UIImageView!
    @IBOutlet weak var chatmessageTableview: UITableView!
    @IBOutlet weak var messageToSend: CustomTextInputView!
    @IBOutlet weak var userSelectedSendButton: UIButton!
    @IBOutlet weak var collectionViewImage: UICollectionView!
    @IBOutlet weak var chatMessageBarHeightConstant: NSLayoutConstraint!
    
    var isPosting:Bool!
    var imagePostingIndex = 0
    var imagePostingCurrent = 0
    var senderID:Int!
    var imageData = [Data]()
    var recipientID:Int!
    var chatUserProfie:UserProfile!
    
    var postItemCameraViewController:PostItemCameraViewController!
    var chatMessageSetResponse:[ChatMessageSetResponse]!
    var pictureTakenImageData:[URL]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isPosting = false
        
        userSelectedSendButton.layer.borderColor = UIColor.black.cgColor
        userSelectedSendButton.layer.borderWidth = 1
        userSelectedSendButton.layer.cornerRadius = 5
        
        if chatMessageSetResponse == nil {
            NetworkManger.shared.requestUser(userID:recipientID, completionHandler: { [weak self] (success, profile) in
                self?.chatUserProfie = profile
                self?.navigationItem.title = self?.chatUserProfie.requestUserName()
                self?.getMessageSet()
            })
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(IncomingMessage(notification :)),
                                               name: Constant.shared.kPostIncomingMessage, object:  nil)
        userSelectedCamera.image =  UIImage.ionicon(with: .iosCamera,
                                                    textColor: UIColor.black, size: CGSize(width: 30, height: 30))
        userSelectedCamera.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userTouchCamera)))
        
        chatMessageBarHeightConstant.constant = 0
        
        if postItemCameraViewController != nil{
            if pictureTakenImageData != nil {
                pictureTakenImageData.removeAll()
            }
            
            pictureTakenImageData = Utility.requestPostAddImageFilePaths()
            chatMessageBarHeightConstant.constant = 173
            collectionViewImage.reloadData()
        }
    }
    
    @IBAction func userSelectedSendMessage(_ sender: Any) {
        isPosting = true
        imagePostingCurrent = 0
        let newChatMeesageSetResponse = ChatMessageSetResponse()
        
        if let message = messageToSend.requetTextField().text{
            if message.isEmpty == false{
                
                let newPostMessage = PostMessage()
                newPostMessage.sender_id = NSNumber(value: LoginController.shared.curretSigInUser.requestUserID())
                newPostMessage.recipient_id = NSNumber(value: recipientID)
                newPostMessage.type = Constant.shared.kMessage_Chat
                newPostMessage.body = message
                
                newChatMeesageSetResponse.body = message
                newChatMeesageSetResponse.sender_id = NSNumber(value: LoginController.shared.curretSigInUser.requestUserID())
                newChatMeesageSetResponse.recipient_id = NSNumber(value: recipientID)
                newChatMeesageSetResponse.imagePaths = [String]()
                
                chatMessageSetResponse.append(newChatMeesageSetResponse)
                
                if pictureTakenImageData != nil{
                    
                    for imagePath in pictureTakenImageData {
                        do {
                            let imageBytes = try Data(contentsOf: imagePath)
                            imageData.append(imageBytes)
                            imagePostingIndex += 1
                        }catch{
                            
                        }
                    }
                }
                
                let indexPath = IndexPath(row: chatMessageSetResponse.count - 1, section: 0)
                chatmessageTableview.insertRows(at: [indexPath], with: .bottom)
                scrollToBottom()
                
                DispatchQueue.main.async {
                    NetworkManger.shared.postMessage(messsage: newPostMessage, imageData: self.imageData) { [weak self] (success) in
                        self?.chatMessageBarHeightConstant.constant = 0
                        Utility.cleanUpDocumentImageDir()
                        Utility.imageIndex = 0
                    }
                }
            }
        }
    }
    
    @objc func userTouchCamera(gestureRecognizer:UITapGestureRecognizer){
        postItemCameraViewController =  Utility.requestViewController(identifier:
            "postItemCameraViewController", storyborad: "HomeNav") as? PostItemCameraViewController
        present(postItemCameraViewController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    private func getMessageSet(){
        //let currentUserID = LoginController.shared.curretSigInUser.requestUserID()
        NetworkManger.shared.requestMessageSet(sender_id: senderID, recipient_id: recipientID) { [weak self] (success, chatMessageSetResponse) in
            if success {
                self?.chatMessageSetResponse = chatMessageSetResponse
                self?.chatmessageTableview.reloadData()
                self?.scrollToBottom()
            }
        }
    }
    
    private func scrollToBottom(){
        let indexPath = IndexPath(row: chatMessageSetResponse.count - 1, section: 0)
        self.chatmessageTableview.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    @objc private func IncomingMessage(notification: NSNotification){
        let messageDic = notification.userInfo as! [String:Any]
        let newChatMeesageSetResponse = ChatMessageSetResponse()
        
        newChatMeesageSetResponse.id = NSNumber(value:messageDic["id"] as! Int)
        newChatMeesageSetResponse.body = messageDic["body"] as? String
        newChatMeesageSetResponse.sender_id = NSNumber(value:(messageDic["sender_id"] as! NSString).intValue)
        newChatMeesageSetResponse.ad_id = NSNumber(value:messageDic["ad_id"] as! Int)
        newChatMeesageSetResponse.recipient_id = NSNumber(value:(messageDic["recipient_id"] as! NSString).intValue)
        newChatMeesageSetResponse.imagePaths = messageDic["imagePaths"] as? [String]
        newChatMeesageSetResponse.message_set_id = NSNumber(value:messageDic["message_set_id"] as! Int)
        
        chatMessageSetResponse.append(newChatMeesageSetResponse)
        
        let indexPath = IndexPath(row: chatMessageSetResponse.count - 1, section: 0)
        chatmessageTableview.insertRows(at: [indexPath], with: .bottom)
        
        scrollToBottom()
    }
}

extension ChatMessageViewController:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension ChatMessageViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if chatMessageSetResponse != nil {
            count = chatMessageSetResponse.count
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        
        let item = chatMessageSetResponse[indexPath.row]
        
        if item.recipient_id.intValue == LoginController.shared.curretSigInUser.requestUserID(){
            if item.imagePaths.count > 0 {
                cell = createFromPicCell(tableView: tableView, profileImagePath: chatUserProfie.requestCoverphoto(), imageData: [Data()],
                                         imagePicPath:item.imagePaths, message: item.body ?? "", indexPath: indexPath)
            }else{
                cell = createfromCell(tableView:tableView, profileImagePath: chatUserProfie.requestCoverphoto(),
                                      message: item.body ?? "", indexPath:indexPath)
            }
            
        }else{
            let myPhoto = LoginController.shared.curretSigInUser.requestCoverphoto()
            if item.imagePaths.count > 0 ||  imageData.count > 0{
                cell = creatToPicCell(tableView: tableView, profileImagePath: chatUserProfie.requestCoverphoto(),
                                      imageData: imageData, imagePicPath: item.imagePaths,
                                      message: item.body ?? "", indexPath: indexPath)
            }else{
                cell = createToCell(tableView:tableView, profileImagePath: myPhoto, message: item.body ?? "", indexPath:indexPath)
            }
        }
        
        return cell
    }
    
    func createFromPicCell(tableView:UITableView, profileImagePath:String,imageData:[Data],imagePicPath:[String],
                           message:String, indexPath:IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "fromImages", for: indexPath) as! ImageTableViewCell
        cell.setupCell(profileImagePath: profileImagePath, imagePic: imagePicPath, imageData:imageData, message: message)
        return cell
    }
    
    func creatToPicCell(tableView:UITableView, profileImagePath:String, imageData:[Data], imagePicPath:[String],
                        message:String, indexPath:IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "toImages", for: indexPath) as! ImageTableViewCell
        cell.setupCell(profileImagePath: profileImagePath, imagePic: imagePicPath, imageData:imageData, message: message)
        return cell
    }
    
    func createToCell(tableView:UITableView, profileImagePath:String, message:String, indexPath:IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "post", for: indexPath) as! PostTableViewCell
        cell.setupCell(profileImagePath: profileImagePath, message: message)
        return cell
    }
    
    func createfromCell(tableView:UITableView, profileImagePath:String, message:String, indexPath:IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "from", for: indexPath) as! FromTableViewCell
        cell.setupCell(profileImagePath: profileImagePath, message: message)
        return cell
    }
}

extension ChatMessageViewController:UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}


extension ChatMessageViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        if pictureTakenImageData != nil {
            count = pictureTakenImageData.count
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! CollectionViewCellImage
        cell.setupCell(imagePath: pictureTakenImageData[indexPath.row])
        return cell
    }
}

