//
//  NotificationsViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 6/26/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseViewController {
    
    @IBOutlet weak var notificaitonTableView: UITableView!
    
    var userProfile:UserProfile!
    var numberOfRequest = [Int]()
    var data = ["Listing Requests","Chat Messages","","Order Status","Friend Requests"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        numberOfRequest = [Int]()
        
        userProfile = LoginController.shared.curretSigInUser
        NetworkManger.shared.requestCurrentNotifications(id: userProfile.requestUserID()) { [weak self] (success, notification) in
            
            self?.numberOfRequest.append(notification?.requestNewListings() ?? 0)
            self?.numberOfRequest.append(notification?.requestNewChatTotal() ?? 0)
            self?.numberOfRequest.append(0)
            self?.numberOfRequest.append(notification?.requestNewOrderStatusTotal() ?? 0)
            self?.numberOfRequest.append(notification?.requestNewFriendsTotal() ?? 0)
            self?.notificaitonTableView.reloadData()
        }
        
        if let index = notificaitonTableView.indexPathForSelectedRow{
            notificaitonTableView.deselectRow(at: index, animated: false)
        }
        
        screenTapGestureRecognizer.delegate = self
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RequestORStatus" {
            let selectedCell = sender as! NotificationTableViewCell
            let VC = segue.destination as! RequestOrChatViewController
            if numberOfRequest.count > 0 {
                if selectedCell.tag ==  Constant.shared.TAG_REQUEST{
                    VC.numberofRequsets = numberOfRequest[0]
                }else{
                    VC.numberofRequsets = numberOfRequest[4]
                }
                VC.numberofChats = numberOfRequest[1]
                VC.cellTag = selectedCell.tag
            }
        }
    }
}

extension NotificationsViewController:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
}

extension NotificationsViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 49
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCells", for: indexPath) as! NotificationTableViewCell
        
        if numberOfRequest.count == 0{
            cell.setupCell(notification: data[indexPath.row], numberOfRequest:0)
        }else{
            cell.setupCell(notification: data[indexPath.row],
                           numberOfRequest:numberOfRequest[indexPath.row])
        }
        
        setCellTag(index: indexPath.row, cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        performSegue(withIdentifier: "RequestORStatus", sender: cell)
    }
    
    private func setCellTag(index:Int, cell:NotificationTableViewCell){
        switch index {
        case 0:
            cell.tag =  Constant.shared.TAG_REQUEST
        case 1:
            cell.tag =  Constant.shared.TAG_CHAT
        case 3:
            cell.tag =  Constant.shared.TAG_MONEYP_TRAS
        case 4:
            cell.tag =  Constant.shared.TAG_FRIEND_REQUEST
            
        default:
            cell.tag = 0
        }
    }
}

extension NotificationsViewController:UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
        return false
    }
}
