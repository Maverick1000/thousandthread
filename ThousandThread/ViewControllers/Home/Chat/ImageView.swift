//
//  ImageView.swift
//  ThousandThread
//
//  Created by Paul Walker on 9/2/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class ImageView : UIView{
  
    @IBOutlet weak var imagePicView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        let view = loadViewFromNib()
        addSubview(view!)
        view?.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: -15))
        addConstraint(NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0))
    }
    
    private func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ImageView", bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
    public func loadImage(atPath:String){
        imagePicView.contentMode = .scaleAspectFill
        Utility.loadImage(atPath: Constant.shared.kBaseURL + "/" + atPath) {[weak self] (success, image) in
            if success{
                self?.imagePicView.image = image
                //self?.imagePicView.sizeToFit()
            }
        }
    }
    
    public func loadImageData(imageData:Data){
        self.imagePicView.image = UIImage(data: imageData)
    }
    
}
