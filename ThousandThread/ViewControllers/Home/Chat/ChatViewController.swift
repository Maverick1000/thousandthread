//
//  ChatViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 5/26/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class ChatViewController: BaseViewController {
    
    @IBOutlet weak var chatsTableView: UITableView!
    var messageResponse:MessageResponse!
    var messageRowResponse = [MessageRowResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getChatMessages()
    }
    
    private func getChatMessages(){
        let userID = LoginController.shared.curretSigInUser.requestUserID()
        NetworkManger.shared.requestMessageChat(id: userID) {[weak self] (succes, messageResponse) in
            if(succes){
                self?.messageResponse = messageResponse
                self?.messageRowResponse = (messageResponse?.requestItems())!
                self?.chatsTableView.reloadData()
            }else{
                
            }
        }
    }
    
    private func listForNewMessages(){
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "onCellSelection"{
            let destionation = segue.destination as! ChatMessageViewController
         
            if let row = chatsTableView.indexPathForSelectedRow?.row{
                let selectededRow = messageRowResponse[row]
                let ID = LoginController.shared.curretSigInUser.requestUserID()
                
                if selectededRow.requestUserOneID() == ID{
                    destionation.senderID = selectededRow.requestUserOneID()
                    destionation.recipientID = selectededRow.requestUserTwoID()
                }else{
                    destionation.senderID = selectededRow.requestUserTwoID()
                    destionation.recipientID = selectededRow.requestUserOneID()
                }
            }
        }
    }
}

extension ChatViewController:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension ChatViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard messageRowResponse.count != 0 else {
            return 0
        }
        return messageRowResponse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellData:ChatMessage!
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "chat", for:indexPath) as! ChatTableViewCell
        let item = messageRowResponse[indexPath.row]
        
        let id = LoginController.shared.curretSigInUser.requestUserID()
        if item.requestUserOneID() != id {
            cellData = ChatMessage(profileName: item.requestUserName1(),
                                   profileImageName: item.requesUser1Photo1(), message: item.requestBody(),
                                   howOldMessage: item.requestHowOldIsMessage())
            
        }else{
            cellData = ChatMessage(profileName: item.requestUserName2(),
                                   profileImageName: item.requesUser2Photo1(), message: item.requestBody(),
                                   howOldMessage: item.requestHowOldIsMessage())
            
        }
        cell.setupCell(cellData: cellData)
        return cell
    }
}
