//
//  RequestViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 8/1/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class RequestViewController: UIViewController {
    
    @IBOutlet weak var requestTableView: UITableView!
    
    var requestViewType:Int!
    var items = [MessageRowResponse]()
    var messageResponse:MessageResponse!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        items.removeAll()
        if requestViewType == Constant.shared.TAG_REQUEST{
            getRentRequest()
        }else{
            getFriendsRequest()
        }
    }
    
    private func getFriendsRequest(){
        let userID = LoginController.shared.curretSigInUser.requestUserID()
        NetworkManger.shared.requestMessageFriend(id: userID) {[weak self] (succes, messageResponse) in
            if(succes){
                self?.messageResponse = messageResponse
                if let requestitems = messageResponse?.requestItems(){
                    self?.items.append(contentsOf: requestitems)
                    self?.requestTableView.reloadData()
                }
            }else{
                
            }
        }
    }
    
    private func getRentRequest(){
        let userID = LoginController.shared.curretSigInUser.requestUserID()
        NetworkManger.shared.requestMessageAD(id: userID) {[weak self] (succes, messageResponse) in
            if(succes){
                self?.messageResponse = messageResponse
                if let requestitems = messageResponse?.requestItems(){
                    self?.items.append(contentsOf: requestitems)
                    self?.requestTableView.reloadData()
                }
                self?.requestTableView.reloadData()
            }else{
                
            }
        }
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "onCellSelection"{
            let destionation = segue.destination as! ChatMessageViewController
            //            let items = messageResponse.requestItems()
            //
            //            if let row = requestTableView.indexPathForSelectedRow?.row{
            //                destionation.messageRowResponse = items[row]
            //            }
        }
    }
}

extension RequestViewController:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension RequestViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard messageResponse != nil else {
            return 0
        }
        return messageResponse.requestPageRowCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellData:Requests!
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "friendRequestCell", for:indexPath) as! FriendRequestTableViewCell
        //let items = messageResponse.requestItems()
        
        let item = items[indexPath.row]
        
        let id = LoginController.shared.curretSigInUser.requestUserID()
        if item.requestUserOneID() == id {
            if requestViewType == Constant.shared.TAG_FRIEND_REQUEST{
                cellData = Requests(profileImageString:
                    item.requesUser2Photo1(), requestMessage: requestFriendsMessage(item:item))
            }else{
                cellData = Requests(profileImageString:
                    item.requesUser1Photo1(), requestMessage: requestFriendsMessage(item:item))
            }
        }else{
            if requestViewType == Constant.shared.TAG_FRIEND_REQUEST{
                cellData = Requests(profileImageString:
                    item.requesUser1Photo1(), requestMessage: requestFriendsMessage(item:item))
            }else{
                cellData = Requests(profileImageString:
                    item.requesUser1Photo1(), requestMessage: requestFriendsMessage(item:item))
            }
        }
        
        cell.setupCell(requests: cellData)
        
        return cell
    }
    
    private func requestFriendsMessage(item:MessageRowResponse) ->String{
        var message = ""
        message = "\(item.requestBody()) | \(item.requestHowOldIsMessage())"
        return message
        
    }
}
