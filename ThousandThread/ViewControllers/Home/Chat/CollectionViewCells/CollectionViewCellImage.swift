//
//  CollectionViewCellImage.swift
//  ThousandThread
//
//  Created by Paul Walker on 9/6/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class CollectionViewCellImage: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    func setupCell(imagePath:URL){
        do{
            let imageBytes = try Data(contentsOf: imagePath)
            if let image = UIImage(data: imageBytes) {
                imageView.image = image
            }
        }catch{
            
        }
    }
}
