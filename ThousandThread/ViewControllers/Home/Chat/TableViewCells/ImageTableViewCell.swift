//
//  ImageTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/25/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var imageStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        for view in imageStackView.arrangedSubviews{
            imageStackView.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setupCell(profileImagePath:String, imagePic:[String],
                   imageData:[Data], message:String){
        
        profileImage.layer.cornerRadius = profileImage.frame.width / 2
        
        if imagePic.count > 0 {
            Utility.loadImage(itemImageView: profileImage, atPath:
                Constant.shared.kBaseURL + "/" + profileImagePath)
            for imagePath in imagePic {
                let view = ImageView(frame: CGRect.zero)
                view.heightAnchor.constraint(equalToConstant: 200).isActive = true
                self.imageStackView.addArrangedSubview(view)
                
                view.loadImage(atPath: imagePath)
                
                //        let label = UILabel(frame: CGRect.zero)
                //        label.text = message
                //        label.sizeToFit()
                //        imageStackView.addArrangedSubview(label)
                
                //}
            }
            
        }else{
            
            for data in imageData {
                let view = ImageView(frame: CGRect.zero)
                view.heightAnchor.constraint(equalToConstant: 150).isActive = true
                self.imageStackView.addArrangedSubview(view)
                view.loadImageData(imageData: data)
            }
        }
    }
}

