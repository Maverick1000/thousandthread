//
//  ChatTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/22/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var chatUserImageView: UIImageView!
    @IBOutlet weak var chatUserProfileName:UILabel!
    
    @IBOutlet weak var howOldMessageLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(cellData:ChatMessage){
       
        messageLabel.text = cellData.message
        chatUserImageView.layer.cornerRadius = chatUserImageView.frame.width / 2
        Utility.loadImage(itemImageView: chatUserImageView,
                          atPath: Constant.shared.kBaseURL + "/" + cellData.profileImageName)
        chatUserProfileName.text = cellData.profileName
        howOldMessageLabel.text = cellData.howOldMessage
    }

}
