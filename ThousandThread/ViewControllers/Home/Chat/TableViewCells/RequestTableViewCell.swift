//
//  RequestTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 8/1/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class RequestTableViewCell: UITableViewCell {

    @IBOutlet weak var requestMessage: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setupCell(requests:Requests){
        profileImageView.layer.cornerRadius = profileImageView.frame.width / 2
        Utility.loadImage(itemImageView: profileImageView,
                          atPath:  Constant.shared.kBaseURL + "/" + requests.profileImageString)
        requestMessage.text = requests.requestMessage
    }
}
