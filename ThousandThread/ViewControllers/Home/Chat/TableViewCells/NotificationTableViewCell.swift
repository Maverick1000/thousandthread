//
//  NotificationTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 6/29/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import BadgeSwift

class NotificationTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var notificaitonnRequest: BadgeSwift!
    
    private var cellTag = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func setupCell(notification:String, numberOfRequest:Int){
        if numberOfRequest == 0{
            notificaitonnRequest.isHidden = true
        }else{
            notificaitonnRequest.isHidden = false
        }
        notificationLabel.text = notification
        notificaitonnRequest.text = "\(numberOfRequest)"
    }
    
    
}
