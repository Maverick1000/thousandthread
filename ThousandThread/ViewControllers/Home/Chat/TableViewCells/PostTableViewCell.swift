//
//  PostTableViewCell.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/25/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCell(profileImagePath:String, message:String){
        userImageView.layer.cornerRadius = userImageView.frame.height / 2 
        Utility.loadImage(itemImageView: userImageView, atPath:
                            Constant.shared.kBaseURL + "/" + profileImagePath)
        messageLabel.text = message
        
    }
}
