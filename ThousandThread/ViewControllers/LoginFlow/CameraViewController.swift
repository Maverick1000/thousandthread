//
//  CameraViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/25/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import Vision
import AVFoundation

class CameraViewController: UIViewController {
    
    private var countDownTime = 3
    private var startCountDown = false
    private var videoOutput: AVCaptureVideoDataOutput!
    private var photoOutput: AVCapturePhotoOutput!
    private var captureSession:AVCaptureSession!
    private var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    private var videoCaptureDevice: AVCaptureDevice?
    private var input: AnyObject?
    
    private let context = CIContext()
    private var countDownTimer:Timer!
    private var confidence = CapturedConfidence()
    
    weak var delegate:ProfilePicViewControllerDelegate!
    private var avSession: AVCaptureSession?
    private var sessionQueue: DispatchQueue!
    private var stillPhotoCallback: ((CIImage?) -> Void)? = nil
    
    @IBOutlet weak var picSelfCountDown: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        print("CameraViewController:deinit")
    }
    
    func startFiltering() {
        // Create a session if we don't already have one
        if avSession == nil {
            do {
                avSession = try createAVSession()
            } catch {
                print(error)
            }
        }
        // And kick it off
        sessionQueue.async {
            self.avSession?.startRunning()
        }
    }
    
    func stopFiltering() {
        // Stop the av session
        avSession?.stopRunning()
        
        // Remove all AVCaptureMetadataOutput for barcode
        for metadataOutput in avSession?.outputs ?? [] {
            if metadataOutput is AVCaptureMetadataOutput {
                avSession?.removeOutput(metadataOutput)
            }
        }
    }
    
    private func createAVSession()  throws -> AVCaptureSession{
        
        let session = AVCaptureSession()
        
        if let videoCaptureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front){
            do {
                input = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                print("video device error")
            }
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session )
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            view.layer.addSublayer(videoPreviewLayer!)
            session.addInput(input as! AVCaptureInput)
            
            sessionQueue = DispatchQueue(label: "AVSessionQueue", attributes: [])
            
            videoOutput = AVCaptureVideoDataOutput()
            videoOutput.videoSettings = [ kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
            videoOutput.alwaysDiscardsLateVideoFrames = true
            videoOutput.setSampleBufferDelegate(self, queue: sessionQueue)
            session.addOutput(videoOutput)
            
            photoOutput = AVCapturePhotoOutput()
            session.addOutput(photoOutput)
        }
        return session
    }
    
    override func viewDidLayoutSubviews() {
        videoPreviewLayer?.frame = view.layer.bounds
    }
    
    private func imageFromSampleBuffer(sampleBuffer: CMSampleBuffer) -> CIImage? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
        let ciImage = CIImage(cvPixelBuffer: imageBuffer)
        return ciImage
    }
    
    private func takeSelfie() {
        
        captureStillPhoto({ [weak self] (image) in
            guard let detectedImage = image else {
                return
            }
            
            let imageProfile = UIImage.from(ciImage: detectedImage)
            imageProfile?.saveToDocuments(filename: Constant.shared.kPortraitmage, ext: "jpg")
            NetworkManger.shared.getImageData().uploadProfilePic(profile: NetworkManger.shared.getUserProfile(),
                                                                 completionHandler: { (succuss) in
                print("Image Prortait image was uploading.")
            })
            self?.stopFiltering()
        })
    }
    
    private func captureStillPhoto(_ callback: @escaping (_ image: CIImage?) -> Void) {
        if let photoOutputConnection = photoOutput.connection(with: .video) {
            photoOutputConnection.videoOrientation = .landscapeLeft
        }
        
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]
        
        settings.flashMode = .off
        settings.isAutoStillImageStabilizationEnabled = true
        settings.previewPhotoFormat = previewFormat
        photoOutput.capturePhoto(with: settings, delegate: self)
        stillPhotoCallback = callback
    }
    
    private func captureSelfPic(image: CIImage){
        
        if startCountDown == false{
            let requestHandler = VNImageRequestHandler(ciImage: image, orientation: CGImagePropertyOrientation.up, options: [:])
            try? requestHandler.perform([VNDetectFaceLandmarksRequest(completionHandler: { (request, error) in
                guard let observations = request.results as? [VNFaceObservation],
                    let face = observations.first?.landmarks,
                    let medianLine = face.medianLine?.normalizedPoints.first
                    else {
                        self.confidence.updateFace(false)
                        return
                }
                
                self.confidence.updateFace(true)
                if self.confidence.face == 1.0 && medianLine.x > 0.23 && medianLine.x < 0.55 {
                    DispatchQueue.main.async { [weak self] in
                        if self?.countDownTimer == nil {
                            self?.countDownTimer = Timer(timeInterval: 0.4, target: self!, selector: #selector(self?.updateLabel), userInfo: nil, repeats: true)
                            RunLoop.current.add((self?.countDownTimer)!, forMode: RunLoop.Mode.common)
                        }
                        self?.startCountDown = true
                    }
                }
            })])
        }
    }
    
    @objc private func updateLabel(){
        countDownTime -= 1
        if countDownTime < 0{
            countDownTimer.invalidate()
            delegate?.hideCountDownerTimer()
            takeSelfie()
        } else {
            delegate.updateCountDownLabel(value: "\(countDownTime)")
        }
    }
}

extension CameraViewController: AVCaptureVideoDataOutputSampleBufferDelegate{
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let ciImage = imageFromSampleBuffer(sampleBuffer: sampleBuffer) else { return }
        DispatchQueue.main.async { [unowned self] in
            self.captureSelfPic(image: ciImage)
        }
    }
}

extension CameraViewController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard error == nil,
            let photoCallback = stillPhotoCallback,
            let imageData = photo.fileDataRepresentation(),
            let image = CIImage(data: imageData)
            else {
                print("error occur: \(error?.localizedDescription ?? "")")
                self.stillPhotoCallback?(nil)
                self.stillPhotoCallback = nil
                return
        }
        let imagePortrait = image.oriented(.right)
        photoCallback(imagePortrait)
        stillPhotoCallback = nil
    }
}

