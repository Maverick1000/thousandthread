//
//  PasswordRulesViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/20/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class PasswordRulesViewController: UIViewController {
    
    @IBOutlet var rulesView: [UIView]!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        setRoundedCorners()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    public func updatePasswordRules(passwordText:String){
        checkMinMaxCharactorCount(text: passwordText)
        checkforLowerAndUpper(text: passwordText)
        checkOneNumberAndSpeicalCharacter(text: passwordText)
    }
    
    private func checkMinMaxCharactorCount(text:String){
        if text.count == Constant.shared.kMinCharacterCount || text.count == Constant.shared.kMaxCharacterCount{
            rulesView[0].backgroundColor = UIColor.green
        }else{
            rulesView[0].backgroundColor = UIColor.red
        }
    }
    
    private func checkforLowerAndUpper(text:String){
        if Utility.matches(for: Constant.shared.kLowwerAndUpperCharacter, in: text).count == 1{
            rulesView[1].backgroundColor = UIColor.green
            rulesView[2].backgroundColor = UIColor.green
        }else{
            rulesView[1].backgroundColor = UIColor.red
            rulesView[2].backgroundColor = UIColor.red
        }
    }
    
    private func checkOneNumberAndSpeicalCharacter(text:String){
        if Utility.matches(for: Constant.shared.kOneNumberAndOneSpecialCharacter, in: text).count == 1{
            rulesView[3].backgroundColor = UIColor.green
        }else{
            rulesView[3].backgroundColor = UIColor.red
        }
    }
    
    private func setRoundedCorners(){
        view.layer.cornerRadius = 10.0
        for view in rulesView{
            view.layer.cornerRadius = view.frame.height / 2
        }
    }
}
