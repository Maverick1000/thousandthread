//
//  LoginViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/12/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var username: CustomTextInputView!
    @IBOutlet weak var password: CustomTextInputView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var needAccountButton: UIButton!
    
    deinit {
        print("LoginViewController:deinit")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        password.isSecureTextEntry(isSecureTextEntry: true)
        
        loginButton.layer.borderWidth = 2
        loginButton.layer.borderColor = Constant.shared.kTagBaseColor.cgColor
    
        setSignUpButtonText()
        
    }
    
    @IBAction func userSelectedLoginButton(_ sender: Any) {

        LoginController.shared.DoUserLogin(usernme: username.requetTextField().text!,
                                           userPassword: password.requetTextField().text!) { (success) in
                if success{
                    if LoginController.shared.curretSigInUser == nil {
                        LoginController.shared.getUserProfile(completionHandler: { (success) in
                            if success{
                                 Constant.shared.appDelegate.MainViewAnimation()
                            }
                        })
                    }
                }else{
                    AlertController.alertShard.showAlert(title: "Login Error", message: "Please retype your username or password.")
            }
        }
    }
    
    private func setSignUpButtonText(){
        let string1 =  "don’t have an account?"
        let string2 =  " sign up."
        
        let atrString1 = NSMutableAttributedString(string: string1, attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
        let atrString2 = NSMutableAttributedString(string: string2,attributes: [NSAttributedString.Key.foregroundColor: Constant.shared.kTagBaseColor])
        let fianlAtriubte = NSMutableAttributedString(attributedString: atrString1)
        fianlAtriubte.append(atrString2)
        needAccountButton.setAttributedTitle(fianlAtriubte, for: .normal)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
