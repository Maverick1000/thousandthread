//
//  ProfilePicViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/25/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

protocol ProfilePicViewControllerDelegate:class {
    func hideCountDownerTimer()
    func updateCountDownLabel(value:String)
}

class ProfilePicViewController: BaseViewController {
    
    @IBOutlet weak var picAddButton: UIButton!
    @IBOutlet weak var picContainterView: UIView!
    @IBOutlet weak var picCountDownTimer: UILabel!

    weak var cameraViewController:CameraViewController!
    
    deinit {
        print("ProfilePicViewController:deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        picContainterView.layer.masksToBounds = true
        picContainterView.layer.cornerRadius = picContainterView.frame.height / 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "profile picture"
    }
    
    @IBAction func UserSelectAddProfilePicButton(_ sender: Any) {
        picAddButton.isHidden = true
        picContainterView.isHidden = false
        picCountDownTimer.isHidden = false
        cameraViewController.startFiltering()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "cameraViewController"{
            cameraViewController = segue.destination as? CameraViewController
            cameraViewController.delegate = self
        }
    }
    
    @IBAction func userSelectedSkipThisStep(_ sender: Any) {
        Constant.shared.appDelegate.MainViewAnimation()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ProfilePicViewController:ProfilePicViewControllerDelegate{
    func updateCountDownLabel(value:String){
        self.picCountDownTimer.text = value
    }
    
    func hideCountDownerTimer(){
        self.picCountDownTimer.isHidden = true
    }
}
