//
//  LocationTimeViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/12/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class LocationTimeViewController: BaseViewController {
    
    @IBOutlet weak var addTimeTableView: UITableView!
    @IBOutlet weak var cityTextField: CustomTextInputView!
    @IBOutlet weak var stateTextField: CustomTextInputView!
    @IBOutlet weak var communityTextField: CustomTextInputView!
    
    private var addedTimeCells = [AddedTimeCelll]()
    private var userAvailability = [UserAvailability]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cityTextField.setTextFieldDelegate(delegate: self)
        stateTextField.setTextFieldDelegate(delegate: self)
        communityTextField.setTextFieldDelegate(delegate: self)
        
        //registerNib()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //addTimeTableView.reloadData()
        
        navigationItem.title = "location and times"
        
        //        cityTextField.layer.borderWidth = 1
        //        cityTextField.layer.borderColor = UIColor.black.cgColor
        //
        //        stateTextField.layer.borderWidth = 1
        //        stateTextField.layer.borderColor = UIColor.black.cgColor
        //
        //        communityTextField.layer.borderWidth = 1
        //        communityTextField.layer.borderColor = UIColor.black.cgColor
        
    }
    
    @IBAction func processNewUserProfile(_ sender: Any) {
        if Utility.isTextFieldEmpty(textField: cityTextField.requetTextField()) &&
            Utility.isTextFieldEmpty(textField: stateTextField.requetTextField()){
            AlertController.alertShard.showAlert(title: "Error", message: "Please enter your City and State.")
        }else{
            updateUserProfile()
            
        }
    }
    
    deinit {
        print("LocationTimeViewController:deinit")
    }
    
    //    private func registerNib(){
    //        addTimeTableView.estimatedRowHeight = 20
    //        addTimeTableView.rowHeight = UITableView.automaticDimension
    //    }
    
    //    private func requestPickerView(name:String) ->UIView{
    //        let view = Bundle.main.loadNibNamed(name, owner: self, options: nil)?.first as! UIView
    //        return view
    //    }
    
    //    private func setupDelegate(cell:TimeAddedCell){
    //        cell.setLocationTimeViewController(delagate: self)
    //    }
    //
    //    private func setupCellWithPickerView(cell:TimeAddedCell){
    //        let weekdayView = requestPickerView(name:"WeekdayView") as! WeekdayView
    //        weekdayView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 260)
    //        weekdayView.setWeekDayDelegate(delegate: cell)
    //        weekdayView.setupData(data: Utility.requestWeekDays())
    //
    //        cell.setWeekDayAccessoryView(view:weekdayView)
    //    }
    //
    //    private func setupCellWithDatePickerView(cell:TimeAddedCell){
    //        let datePickerView = requestPickerView(name:"DatePickerView") as! DatePickerView
    //        datePickerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 260)
    //        datePickerView.setDatePickerDelegate(delegate: cell)
    //        cell.setAvailableTimeTextFieldccessoryView(view:datePickerView)
    //    }
    
    private func updateUserProfile(){
        NetworkManger.shared.getUserProfile().updateCity(city: cityTextField.requetTextField().text ?? "")
        NetworkManger.shared.getUserProfile().updateState(state: stateTextField.requetTextField().text ?? "")
        NetworkManger.shared.getUserProfile().updateCmmunity(community: communityTextField.requetTextField().text ?? "")
        
        let id = NetworkManger.shared.getUserProfile().requestUserID()
        NetworkManger.shared.updateUser(userID: id, profile: NetworkManger.shared.getUserProfile()) { [weak self] (success) in
            if success {
                self?.performSegue(withIdentifier: "payment", sender: self)
            }else{
                AlertController.alertShard.showAlert(title: "Error", message:"There was an issue with updating your user account. " +
                    "Please try again in a few minutes.")
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

//extension LocationTimeViewController:UITableViewDataSource{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 1{
//            return 1
//        }
//        return addedTimeCells.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        var cell:BaseTableViewCell!
//
//        if indexPath.section == 0{
//            cell = tableView.dequeueReusableCell(withIdentifier: "TimeAddedCell", for: indexPath) as! TimeAddedCell
//            setupCellWithPickerView(cell: cell as! TimeAddedCell)
//            setupCellWithDatePickerView(cell: cell as! TimeAddedCell)
//        }else{
//            cell = tableView.dequeueReusableCell(withIdentifier: "AddedTimeCell", for: indexPath) as! AddedTimeCelll
//            cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userRequetAddedTimeCell(gesture:))))
//        }
//
//        cell.selectionStyle = .none
//        cell.setCellIndexPath(indexPath: indexPath)
//        cell.setLocationTimeViewController(delagate: self)
//        return cell
//    }
//
//    @objc private func userRequetAddedTimeCell(gesture:UITapGestureRecognizer){
//        addedTimeCells.append(AddedTimeCelll())
//        addTimeTableView.reloadData()
//    }
//}
//
//extension LocationTimeViewController:UITableViewDelegate{
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }
//}
//
//extension LocationTimeViewController:LocationTimeViewControllerProocol{
//    func onCellTapGesture(cellIndex:IndexPath){
//
//        let selectedCell = addTimeTableView.cellForRow(at: cellIndex) as! TimeAddedCell
//
//        if selectedCell.cellisSelected == false {
//            if userAvailability.count < 3 {
//                userAvailability.append(
//                    Utility.createUserAvailability(fromCell: selectedCell))
//
//                selectedCell.cellisSelected = true
//            }
//        }else{
//            let temp =  Utility.createUserAvailability(fromCell: selectedCell)
//            let index = userAvailability.index(where: { (item) -> Bool in
//                item == temp
//            })
//
//            if index != nil{
//                userAvailability.remove(at: index!)
//                selectedCell.cellisSelected = false
//            }
//        }
//        selectedCell.setSelectionLabel()
//    }
//}
