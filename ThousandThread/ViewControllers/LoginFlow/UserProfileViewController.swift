//
//  UserProfileViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/7/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

protocol UserProfileViewControllerProtocol {
    func showPasswordRules(show:Bool)
}

class UserProfileViewController : BaseViewController{
    @IBOutlet private weak var firstNameTextField: CustomTextInputView!
    @IBOutlet private weak var lastNameTextField: CustomTextInputView!
    @IBOutlet private weak var userNameTextField: CustomTextInputView!
    @IBOutlet private weak var userEmailTextField: CustomTextInputView!
    @IBOutlet private weak var userPasswordTextField: CustomTextInputView!
    
    @IBOutlet weak var passwordViewContainter: UIView!
    
    private weak var passwordRulesViewController:PasswordRulesViewController!
    
    deinit {
        print("UserProfileViewController:deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userPasswordTextField.textInput.addTarget(self, action: #selector(passwordTextFieldChange(_:)),
                                                  for: UIControl.Event.editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTags()
        setDelegates()
    }
    
    @IBAction func userTouchNextStep(_ sender: Any) {
        
        if Utility.isTextFieldEmpty(textField: firstNameTextField.requetTextField()) ||
            Utility.isTextFieldEmpty(textField: lastNameTextField.requetTextField()) ||
            Utility.isTextFieldEmpty(textField: userNameTextField.requetTextField()) ||
            Utility.isTextFieldEmpty(textField: userEmailTextField.requetTextField()) ||
            Utility.isTextFieldEmpty(textField: userPasswordTextField.requetTextField()){
            
            AlertController.alertShard.showAlert(title: "Error", message: "Please check that all text fields are filled out.")
            
        }else{
            if !userPasswordTextField.isValidPassword(){
                AlertController.alertShard.showAlert(title: "Error", message: "Pasword is not valid")
            }else if !userEmailTextField.isValidEmail(){
                AlertController.alertShard.showAlert(title: "Error", message: "Please ener a valid email address.")
            }else{
                startProfileCreation()
            }
        }
    }
    
    private func setDelegates(){
        firstNameTextField.setTextFieldDelegate(delegate: self)
        lastNameTextField.setTextFieldDelegate(delegate: self)
        userNameTextField.setTextFieldDelegate(delegate: self)
        userEmailTextField.setTextFieldDelegate(delegate: self)
        userPasswordTextField.setTextFieldDelegate(delegate: self)
        userPasswordTextField.isSecureTextEntry(isSecureTextEntry: true)
        
        setUserProfileViewControllerDelegate(userProfileViewControllerProtocol: self)
    }
    
    private func setTags(){
        userPasswordTextField.setTextField(tagValue:Constant.shared.kTagPasswordField)
    }
    
    private func startProfileCreation(){
        
        NetworkManger.shared.getUserProfile().updateFristName(firstName: firstNameTextField.requetTextField().text ?? "")
        
        NetworkManger.shared.getUserProfile().updateLastName(lastName: lastNameTextField.requetTextField().text ?? "")
        
        NetworkManger.shared.getUserProfile().updateEmailAddress(emailAddress:userEmailTextField.requetTextField().text ?? "")
        
        NetworkManger.shared.getUserProfile().updateUserName(userName: userNameTextField.requetTextField().text ?? "")
        
        NetworkManger.shared.getUserProfile().updateUserPassword(userPassword: userPasswordTextField.requetTextField().text ?? "")
        
        NetworkManger.shared.createUser(profile: NetworkManger.shared.getUserProfile()) { [weak self] (success) in
            if success {
                self?.performSegue(withIdentifier: "locationAndTimes", sender: self)
            }else{
                AlertController.alertShard.showAlert(title: "Error", message:"There was an issue with " +
                    "creating your user account. Please try again in a few minutes.")
            }
        }
    }
    
    @objc private func passwordTextFieldChange(_ textField: UITextField){
        passwordRulesViewController.updatePasswordRules(passwordText: textField.text ?? "")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.shared.kPasswordRulesViewControllerSegue {
            passwordRulesViewController = segue.destination as? PasswordRulesViewController
        }
    }
}
extension UserProfileViewController:UserProfileViewControllerProtocol{
    func showPasswordRules(show:Bool){
        if show{
            passwordViewContainter.isHidden = false
        }else{
            passwordViewContainter.isHidden = true
        }
    }
}


