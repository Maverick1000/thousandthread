//
//  PaymentMethodChoiceViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/16/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class PaymentMethodChoiceViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "payment methods"
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
