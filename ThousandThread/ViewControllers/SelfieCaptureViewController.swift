//
//  SelfieCaptureViewController.swift
//
//
//  Created by Atif Jamil, Syed on 11/17/17.
//  Copyright © 2017 Wells Fargo. All rights reserved.
//

import UIKit
import GLKit
import Vision
import Speech
import AVFoundation

class SelfieCaptureViewController: BaseViewController {
    
    @IBOutlet weak var overlayView: UIImageView!
    @IBOutlet weak var preview: UIView!
    
    var videoFilter: CoreImageVideoFilter? = nil
    var confidence: CapturedConfidence = CapturedConfidence()
    
    var detectedImage: CIImage? = nil
    var imageSelfie: UIImage? = nil
    var imageFrontWithoutFlash: UIImage? = nil
    var imageFrontWithFlash: UIImage? = nil
    var imageBackWithoutFlash: UIImage? = nil
    var imageBackWithFlash: UIImage? = nil
    
    var startCapture: BoolDelay = BoolDelay(false)
    var capturingImage: Bool = false
    
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.videoFilter = CoreImageVideoFilter(preview: self.preview, position: .front, canBarcodeRead: false, applyFilterCallback: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true

        DispatchQueue.main.async {
            self.capturingImage = false
            self.startCapture.set(value: false)
            self.startCapturing()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startCapture.set(value: true, afterDelay: 1.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopCapturing()
    }
    
    
    // MARK: - Navgation Handler
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard let destinationVC = segue.destination as? EBSelfieDetailViewController else { return }
//        destinationVC.imageSelfie =  imageSelfie
//        destinationVC.imageFrontWithoutFlash = imageFrontWithoutFlash
//        destinationVC.imageFrontWithFlash = imageFrontWithFlash
//        destinationVC.imageBackWithoutFlash = imageBackWithoutFlash
//        destinationVC.imageBackWithFlash = imageBackWithFlash
    }
    
    
    // MARK: - Action Handler
    
//    @IBAction override func goBack() {
//        self.navigationController?.popViewController(animated: true)
//    }
    
}


// MARK: - Capturing - from Camera to take Selfie

extension SelfieCaptureViewController {
    
    func startCapturing() {
        confidence.reset()
        overlayView.tintColor = UIColor(hue: 54/360.0, saturation: confidence.value, brightness: 1.0, alpha: 1.0)
        
        guard let videoFilter = videoFilter else { return }
        videoFilter.stopFiltering()
        videoFilter.applyFilter = { [weak self] image in
            self!.detectedImage = image
            self!.processCapturing()
        }
        
        videoFilter.startFiltering()
    }
    
    func processCapturing() {
        guard startCapture.value == true else { return }
        DispatchQueue.main.async {
            let options: UIView.AnimationOptions = [.beginFromCurrentState, .beginFromCurrentState]
            UIView.animate(withDuration: 0.4, delay: 0.0, options: options, animations: {
                self.overlayView.tintColor = UIColor(hue: 54/360.0, saturation: self.confidence.face, brightness: 1.0, alpha: 1.0)
            }, completion: nil)
        }
        
        guard let image = detectedImage else { return }
        
        let requestHandler = VNImageRequestHandler(ciImage: image, orientation: CGImagePropertyOrientation.up, options: [:])
        try? requestHandler.perform([VNDetectFaceLandmarksRequest(completionHandler: { (request, error) in
            guard let observations = request.results as? [VNFaceObservation],
                let face = observations.first?.landmarks,
                let medianLine = face.medianLine?.normalizedPoints.first
                else {
                    self.confidence.updateFace(false)
                    return
            }
            
            self.confidence.updateFace(true)
            if self.confidence.face == 1.0 && medianLine.x > 0.45 && medianLine.x < 0.55 {
                DispatchQueue.main.async { [weak self] in
                    self?.overlayView.tintColor = UIColor.green
                    self?.takeSelfie()
                }
            }
        })])
    }
    
    func stopCapturing() {
        videoFilter?.stopFiltering()
        videoFilter?.applyFilter = nil
    }
    
    func takeSelfie() {
        guard videoFilter != nil && detectedImage != nil && capturingImage == false else {
            return
        }
        
        capturingImage = true
        videoFilter?.captureStillPhoto({ (image) in
            guard let detectedImage = image else {
                self.capturingImage = false
                return
            }
            
            self.imageSelfie = UIImage.from(ciImage: detectedImage)
            self.submitSelfieForVerification()
        })
    }

    func submitSelfieForVerification() {
//        self.registerNotificationForSelfieSubmission()
//        let docUploadInstance = DocumentSubmissionOperations.sharedInstance
//        docUploadInstance.selfieImageTaken = UIImageJPEGRepresentation(imageSelfie!, 0.75)!
//        docUploadInstance.startUploadSelfieProcess()
//
//        self.performSegue(withIdentifier: "displayEBSSN", sender: self)
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.5) {
//            self.capturingImage = false
//        }
    }
}
