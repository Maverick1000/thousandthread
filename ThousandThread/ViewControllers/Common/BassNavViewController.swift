//
//  BassNavViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 3/31/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class BassNavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    @objc func userSelectedHamugerIcon(){
        let profileTableViewController = SettingsTableViewController()
        navigationController?.present(profileTableViewController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension BassNavViewController:UINavigationControllerDelegate{
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
          //navigationController.navigationItem.leftBarButtonItem = //UIBarButtonItem(image: UIImage(named: "hamburgerIcon"), style: .plain, target: self, action: #selector(userSelectedHamugerIcon))
        //if viewController is ReviewCommentViewController{
//            let VC = viewController as? BaseViewController
//            VC?.addBackButton()
        //}
    }
}
