//
//  CameraContorller.swift
//  ThousandThread
//
//  Created by Paul Walker on 6/23/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import IoniconsKit
import AVFoundation

class CameraContorller: NSObject {
    static let shared = CameraContorller()
    
    private var input: AnyObject?
    private var captureSession:AVCaptureSession!
    private var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    private var videoOutput: AVCaptureVideoDataOutput!
    
    
    weak var delegate:ImagePickerDelegate?
    
    public func showCamaeraView(viewLayer:UIView){
        
        do{
             captureSession = try createAVSession(viewLayer: viewLayer)
        }catch{
            print("Capture Session failed")
        }
       
        captureSession.startRunning()
        
        
//        if UIImagePickerController.isSourceTypeAvailable(.camera) {
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = .camera;
//            imagePicker.allowsEditing = false
//            presentViewController.present(imagePicker, animated: true, completion: nil)
//        }
    }
    
    private func createAVSession(viewLayer:UIView)  throws -> AVCaptureSession{
        
        let session = AVCaptureSession()
        
        if let videoCaptureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front){
            do {
                input = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                print("video device error")
            }
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session )
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspect
            videoPreviewLayer?.frame.size = viewLayer.frame.size
            viewLayer.layer.addSublayer(videoPreviewLayer!)
            session.addInput(input as! AVCaptureInput)
            //
            //            sessionQueue = DispatchQueue(label: "AVSessionQueue", attributes: [])
            //
            //            videoOutput = AVCaptureVideoDataOutput()
            //            videoOutput.videoSettings = [ kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
            //            videoOutput.alwaysDiscardsLateVideoFrames = true
            //            videoOutput.setSampleBufferDelegate(self, queue: sessionQueue)
            //            session.addOutput(videoOutput)
            //
            //            photoOutput = AVCapturePhotoOutput()
            //            session.addOutput(photoOutput)
        }
        return session
    }
    
    func  updatePreviewLayoutSize(size:CGSize){
        videoPreviewLayer?.frame.size = size
    }
}

extension CameraContorller:UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        picker.dismiss(animated: true, completion: nil)
        delegate?.didSelect(image: image)
    }
}

extension CameraContorller:UINavigationControllerDelegate{
    
}

