//
//  PageViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 6/26/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class PageViewController:UIPageViewController{
    var currentPageIndex = 0
    var orderedViewControllers = [UIViewController]()
    
    weak var pageViewControllerDelegate:PageViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func gotoPage(index:Int){
        let direction:UIPageViewController.NavigationDirection!
        
        
        if index > currentPageIndex{
            direction = .forward
        }else{
            direction = .reverse
        }
        
        let VC = orderedViewControllers[index]
        self.setViewControllers([VC], direction: direction, animated: true) { (sucess) in
            if sucess{
            }
        }
        currentPageIndex = index
    }
    
    func addPageViewControlls(viewControllers:[String]){
        for name in viewControllers{
            orderedViewControllers.append(self.newVc(viewController: name))
        }
    }
    
    func addPageViewControll(viewController:UIViewController){
        orderedViewControllers.append(viewController)
    }
    
    func setupFirstViewConroller(){
        self.delegate = self
        self.dataSource = self
        
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
    }
    
    func setupPageViewControllTabIndex(viewControllerIndex:Int, pageIndex:Int){
        if let VC = orderedViewControllers[viewControllerIndex] as? ItemCollectionViewController{
            VC.pageTabIndex = pageIndex
        } else if let VC = orderedViewControllers[viewControllerIndex] as? RequestViewController{
            VC.requestViewType = pageIndex
        }
    }
    
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "HomeNav", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
}

extension PageViewController:UIPageViewControllerDelegate{
    // MARK: Delegate functions
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        //let pageContentViewController = pageViewController.viewControllers![0]
        if let VC = pageViewController.viewControllers![0] as? ItemCollectionViewController{
            let pageIndex = orderedViewControllers.index(of: VC)!
            if let VC = orderedViewControllers[pageIndex] as? ItemCollectionViewController{
                pageViewControllerDelegate?.pageViewControllerHasChange(pageIndex:VC.pageTabIndex)
            }
        }
    }
}

extension PageViewController:UIPageViewControllerDataSource{
    // MARK: Data source functions.
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            // return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            return orderedViewControllers.first
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            // return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}
