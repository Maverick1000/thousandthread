//
//  BaseViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/13/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    @IBOutlet weak var titleView: TitleView!
    
    var screenTapGestureRecognizer:UITapGestureRecognizer!
    private var selectedTextField:UITextField!
    private var userProfileViewControllerProtocol:UserProfileViewControllerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        addTapGestureRecognizer()
        
        
        if LoginController.shared.isLogin(){
            navigationItem.rightBarButtonItem =  UIBarButtonItem(image: UIImage(named: "hamburgerIcon"), style: .plain, target: self, action: #selector(userSelectedHamugerIcon))
        }
    }
    
    @objc func userClickBackButton(){
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func goBackButton(_ sender: Any){
        dismiss(animated: true, completion: nil)
    }
    
    private func addTapGestureRecognizer(){
        screenTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(userTapScreen))
        screenTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(screenTapGestureRecognizer)
    }
    
    @objc private func userTapScreen(gestureRecognizer:UITapGestureRecognizer){
        selectedTextField?.resignFirstResponder()
    }
    
    @objc func userSelectedHamugerIcon(){
        let profileTableViewController = SettingsTableViewController()
        navigationController?.pushViewController(profileTableViewController, animated: true)
    }
    
    public func setUserProfileViewControllerDelegate(userProfileViewControllerProtocol:UserProfileViewControllerProtocol?){
        self.userProfileViewControllerProtocol = userProfileViewControllerProtocol
    }
    
    private func setBackButton(){
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "",
                                    style: UIBarButtonItem.Style.plain, target: nil, action: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}


extension BaseViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
        
        if selectedTextField.tag == Constant.shared.kTagPasswordField{
            userProfileViewControllerProtocol?.showPasswordRules(show: true)
        }else{
            userProfileViewControllerProtocol?.showPasswordRules(show: false)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if selectedTextField.tag == Constant.shared.kTagPasswordField{
            if string != "" {
                if let count = textField.text?.count{
                    return (count <= Constant.shared.kMinCharacterCount ||  count <= Constant.shared.kMaxCharacterCount)
                }
            }
        }
        return true
    }
}

extension BaseViewController:PageNavgationProtocol{
    func goBack() {
        navigationController?.popViewController(animated: true)
    }
}
