//
//  KeyChainManager.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/10/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import KeychainSwift

class KeyChainManager{
    static let shared = KeyChainManager()
    
    let keychain = KeychainSwift()
    
    func saveValue(key:String, value:String){
        keychain.set(value, forKey: key)
    }
    
    func getValue(key:String) ->String?{
        if let value = keychain.get(key){
            return value
        }
        return nil
    }
}
