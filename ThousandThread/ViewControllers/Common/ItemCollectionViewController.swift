//
//  ProfileItemCollectionViewController.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/29/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

private let reuseIdentifier = "item"

class ItemCollectionViewController: UICollectionViewController, NVActivityIndicatorViewable {
    
    var pageTabIndex:Int!
    private var items = [ItemPageRowResponse]()
    private var pageRespone:ItemPageResponse!
    private var socialPageResponse:SocialPageResponse!
    
    var shouldSearchResutles = false
    
    @IBOutlet var itemCollectionTableView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        shouldSearchResutles = false
    }
    
    func processSearchReults(pageRespone:ItemPageResponse){
        items.removeAll()
        
        self.pageRespone = pageRespone
        items = [ItemPageRowResponse]()
        items.append(contentsOf: pageRespone.requestItems())
    }
    
    func requestLoginUserItemList(userID:Int){
        //startAnimating(type:.ballRotateChase)
        items.removeAll()
        NetworkManger.shared.requestItems(forId: userID) { [weak self] (success, pageRespone) in
            if success{
                // self?.stopAnimating()
                self?.pageRespone = pageRespone
                self?.items.append(contentsOf: pageRespone!.requestItems())
                self?.itemCollectionTableView.reloadData()
            }
        }
    }
    
    func requestLoginUserFriendClosset(userID:Int){
        items.removeAll()
        NetworkManger.shared.requestFriendCloset(userID: userID) { [weak self] (success, pageRespone) in
            self?.socialPageResponse = pageRespone
            if let rows = pageRespone?.pageRows {
                self?.items.append(contentsOf: rows)
                self?.itemCollectionTableView.reloadData()
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    private func loadPageNumber(page:Int){
        startAnimating(type:.ballRotateChase)
        ItemResquestQueue.shared.requestPageOItems(atPage: page) { [weak self]  (success, pageRespone) in
            self?.stopAnimating()
            if success{
                self?.items.append(contentsOf: pageRespone.requestItems())
                self?.itemCollectionTableView.reloadData()
                
            }
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if items != nil{
            return items.count
        }
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProfileItemTableViewCell
        
        let item = items[indexPath.row]
        
        let celleData = ProfileItemCell(imagePath:Constant.shared.kBaseURL + item.requestImagePath(),
                                        title: item.requestItemTitle(), priceRent:"$ \(item.requestRentPrice())",
            retailPrice: "$ \(item.requestPrice())", size: item.requestSize())
        cell.setupCell(cellData: celleData)
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
     
     }
     */
    
}

extension ItemCollectionViewController: UICollectionViewDataSourcePrefetching//UITableViewDataSourcePrefetching
{
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if indexPath.row == self.items.count - 1{
                if pageRespone.totalItems() > items.count{
                    loadPageNumber(page: pageRespone.requestNextPage())
                }
            }
        }
    }
}
