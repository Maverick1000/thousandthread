//
//  SiginAPI.swift
//  ThousandThread
//
//  Created by Paul Walker on 3/4/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class SiginAPI:RestKitManager{
    static let shared = SiginAPI()
    
    func userRequestToken(username:String, userpassword:String, completionHandler:@escaping(Bool,UserSignTokenResponse?) ->()){
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: UserSignTokenResponse().requestMapping(),
                                                               method: .POST, pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        let request = manager.multipartFormRequest(with:nil, method: .POST, path: "/users/sign-in", parameters:nil) { (AFMultipartFormData) in
            
            let parameters = [
                "username": username,
                "password": userpassword
            ]
            
            for (key, value) in parameters {
                AFMultipartFormData?.appendPart(withForm:value.data(using: String.Encoding.utf8)!, name: key)
            }
        }
        
        let operation = manager.objectRequestOperation(with: request! as URLRequest, success: { (requestOperation, mappingResults) in
            if let mapResponse = mappingResults?.firstObject as? UserSignTokenResponse{
                completionHandler(true,mapResponse)
            }
        }) { (requestOperation, error) in
            completionHandler(false,nil)
        }
        manager.enqueue(operation)
    }
    
    func userSigin(username:String,userpassword:String, profile:UserProfile, completionHandler:@escaping(Bool,UserProfile?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: profile.requestMapping(),
                                                            method: .POST, pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        let parameters = [
            "username": username,
            "password" : userpassword
        ]
        
        let request = manager.multipartFormRequest(with:nil, method: .POST, path: "/users/sign-In", parameters:parameters) { (AFMultipartFormData) in
        }
        
        let operation = manager.objectRequestOperation(with: request! as URLRequest, success: { (requestOperation, mappingResults) in
            if let mapResponse = mappingResults?.firstObject as? UserProfile{
                completionHandler(true,mapResponse)
             }
        }) { (requestOperation, error) in
            completionHandler(false,nil)
        }
        manager.enqueue(operation)
    }
}
