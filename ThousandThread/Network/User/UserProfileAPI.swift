//
//  UserAPI.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/5/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class UserProfileAPI:RestKitManager{

    func postObject(profile:UserProfile, path:String, completionHandler:@escaping(Bool) ->()){
        
        if let userRequestDescriptor = RKRequestDescriptor(mapping: profile.requestMapping()?.inverse(), objectClass: profile.classForCoder, rootKeyPath: nil, method: .POST) {
            addRequestDescriptor(descriptor: userRequestDescriptor )
        }
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: PostResponse().postResponseMapping(), method: .POST, pathPattern: nil,
                                                               keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.post(profile, path: path, parameters: nil, success: { (operation, result) in
            if  let postResponse  = result?.firstObject as? PostResponse{
                profile.updateUserID(userID: postResponse.requestUserID())
            }
            completionHandler(true)
        }) { (operation, error) in
            completionHandler(false)
        }
    }
    
    func getObject(path:String,completionHandler:@escaping(Bool,UserProfile?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: UserProfile().requestMapping(), method: .GET, pathPattern: nil,
                                                               keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.getObject(nil, path: path , parameters: nil, success: { (operation, result) in
            completionHandler(true, result?.array()?.first as? UserProfile)
        }) { (operation, error) in
            completionHandler(false,nil)
        }
    }
    
    func updateObject(profile:UserProfile, path:String,completionHandler:@escaping(Bool) ->()){
      
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: UpdateResponse().updateResponseMapping(), method: .PUT,
                                                               pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }

        manager.put(profile, path: path, parameters: nil, success: { (operation, result) in
            completionHandler(true)
        }) { (operation, error) in
            completionHandler(false)
        }
    }
    
    func deleteObject(path:String,completionHandler:@escaping(Bool) ->()){
      
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: DeleteResponse().deleteResponseMapping(), method: .DELETE,
                                                               pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        manager.delete(nil, path: path, parameters: nil, success: { (operation, result) in
            completionHandler(true)
        }) { (operation, error) in
            completionHandler(false)
        }
    }
}

