//
//  PostResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/22/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class PostResponse:NSObject{
    @objc private var userID:NSNumber!

    override init() {
        userID = 0
    }
    
    public func postResponseMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["id":"userID"])
            return mapping
        }else{
            return nil
        }
    }
    
    public func requestUserID() -> Int{
        return userID.intValue
    }
    
}
