//
//  ImageDataManager.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/27/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import RestKit
import Foundation

class ImageDataManager:RestKitManager{
    var imageDataResponse = ImageDataResponse()
    
    func uploadProfilePic(profile:UserProfile, completionHandler:@escaping(Bool) ->()){
        
        if let imagePath = Utility.requestImageDocument(){
            
            if  let userResponseDescriptor =  RKResponseDescriptor(mapping: imageDataResponse.responseMapping(),
                                                                   method: .POST, pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
                addResponseDescriptor(descriptor: userResponseDescriptor)
            }
            
            let request = manager.multipartFormRequest(with:nil, method: .POST, path: "/users/formData", parameters:nil) { (AFMultipartFormData) in
                let imageData = try!Data(contentsOf: imagePath)
                let image = UIImage(data: imageData, scale: 0.5)
                
                AFMultipartFormData?.appendPart(withFileData: image?.jpegData(compressionQuality: 1.0), name: "Portrait",
                                                fileName: Constant.shared.kPortraitmage, mimeType: "image/jpg")
                let parameters = ["id": "\(profile.requestUserID())",
                    "photo1": imagePath.absoluteString,
                    "first_name" : profile.requestFristName(),
                    "last_name"  : profile.requestLastName()]
                
                for (key, value) in parameters {
                    AFMultipartFormData?.appendPart(withForm:value.data(using: String.Encoding.utf8)!, name: key)
                }
            }
            
            let operation = manager.objectRequestOperation(with: request! as URLRequest, success: { (requestOperation, mappingResults) in
                if let mapResponse = mappingResults?.firstObject as? ImageDataResponse{
                    profile.updateDate(date: mapResponse.requestUpdatedate())
                }
                completionHandler(true)
            }) { (requestOperation, error) in
                completionHandler(false)
            }
            manager.enqueue(operation)
        }
    }
      
}
