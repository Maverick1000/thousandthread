//
//  SearchAPI.swift
//  ThousandThread
//
//  Created by Paul Walker on 4/24/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class SearchAPI:RestKitManager{
    
    func doSearchRequest(request:SearchRequest, completionHandler:@escaping(Bool,ItemPageResponse?,SearchResultIsEmpty?) ->()){
        
        if  let userResponseDescriptor = RKResponseDescriptor(mapping: ItemPageResponse().requestMapping(),
                                                              method: .POST, pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
           addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        var parameters = [String:String]()
        
        if request.seaStr?.isEmpty == false{
            parameters["seaStr"] = request.seaStr?.lowercased()
        }
        
        if request.productGender?.isEmpty == false{
            parameters["productGender"] = request.productGender?.lowercased()
        }
        
        if request.category?.isEmpty == false{
            parameters["category"] = request.category?.lowercased()
        }
        
        if request.size?.isEmpty == false{
            parameters["size"] = request.size
        }
        
        if request.pantSize?.isEmpty == false{
            parameters["pantSize"] = request.pantSize?.lowercased()
        }
        
        if request.shoesize?.isEmpty == false{
            parameters["shoesize"] = request.shoesize?.lowercased()
        }
        
        if let profile = LoginController.shared.curretSigInUser {
            let request = manager.multipartFormRequest(with:nil, method: .POST, path:
                "/ads/search?longitude=\(profile.requestLongitude())&latitude=\(profile.requestLatitude())",
            parameters:parameters) { (AFMultipartFormData) in
            }
            
            let operation = manager.objectRequestOperation(with: request! as URLRequest, success: { (requestOperation, mappingResults) in
                if let mappingResults = mappingResults?.firstObject as? ItemPageResponse{
                    if mappingResults.isEmpty == "true"{
                        completionHandler(false, mappingResults, nil)
                    }else{
                        completionHandler(true, mappingResults, nil)
                    }
                }
            }) { (requestOperation, error) in
                completionHandler(false,nil,nil)
            }
            manager.enqueue(operation)
        }
    }
}
