//
//  ItemResquestQueue.swift
//  ThousandThread
//
//  Created by Paul Walker on 3/3/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

class ItemResquestQueue{
    static let shared = ItemResquestQueue()
    var queue:OperationQueue!
    
    init() {
        queue = OperationQueue()
    }
    
    func requestFirstPageOfItems(completionHandler:@escaping(Bool,ItemPageResponse) ->()){
        NetworkManger.shared.requestItemsList { (success, pageResponse) in
            if( success ){
                completionHandler(success,pageResponse!)
            }
        }
    }

    func requestPageOItems(atPage:Int, completionHandler:@escaping(Bool,ItemPageResponse) ->()){
        NetworkManger.shared.requestItemsAt(page: atPage) {  (success, pageResponse) in
            if( success ){
                completionHandler(success,pageResponse!)
            }
        }
    }
}
