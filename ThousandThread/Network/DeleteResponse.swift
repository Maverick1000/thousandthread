//
//  DeleteResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/22/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class DeleteResponse:NSObject{
    @objc private var message:String!
    
    override init() {
        message = ""
    }
    
    public func deleteResponseMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["message"])
            return mapping
        }else{
            return nil
        }
    }
    
}
