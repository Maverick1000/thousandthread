//
//  SocialAPI.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/29/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class SocialAPI:RestKitManager{
    func requestFollowByMe(userID:Int, path:String, completionHandler:@escaping(Bool, SocialPageResponse?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: SocialPageResponse().requestMapping(),
                                                               method: .GET, pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.getObject(nil, path: path , parameters: nil, success: { [weak self]  (operation, result) in
            if let mappingResults = result?.array()?.first as? SocialPageResponse {
                self?.checkRequestMapping(response: mappingResults, completionHandler: completionHandler)
            }
        }) { (operation, error) in
            completionHandler(false,nil)
        }
    }
    
    func requestFriendsList(userID:Int, path:String, completionHandler:@escaping(Bool, SocialPageResponse?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: SocialPageResponse().requestMapping(),
                                                               method: .GET, pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.getObject(nil, path: path , parameters: nil, success: { [weak self]  (operation, result) in
            
            if let mappingResults = result?.array()?.first as? SocialPageResponse {
                self?.checkRequestMapping(response: mappingResults, completionHandler: completionHandler)
            }
        }) { (operation, error) in
            completionHandler(false,nil)
        }
    }
    
    func requestFriendCloset(userID:Int, path:String, completionHandler:@escaping(Bool, SocialPageResponse?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: SocialPageResponse().requestMapping(),
                                                               method: .GET, pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.getObject(nil, path: path , parameters: nil, success: { [weak self] (operation, result) in
            if let mappingResults = result?.array()?.first as? SocialPageResponse {
                self?.checkRequestMapping(response: mappingResults, completionHandler: completionHandler)
            }
        }) { (operation, error) in
            completionHandler(false,nil)
        }
    }
    
    func requestFollowers(userID:Int, path:String, completionHandler:@escaping(Bool, SocialPageResponse?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: SocialPageResponse().requestMapping(),
                                                               method: .GET, pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.getObject(nil, path: path , parameters: nil, success: {[weak self] (operation, result) in
            if let mappingResults = result?.array()?.first as? SocialPageResponse {
                self?.checkRequestMapping(response: mappingResults, completionHandler: completionHandler)
            }
        }) { (operation, error) in
            completionHandler(false,nil)
        }
    }
    
    private func checkRequestMapping(response:SocialPageResponse,
                                     completionHandler:@escaping(Bool, SocialPageResponse?) ->()){
        if response.isEmpty == "true"{
            completionHandler(false, response)
        }else{
            completionHandler(true, response)
        }
        
    }
}

