//
//  UpdateResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/27/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class UpdateResponse : NSObject{
    @objc private var fieldCount:NSNumber!
    @objc private var insertId:NSNumber!
    @objc private var serverStatus:NSNumber!
    @objc private var warningCount:NSNumber!
    @objc private var message:NSDictionary!
    @objc private var protocol41:NSNumber!
    @objc private var changedRows:NSNumber!
    
    public func updateResponseMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["fieldCount","insertId","serverStatus","warningCount", "message",
                "protocol41","changedRows"])
            return mapping
        }else{
            return nil
        }
    }

}

