//
//  NotificationsAPI.swift
//  ThousandThread
//
//  Created by Paul Walker on 6/26/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class NotificationsAPI: RestKitManager {
    func getNotifications(path:String,completionHandler:@escaping(Bool,NotificationResponse?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: NotificationResponse().requestMapping(), method: .GET, pathPattern: nil,
                                                               keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.getObject(nil, path: path , parameters: nil, success: { (operation, result) in
            completionHandler(true, result?.array()?.first as? NotificationResponse)
        }) { (operation, error) in
            completionHandler(false,nil)
        }
    }
}
