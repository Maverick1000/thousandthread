//
//  ChatAPI.swift
//  ThousandThread
//
//  Created by Paul Walker on 5/20/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class ChatAPI : RestKitManager{
    func getChatMessage(path:String,completionHandler:@escaping(Bool,MessageResponse?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: MessageResponse().requestMapping(), method: .GET, pathPattern: nil,
                                                               keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.getObject(nil, path: path , parameters: nil, success: { (operation, result) in
            if let mappingResults = result?.firstObject as? MessageResponse{
                if mappingResults.isEmpty == "true" ||
                    mappingResults.isError == "true"{
                    completionHandler(false, nil)
                }else{
                    completionHandler(true, mappingResults )
                }
            }
        }) { (operation, error) in
            completionHandler(false,nil)
        }
    }
    
    func getMessageSet(path:String,completionHandler:@escaping(Bool,[ChatMessageSetResponse]?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: ChatMessageSetResponse().requestMapping(), method: .GET, pathPattern: nil,
                                                               keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.getObject(nil, path: path , parameters: nil, success: { (operation, result) in
            if let mappingResults = result?.array() as? [ChatMessageSetResponse]{
                completionHandler(true, mappingResults)
            }else{
                completionHandler(false, nil )
            }
            
        }) { (operation, error) in
            completionHandler(false,nil)
        }
    }
    
    func postMessage(message:PostMessage,imageData:[Data]?, completionHandler:@escaping(Bool) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: UpdateResponse().updateResponseMapping(),
                                                               method: .POST, pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        
        let request = manager.multipartFormRequest(with:message, method: .POST, path: "/messages",
                                                   parameters:message.requsestDictionaryParams()) { (AFMultipartFormData) in
                if imageData != nil {
                    for(index,imageData) in imageData!.enumerated(){
                        AFMultipartFormData?.appendPart(withFileData: imageData, name: "uploadfiles",
                                                        fileName: "image_\(index).jpg", mimeType: "image/jpg")
                    }
                }
        }
        
        let operation = manager.objectRequestOperation(with: request! as URLRequest, success: { (requestOperation, mappingResults) in
            completionHandler(true)
        }) { (requestOperation, error) in
            completionHandler(false)
        }
        manager.enqueue(operation)
    }
    
}
