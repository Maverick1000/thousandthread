//
//  SocketIOManager.swift
//  ThousandThread
//
//  Created by Paul Walker on 5/20/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager : NSObject{
    static let shared = SocketIOManager()
    
    var socket:SocketIOClient!
    var manager:SocketManager!
    
    override init() {
        super.init()
        manager =  SocketManager(socketURL: URL(string: Constant.shared.kBaseURL)!, config: [.log(true), .compress])
        socket = manager.defaultSocket
        
        setupConnect()
        setupDisConnect()
    }
    
    public func establishConnection() {
        socket.connect()
    }
    
    public func closeConnection() {
        socket.disconnect()
    }
    
    private func setupRealTimeEvent(){
        socket.on("realTimeEvent") {data, ack in
            if let item = data[0] as? [String:Any]{
                NotificationCenter.default.post(name: Constant.shared.kPostIncomingMessage, object: nil, userInfo: item)
            }
        }
    }
    
    private func setupConnect(){
        socket.on(clientEvent: .connect) {[weak self] data, ack in
            print("socket connected")
            
            let userID = LoginController.shared.curretSigInUser.requestUserID()
            self?.socket.emit("user_id",userID)
            self?.setupRealTimeEvent()
        }
    }
    
    private func setupDisConnect(){
        socket.on(clientEvent: .disconnect) {data, ack in
            print("socket disconnected")
        }
    }
}
