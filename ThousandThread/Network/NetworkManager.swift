//
//  NetworkManager.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/5/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit




class NetworkManger{
    static let shared = NetworkManger()
    
    private var itemAPI:ItemAPI!
    private var siginAPI:SiginAPI!
    private var chatAPI:ChatAPI!
    private var reviewAPI:ReviewAPI!
    private var userAPI:UserProfileAPI!
    private var searchAPI:SearchAPI!
    private var notificationsAPI:NotificationsAPI!
    private var imageDataManager:ImageDataManager!
    private var userProfile:UserProfile!
    private var socialAPI:SocialAPI!
    
    init() {
        itemAPI = ItemAPI()
        reviewAPI = ReviewAPI()
        userAPI = UserProfileAPI()
        siginAPI = SiginAPI()
        searchAPI = SearchAPI()
        chatAPI = ChatAPI()
        notificationsAPI = NotificationsAPI()
        imageDataManager = ImageDataManager()
        socialAPI = SocialAPI()
        
        if Constant.shared.kUseToken{
            itemAPI.setAuthorizationWithToken()
            reviewAPI.setAuthorizationWithToken()
            userAPI.setAuthorizationWithToken()
            siginAPI.setAuthorizationWithToken()
            searchAPI.setAuthorizationWithToken()
            chatAPI.setAuthorizationWithToken()
            notificationsAPI.setAuthorizationWithToken()
            imageDataManager.setAuthorizationWithToken()
            socialAPI.setAuthorizationWithToken()
        }
    }
    
    func getUserProfile() ->UserProfile{
        return userProfile
    }
    
    func getImageData() ->ImageDataManager{
        return imageDataManager
    }
    
    //MARK:User API Func
    func createUser(profile:UserProfile,completionHandler:@escaping(Bool) ->()){
        userAPI.postObject(profile: profile, path: "/users") { (success) in
            completionHandler(success)
        }
    }
    
    func requestUser(userID:Int,completionHandler:@escaping(Bool,UserProfile?) ->()){
        userAPI.getObject(path: "/users/\(userID)") { (success, profile) in
            completionHandler(success,profile)
        }
    }
    
    func updateUser(userID:Int, profile:UserProfile,completionHandler:@escaping(Bool) ->()){
        userAPI.updateObject(profile: profile, path: "/users/\(userID)") { (success) in
            completionHandler(success)
        }
    }
    
    func deleteUser(userID:Int, completionHandler:@escaping(Bool) ->()){
        userAPI.deleteObject(path: "/users/\(userID)") { (success) in
            completionHandler(success)
        }
    }
    
    //MARK:Item API Func
    func requestItemsList(completionHandler:@escaping(Bool,ItemPageResponse?) ->()){
        if let profile = LoginController.shared.curretSigInUser {
            itemAPI.getItems(path: "/ads?longitude=\(profile.requestLongitude())&latitude=\(profile.requestLatitude())") { (success,pageResponse) in
                completionHandler(success,pageResponse)
            }
        }
    }
    
    func requestItemsAt(page:Int, completionHandler:@escaping(Bool,ItemPageResponse?) ->()){
        if let profile = LoginController.shared.curretSigInUser {
            itemAPI.getItems(path: "/ads?page=\(page)&longitude=\(profile.requestLongitude())&latitude=\(profile.requestLatitude())") { (success,pageResponse) in
                completionHandler(success,pageResponse)
            }
        }
    }
    
    func requestItems(forId:Int,completionHandler:@escaping(Bool,ItemPageResponse?) ->()){
        itemAPI.getItems(path: "/ads/get-ad-by-user/\(forId)") { (success,pageRowResponse) in
            completionHandler(success,pageRowResponse)
        }
    }
    
    func requestItem(id:Int,completionHandler:@escaping(Bool,ItemPageRowResponse?) ->()){
        itemAPI.getItem(path: "/ads/\(id)") { (success,pageRowResponse) in
            completionHandler(success,pageRowResponse)
        }
    }
    
    func postItem(profile:UserProfile, images:[Data], item:ItemPageRowResponse, completionHandler:@escaping(Bool) ->()){
        itemAPI.postItem(profile: profile, images: images, item: item) { (success) in
            completionHandler(success)
        }
    }
    
    //MARK:Reveiw API Func
    func requestReviewComments(id:Int, completionHandler:@escaping(Bool,[Review]?) ->()){
        reviewAPI.getReviewComments(path: "/ads/\(id)/review") { (success,review) in
            completionHandler(success,review)
        }
    }
    
    //MARK:Search API Func
    func prefromSearchRequest(searchRequest:SearchRequest,completionHandler:@escaping(Bool,ItemPageResponse?,SearchResultIsEmpty?) ->()){
        searchAPI.doSearchRequest(request: searchRequest) { (success, pageResponse, searchResultError) in
            completionHandler(success,pageResponse,searchResultError)
        }
    }
    
    //MARK:Login API Func
    func userRequestToken(username:String, userpassword:String, completionHandler:@escaping(Bool,UserSignTokenResponse?) ->()){
        siginAPI.userRequestToken(username: username, userpassword: userpassword) { (success, userSignToken) in
            completionHandler(success,userSignToken)
        }
    }
    
    func userRequestSigin(username:String, userpassword:String, completionHandler:@escaping(Bool,UserProfile?) ->()){
        siginAPI.userSigin(username: username, userpassword: userpassword, profile: UserProfile()) { (success, userProfile) in
            completionHandler(success,userProfile)
        }
    }
    
    //MARK:Social API Func
    func requestWhoFollowByMe(userID:Int, completionHandler:@escaping(Bool,SocialPageResponse?) ->()){
        socialAPI.requestFollowByMe(userID: userID, path: "/social/\(userID)/user-followed-by-me") {(succues, socialPageResponse) in
            completionHandler(succues,socialPageResponse)
        }
    }
    
    func requestFriendList(userID:Int, completionHandler:@escaping(Bool,SocialPageResponse?) ->()){
        socialAPI.requestFriendsList(userID: userID, path: "/social/\(userID)/friend") {(succues, socialPageResponse) in
            completionHandler(succues,socialPageResponse)
        }
    }
    
    func requestFriendCloset(userID:Int, completionHandler:@escaping(Bool,SocialPageResponse?) ->()){
        socialAPI.requestFriendCloset(userID: userID, path: "/social/\(userID)/friend-closet") {(succues, socialPageResponse) in
            completionHandler(succues,socialPageResponse)
        }
    }
    
    func requestUserWhoFollowMe(userID:Int, completionHandler:@escaping(Bool,SocialPageResponse?) ->()){
        socialAPI.requestFriendsList(userID: userID, path: "/social/\(userID)/user-following-me") {(succues, socialPageResponse) in
            completionHandler(succues,socialPageResponse)
        }
    }
    
    //MARK:Chat API Func
    func requestMessageAD(id:Int, completionHandler:@escaping(Bool,MessageResponse?) ->()){
        chatAPI.getChatMessage(path:"/messages/\(id)/ad") { (success, chatMessage) in
            completionHandler(success,chatMessage)
        }
    }
    func requestMessageChat(id:Int, completionHandler:@escaping(Bool,MessageResponse?) ->()){
        chatAPI.getChatMessage(path:"/messages/\(id)/chat") { (success, chatMessage) in
            completionHandler(success,chatMessage)
        }
    }
    
    func requestMessageFriend(id:Int, completionHandler:@escaping(Bool,MessageResponse?) ->()){
        chatAPI.getChatMessage(path:"/messages/\(id)/friend") { (success, chatMessage) in
            completionHandler(success,chatMessage)
        }
    }
    
    func requestMessageReview(id:Int, completionHandler:@escaping(Bool,MessageResponse?) ->()){
        chatAPI.getChatMessage(path:"/messages/\(id)/review") { (success, chatMessage) in
            completionHandler(success,chatMessage)
        }
    }
    
    func  requestMessageSet(id:Int, messageSetID:Int,
                            completionHandler:@escaping(Bool,[ChatMessageSetResponse]?) ->()) {
        chatAPI.getMessageSet(path:"/messages/\(id)/set/\(messageSetID)") { (success, chatMessageSetResponse) in
            completionHandler(success,chatMessageSetResponse)
        }
    }
    
    func  requestMessageSet(sender_id:Int, recipient_id:Int,
                            completionHandler:@escaping(Bool,[ChatMessageSetResponse]?) ->()) {
        chatAPI.getMessageSet(path:"/messages/\(sender_id)/to/\(recipient_id)") { (success, chatMessageSetResponse) in
            completionHandler(success,chatMessageSetResponse)
        }
    }
    
    func postMessage(messsage:PostMessage, imageData:[Data]?, completionHandler:@escaping(Bool) ->()){
        chatAPI.postMessage(message: messsage, imageData:imageData) { (success) in
            completionHandler(success)
        }
    }
    
    //MARK:Notifications API
    func requestCurrentNotifications(id:Int, completionHandler:@escaping(Bool, NotificationResponse?) ->()){
        notificationsAPI.getNotifications(path: "/notifications/\(id)") { (success, notification) in
            completionHandler(success,notification)
        }
    }
}
