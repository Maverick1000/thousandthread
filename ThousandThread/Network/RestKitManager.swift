//
//  RestKitManager.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/27/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class RestKitManager{
    var manager:RKObjectManager!
    let baseURL = URL(string: Constant.shared.kBaseURL)
    
    init() {
        manager = RKObjectManager(baseURL: baseURL)
        manager.requestSerializationMIMEType = RKMIMETypeJSON
//        
//        if  let clientErrorDescriptor =  RKResponseDescriptor(mapping: nil, method: .any,
//                                                               pathPattern: nil, keyPath: nil, statusCodes: Utility.clientStatusCode){
//            addResponseDescriptor(descriptor: clientErrorDescriptor)
//        }
    }
    
    func addRequestDescriptor(descriptor:RKRequestDescriptor){
        manager.addRequestDescriptor(descriptor)
    }
    
    func addResponseDescriptor(descriptor:RKResponseDescriptor){
        manager.addResponseDescriptor(descriptor)
    }
    
    func addResponseDescriptors(descriptor:[RKResponseDescriptor]){
         manager.addResponseDescriptors(from: descriptor)
    }
    
    func setAuthorizationWithToken(){
        if let token = KeyChainManager.shared.getValue(key: "token") {
            manager.httpClient.setDefaultHeader("Authorization",
                                            value: "Bearer \(token)")
        }
    }
}
