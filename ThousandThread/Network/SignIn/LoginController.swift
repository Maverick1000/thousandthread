//
//  LoginController.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/17/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class LoginController: NSObject {
    
    public static let shared = LoginController()
    
    private var _isLogin = false
    var curretSigInUser:UserProfile!
    
    public func isLogin() ->Bool{
        requestLoginStatus()
        return _isLogin;
    }
    
    public func updateLoginStatus(status:Bool, profileID:Int){
        UserDefaults.standard.set(status, forKey: Constant.shared.kIsLogin)
        UserDefaults.standard.set(profileID, forKey: Constant.shared.kUSerID)
    }
    
    private func requestLoginStatus() {
        let status = UserDefaults.standard.bool(forKey: Constant.shared.kIsLogin)
        if status{
            _isLogin = status
        }else{
            _isLogin = false
        }
    }
    public func getUserProfile(completionHandler:@escaping(Bool) ->()){
        if curretSigInUser == nil{
            let userID = UserDefaults.standard.integer(forKey: Constant.shared.kUSerID);
            NetworkManger.shared.requestUser(userID: userID) {[weak self] (success, profile) in
                self?.curretSigInUser = profile
                completionHandler(success)
            }
        }
    }
    
    func DoUserLogin(usernme:String, userPassword:String, completionHandler:@escaping(Bool) ->()){
        if Constant.shared.kUseToken == false{
            NetworkManger.shared.userRequestSigin(username: usernme, userpassword: userPassword) { [weak self] (success, userProfile) in
                if( userProfile != nil && success ){
                    self?.curretSigInUser = userProfile
                    self?.updateLoginStatus(status: success,profileID: self?.curretSigInUser.requestUserID() ?? 1)
                    completionHandler(success)
                }else{
                    completionHandler(success)
                }
            }
        }else{
            NetworkManger.shared.userRequestToken(username: usernme,userpassword: userPassword) { [weak self] (success, userSignToken) in
                if let tokenObject = userSignToken {
                    let userID = tokenObject.requestUser()
                    if let token = tokenObject.requestToken(){
                        KeyChainManager.shared.saveValue(key: "token", value: token)
                        self?.updateLoginStatus(status: success,profileID:userID?.requestID() ?? 0)
                        completionHandler(true)
                    }else{
                        completionHandler(false)
                    }
                }
            }
        }
    }
}
