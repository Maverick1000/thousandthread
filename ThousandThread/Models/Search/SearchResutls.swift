//
//  SearchResutls.swift
//  ThousandThread
//
//  Created by Paul Walker on 4/24/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation

struct SearchRequest{
    var seaStr:String?
    var productGender:String?
    var category:String?
    var size:String?
    var pantSize:String?
    var shoesize:String?
}
