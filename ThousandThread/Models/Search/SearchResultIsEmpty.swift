//
//  SearchResultIsEmpty.swift
//  ThousandThread
//
//  Created by Paul Walker on 4/24/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class SearchResultIsEmpty:NSObject{
    @objc private var isEmpty:String!
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["isEmpty"])
            return mapping
        }else{
            return nil
        }
    }
}
