//
//  ImageDataResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/27/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import RestKit

class ImageDataResponse:NSObject{
    @objc private var updatedate:Date!
    
    override init() {
        updatedate = Date()
    }
    public func requestUpdatedate() ->Date{
        return updatedate
    }
    
    public func responseMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["updated_at":"updatedate"])
            return mapping
        }else{
            return nil
        }
    }
}
