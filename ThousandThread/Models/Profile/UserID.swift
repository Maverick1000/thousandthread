//
//  UserID.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/10/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class UserID:NSObject{
    @objc private var id:NSNumber!
   
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["id"])
            
            return mapping
        }else{
            return nil
        }
    }
    
    func requestID()-> Int{
        return id.intValue 
    }
}
