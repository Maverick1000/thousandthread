//
//  UserSignTokenResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/10/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class UserSignTokenResponse:NSObject{
    @objc private var user:UserID?
    @objc private var token :String?
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["token":"token"])
            
            let relationShipMappping = RKRelationshipMapping(fromKeyPath: "user", toKeyPath: "user", with: UserID().requestMapping())
            mapping.addPropertyMapping(relationShipMappping)
            
            return mapping
        }
        return nil
    }
    
    func requestUser() ->UserID?{
        if let user = user {
            return user
        }
        return nil
    }
    
    func requestToken() ->String?{
        if let token = token {
            return token
        }
        return nil
    }
}
