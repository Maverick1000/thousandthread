//
//  UserProfile.swift
//  ThousandThread
//
//  Created by Paul Walker on 1/7/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit


class UserProfile:NSObject {
    @objc private var id:NSNumber?
    @objc private var firstName:String?
    @objc private var lastName:String?
    @objc private var city:String?
    @objc private var state:String?
    @objc private var emailAddress:String?
    @objc private var phone:String?
    @objc private var username:String?
    @objc private var password:String?
    @objc private var screenName:String?
    @objc private var photo1:String?
    @objc private var community:String?
    @objc private var latitude:NSNumber?
    @objc private var longitude:NSNumber?
    @objc private var online:NSNumber?
    @objc private var availability1:String?
    @objc private var availability2:String?
    @objc private var availability3:String?
    @objc private var payvenmo:String?
    @objc private var paybank:String?
    @objc private var paypaypal:String?
    @objc private var createdate:Date?
    @objc private var updatedate:Date?
    @objc private var status:NSNumber?
    @objc private var payCreditCardExpireDt:Date?
    @objc private var payCreditCardNumber:String?
    @objc private var payCreditCardPin:String?
    @objc private var payCreditCardType:String?
    
    override init() {
        id = 0
        firstName = ""
        lastName = ""
        city = ""
        state = ""
        emailAddress = ""
        phone = ""
        username = ""
        password = ""
        screenName = ""
        photo1 = ""
        community = ""
        latitude = 0.0
        longitude = 0.0
        online = NSNumber( value: false)
        availability1 = ""
        availability2 = ""
        availability3 = ""
        payvenmo = ""
        paybank = ""
        paypaypal = ""
        createdate = Date()
        status = NSNumber( value: false)
    }
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["id",
                                                "username",
                                                "password",
                                                "firstName",
                                                "lastName",
                                                "screenName",
                                                "photo1",
                                                "phone",
                                                "emailAddress",
                                                "availability1",
                                                "availability2",
                                                "availability3",
                                                "payvenmo",
                                                "paybank",
                                                "paypaypal",
                                                "city",
                                                "state",
                                                "community",
                                                "latitude",
                                                "longitude",
                                                "online",
                                                "createdate",
                                                "updatedate",
                                                "payCreditCardExpireDt",
                                                "payCreditCardNumber",
                                                "payCreditCardPin",
                                                "payCreditCardType",
                                                "status"])
            
            return mapping
        }else{
            return nil
        }
    }
    
    public func requestUserName() ->String{
        return username ?? ""
    }
    
    public func requestScreenName() ->String{
        return screenName ?? ""
    }
    
    public func requestUserID() ->Int{
        return id?.intValue ?? 0
    }
    
    public func updateUserID(userID:Int){
        self.id = NSNumber(value: userID)
    }
    
    public func updateFristName(firstName:String){
        self.firstName = firstName
    }
    
    public func requestFristName() ->String{
        return firstName ?? ""
    }
    
    public func updateLastName(lastName:String){
        self.lastName = lastName
    }
    
    public func requestLastName() ->String{
        return lastName ?? ""
    }
    
    public func requestCity() ->String{
        return city ?? ""
    }
    
    public func updateCity(city:String){
        self.city = city
    }
    
    public func requestState() ->String{
        return state ?? ""
    }
    public func updateState(state:String){
        self.state = state
    }
    
    public func requestEmailAddress() ->String{
        return emailAddress ?? ""
    }
    
    public  func updateEmailAddress(emailAddress:String){
        self.emailAddress = emailAddress
    }
    
    public func requestPhone() ->String{
        return phone ?? ""
    }
    
    public func updatePhone(phone:String){
        self.phone = phone
    }
    
    public func updateUserName(userName:String){
        self.username = userName
    }
    
    public func requestPassword() ->String{
        return password ?? ""
    }
    
    public  func updateUserPassword(userPassword:String){
        self.password = userPassword
    }
    
    
    public  func updateUserPassword(screenName:String){
        self.screenName = screenName
    }
    
    public func requestCoverphoto() ->String{
        return photo1 ?? ""
    }
    
    public  func updateCoverphoto(coverphoto:String){
        self.photo1 = coverphoto
    }
    
    
    public func requestCommunity() ->String{
        return community ?? ""
    }
    
    public  func updateCmmunity(community:String){
        self.screenName = community
    }
    
    
    public func requestLatitude() ->Double{
        return latitude?.doubleValue ?? 0.0
    }
    
    public  func updateLatituded(latitude:Double){
        self.latitude = NSNumber(value:latitude)
    }
    
    public func requestLongitude() ->Double{
        return longitude?.doubleValue ?? 0.0
    }
    
    public  func updateLongituded(longitude:Double){
        self.longitude = NSNumber(value:longitude)
    }
    
    public func requestLongitude() ->Int{
        return online?.intValue ?? 0
    }
    
    public  func updateLongituded(online:Double){
        self.online = NSNumber(value:online)
    }
    
    public func requestAvailability1() ->String{
        return availability1 ?? ""
    }
    
    public  func updateAvailability1(availability:String){
        self.availability1 = availability
    }
    
    
    public func requestAvailability2() ->String{
        return availability2 ?? ""
    }
    
    public  func updateAvailability2(availability:String){
        self.availability2 = availability
    }
    
    public func requestAvailability3() ->String{
        return availability3 ?? ""
    }
    
    public  func updateAvailability3(availability:String){
        self.availability3 = availability
    }
    
    public func requestPayVenmo() ->String{
        return payvenmo ?? ""
    }
    
    public  func updatePayVenmo(payvenmo:String){
        self.payvenmo = payvenmo
    }
    
    public func requestPayPaypal() ->String{
        return paypaypal ?? ""
    }
    
    public  func updatePayPaypal(paypaypal:String){
        self.paypaypal = paypaypal
    }
    
    public func requestUpdateDate() ->Date{
        return updatedate ?? Date()
    }
    
    public  func updateDate(date:Date){
        self.updatedate = date
    }
    
    public func requestStatus() ->Date{
        return updatedate ?? Date()
    }
    
    public  func updateStatus(status:Bool){
        self.status = NSNumber(value: status)
    }
}
