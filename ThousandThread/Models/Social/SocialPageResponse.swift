//
//  FollowByMePageResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/29/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class SocialPageResponse:NSObject{
    @objc var rowPerPage:NSNumber!
    @objc var page:NSNumber!
    @objc var totalRows:NSNumber!
    @objc var totalPages:NSNumber!
    @objc var prev:NSNumber!
    @objc var next:NSNumber!
    @objc var pageRows:[ItemPageRowResponse]!
    @objc var isEmpty:String!
    
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["rowPerPage",
                                                "page",
                                                "totalRows",
                                                "totalPages",
                                                "prev",
                                                "next",
                                                "isEmpty"])
            
            let relationShipMappping = RKRelationshipMapping(fromKeyPath: "pageRows", toKeyPath: "pageRows", with: ItemPageRowResponse().requestMapping())
            mapping.addPropertyMapping(relationShipMappping)
            
            return mapping
        }else{
            return nil
        }
    }
}
