//
//  ItemPageResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 3/1/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit


class ItemPageResponse:NSObject{
    @objc private var rowsPerPage:NSNumber!
    @objc private var page:NSNumber!
    @objc private var totalRows:NSNumber!
    @objc private var totalPages:NSNumber!
    @objc private var prev:NSNumber!
    @objc private var next:NSNumber!
    @objc private var pageRows:[ItemPageRowResponse]!
    @objc var isEmpty:String!
    
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["rowsPerPage",
                                                "page",
                                                "totalRows",
                                                "totalPages",
                                                "prev",
                                                "next",
                                                "isEmpty"])
            
            let relationShipMappping = RKRelationshipMapping(fromKeyPath: "pageRows", toKeyPath: "pageRows", with: ItemPageRowResponse().requestMapping())
            mapping.addPropertyMapping(relationShipMappping)
            
            return mapping
        }else{
            return nil
        }
    }
    
    public func requestNextPage()->Int
    {
        return next.intValue;
    }
    
    public func requestPageRowCount() -> Int{
        return pageRows.count
    }
    
    public func requestTotalPageCount() -> Int{
        return totalPages.intValue
    }
    
    public func totalItems() ->Int{
        return totalRows.intValue
    }
    
    public func requestItems() ->[ItemPageRowResponse]{
        return pageRows
    }
}




