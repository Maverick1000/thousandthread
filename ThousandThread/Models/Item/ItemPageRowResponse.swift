//
//  ItemPageRowResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 3/1/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class ItemPageRowResponse:NSObject{
    @objc private var id:NSNumber?
    @objc private var siteSub:Any?
    @objc private var user_id:NSNumber!
    @objc private var tradeType:String?
    @objc private var productGender:String?
    @objc private var category:String?
    @objc private var tag1:String?
    @objc private var tag2:String?
    @objc private var tag3:String?
    @objc private var title:String!
    @objc private var descript:String!
    @objc private var latitude:NSNumber!
    @objc private var longitude:NSNumber!
    @objc private var imagePath:String!
    @objc private var imagePaths:[String]!
    @objc private var statusPhoto:Any?
    @objc private var brand:String?
    @objc private var size:String?
    @objc private var priceType:Any?
    @objc private var cost:Any?
    @objc private var note:String?
    @objc private var pantSize:NSNumber?
    @objc private var shoeSize:NSNumber?
    @objc private var price:NSNumber!
    @objc private var priceSpecial:NSNumber?
    @objc private var priceRent:NSNumber?
    @objc private var priceUnit:NSNumber?
    @objc private var weight:Any?
    @objc private var reviewOpen:NSNumber!
    @objc private var reviewTotal:NSNumber!
    @objc private var reviewAverage:Any?
    @objc private var term:Any?
    @objc private var notAvailStartDt:Date?
    @objc private var notAvailEndDt:Date?
    @objc private var updatedDt:Date?
    @objc private var expiredDt:Date?
    @objc private var createdDt:Date?
    @objc private var status:Any?
    @objc private var pickupDt:Date?
    @objc private var friendDiscountRate:NSNumber?
    @objc private var username:String?
    @objc private var screenName:String?
    @objc private var userPhoto1:String?
    @objc private var street:String?
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from:["id":"id",
                                               "siteSub":"siteSub",
                                               "user_id":"user_id",
                                               "tradeType":"tradeType",
                                               "productGender":"productGender",
                                               "category":"category",
                                               "tag1":"tag1",
                                               "tag2":"tag2",
                                               "tag3":"tag3",
                                               "title":"title",
                                               "descript":"description",
                                               "latitude":"latitude",
                                               "longitude":"longitude",
                                               "imagePath":"imagePath",
                                               "imagePaths":"imagePaths",
                                               "statusPhoto":"statusPhoto",
                                               "brand":"brand",
                                               "size":"size",
                                               "priceType":"priceType",
                                               "cost":"cost",
                                               "price":"price",
                                               "pantSize":"pantSize",
                                               "shoeSize":"shoeSize",
                                               "priceSpecial":"priceSpecial",
                                               "priceUnit":"priceUnit",
                                               "priceRent":"priceRent",
                                               "weight":"weight",
                                               "reviewOpen":"reviewOpen",
                                               "reviewTotal":"reviewTotal",
                                               "reviewAverage":"reviewAverage",
                                               "term":"term",
                                               "note":"note",
                                               "notAvailStartDt":"notAvailStartDt",
                                               "notAvailEndDt":"notAvailEndDt",
                                               "updatedDt":"updatedDt",
                                               "expiredDt":"expiredDt",
                                               "createdDt":"createdDt",
                                               "pickupDt":"pickupDt",
                                               "status":"status",
                                               "username":"username",
                                               "friendDiscountRate":"friendDiscountRate",
                                               "screenName":"screenName",
                                               "userPhoto1":"userPhoto1",
                                               "street":"street"])
            
            return mapping
        }else{
            return nil
        }
    }
    
    public func reqeustItemID() ->Int{
        return id?.intValue ?? Int(truncating: NSNumber(value: 0))
    }
    
    public func requestItemTitle() ->String{
        return title ?? ""
    }
    
    public func requestDescription() ->String{
        return descript ?? ""
    }
    public func reqeustUserID() ->Int{
        return user_id?.intValue ?? Int(truncating: NSNumber(value: 0))
    }
    
    public func requestBrand() ->String{
        return brand ?? ""
    }
    
    public func requestCategory() ->String{
        return category ?? ""
    }

    public func requestSize() ->String{
        return size ?? ""
    }
    
    public func requestUserName() ->String{
        return username ?? ""
    }
    
    public func requestProductGender() ->String{
        return productGender ?? ""
    }
    
    public func reqeustUserPhotoPath() ->String{
        if let userPhoto1 = userPhoto1{
            return "/\(userPhoto1)"
        }
        return ""
    }
    
    public func requestTags() ->[String]{
        var temp = [String]()
        
        if (tag1 != nil) {
            temp.append(tag1!)
        }
        
        if (tag2 != nil) {
            temp.append(tag2!)
        }
        
        if(tag3 != nil) {
            temp.append(tag3!)
        }
        return temp
    }
    public func requestTag(tagNumber:Int) ->String{
        var temp = ""
        switch tagNumber {
        case 1:
            temp = tag1 ?? ""
        case 2:
            temp = tag2 ?? ""
        case 3:
            temp = tag3 ?? ""
  
        default:
            temp =  ""
        }
        
        return temp
    }
    
    public func requestPrice() ->NSNumber{
        return price == nil ?  NSNumber(value: 0.0): price
    }
    
    public func requestRentPrice() ->NSNumber{
        let temp = priceRent?.floatValue ?? 0.0;
        return NSNumber(value: temp )
    }
    
    func requestImagePath() ->String{
        if let itemImagePath = imagePath{
            return "/\(itemImagePath)"
        }
        return ""
    }
    
    func  requestImagePaths() ->[String]{
        if let itemImagePaths = imagePaths{
            return itemImagePaths
        }
        return []
    }
    
    func requestReviewAverage() -> Double {
        if let value = reviewAverage as? Double{
            return value
        }
        return 0.0
    }
    
    func requestReviewTotal() -> Int{
        return reviewTotal.intValue
    }
    
    func requestNotes() ->String {
        return note ?? ""
    }
    
    func requestNotAvailStartDt() -> Date?{
        return notAvailStartDt ?? nil
    }
    
    func requestNotAvailEndDt() -> Date?{
        return notAvailEndDt ?? nil
    }
}
//notAvailEndDt


/*
 
 {
 "id": 22,
 "siteSub": null,
 "user_id": 2,
 "cat": null,
 "title": "sample 22",
 "desc": "This is description of sample 22",
 "locLat": "37.78188300",
 "locLon": "-122.38761200",
 "photo1": "photo/item/2019/02/21/p22.jpg",
 "photo2": null,
 "photo3": null,
 "photo4": null,
 "photo5": null,
 "photo6": null,
 "photo7": null,
 "photo8": null,
 "photo9": null,
 "photo10": null,
 "statusPhoto": null,
 "priceType": null,
 "cost": 22,
 "price": 22,
 "priceUnit": null,
 "weight": null,
 "reviewOpen": 1,
 "reviewTotal": 0,
 "reviewAverage": null,
 "term": null,
 "expireDt": null,
 "submitDt": "2019-02-21T01:01:01.000Z",
 "status": null
 },
 */
