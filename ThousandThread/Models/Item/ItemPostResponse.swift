//
//  ItemPostResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/11/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class ItemPostResponse:NSObject{
    @objc private var id:NSNumber!
  
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["id"])
                                            
            return mapping
        }else{
            return nil
        }
    }
}
