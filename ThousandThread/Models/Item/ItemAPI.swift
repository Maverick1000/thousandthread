//
//  ItemAPI.swift
//  ThousandThread
//
//  Created by Paul Walker on 3/1/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class ItemAPI:RestKitManager{
    
    func getItem(path:String,completionHandler:@escaping(Bool,ItemPageRowResponse?) ->()){

        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: ItemPageResponse().requestMapping(), method: .GET, pathPattern: nil,
                                                               keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }

        manager.getObject(nil, path: path , parameters: nil, success: { (operation, result) in
            completionHandler(true, result?.array()?.first as? ItemPageRowResponse)
        }) { (operation, error) in
            completionHandler(false,nil)
        }
    }
    
    func getItems(path:String,completionHandler:@escaping(Bool,ItemPageResponse?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: ItemPageResponse().requestMapping(), method: .GET, pathPattern: nil,
                                                               keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.getObject(nil, path: path , parameters: nil, success: { (operation, result) in
            completionHandler(true, result?.array()?.first as? ItemPageResponse)
        }) { (operation, error) in
            completionHandler(false,nil)
        }
    }
    
    func postItem(profile:UserProfile, images:[Data], item:ItemPageRowResponse, completionHandler:@escaping(Bool) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: ItemPostResponse().requestMapping(),
                                                               method: .POST, pathPattern: nil, keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        let parameters:[String:Any] = [
            "user_id":NSNumber(value: profile.requestUserID()),
            "title":item.requestItemTitle(),
            "size":item.requestSize(),
            "tag1":item.requestTag(tagNumber: 1),
            "tag2":item.requestTag(tagNumber: 2),
            "tag3":item.requestTag(tagNumber: 3),
            "category":item.requestCategory,
            "note":item.requestNotes(),
            "priceRent":item.requestRentPrice(),
            "price":item.requestPrice(),
            "brand":item.requestBrand(),
            "productGender":item.requestProductGender(),
            "notAvailStartDt":item.requestNotAvailStartDt()!,
            "notAvailEndDt":item.requestNotAvailEndDt()!]
        
        let request = manager.multipartFormRequest(with:item, method: .POST, path: "/ads", parameters:parameters) { (AFMultipartFormData) in
            for(index,imageData) in images.enumerated(){
                AFMultipartFormData?.appendPart(withFileData: imageData, name: "uploadfiles",
                                                fileName: "image_\(index).jpg", mimeType: "image/jpg")
            }
        }
        
        let operation = manager.objectRequestOperation(with: request! as URLRequest, success: { (requestOperation, mappingResults) in
            //                if let mapResponse = mappingResults?.firstObject as? ImageDataResponse{
            //                    //profile.updateDate(date: mapResponse.requestUpdatedate())
            //                }
            completionHandler(true)
        }) { (requestOperation, error) in
            completionHandler(false)
        }
        manager.enqueue(operation)
    }
}


