//
//  PostMessage.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/31/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class PostMessage: NSObject {
    @objc var sender_id:NSNumber!
    @objc var recipient_id:NSNumber!
    @objc var type:String!
    @objc var ad_id:NSNumber!
    @objc var body:String?
    @objc var action:String!
    @objc var imageData:[Data]!
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["sender_id",
                                                "recipient_id",
                                                "type",
                                                "ad_id",
                                                "body",
                                                "action"])
            return mapping
        }else{
            return nil
        }
    }
    
    public func requsestDictionaryParams() -> [String:Any]{
        return ["sender_id":sender_id,"recipient_id":recipient_id,
                "type":type,"body":body ?? ""]
    }
}
