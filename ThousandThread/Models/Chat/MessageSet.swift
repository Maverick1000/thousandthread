//
// MessageSetResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 5/20/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class MessageSetResponse:NSObject{
    @objc private var rowPerPage:NSNumber!
    @objc private var page:NSNumber!
    @objc private var totalRows:NSNumber!
    @objc private var totalPages:NSNumber!
    @objc private var prev:NSNumber!
    @objc private var next:NSNumber!
    @objc private var pageRows:[PageRowResponse]!
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["rowPerPage",
                                                "page",
                                                "totalRows",
                                                "totalPages",
                                                "prev",
                                                "next"])
            
            let relationShipMappping = RKRelationshipMapping(fromKeyPath: "pageRows", toKeyPath: "pageRows", with: PageRowResponse().requestMapping())
            mapping.addPropertyMapping(relationShipMappping)
            
            return mapping
        }else{
            return nil
        }
    }
    
    public func requestNextPage()->Int
    {
        return next.intValue;
    }
    
    public func requestPageRowCount() -> Int{
        return pageRows.count
    }
    
    public func requestTotalPageCount() -> Int{
        return totalPages.intValue
    }
    
    public func totalItems() ->Int{
        return totalRows.intValue
    }
    
    public func requestItems() ->[PageRowResponse]{
        return pageRows
    }
    
}
