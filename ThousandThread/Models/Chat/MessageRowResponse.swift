//
//  MessageSetRowResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 5/20/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class MessageRowResponse:NSObject{
    @objc  var id:NSNumber!
    @objc  var siteSub:String?
    @objc  var user1_id:NSNumber?
    @objc  var user2_id:NSNumber?
    @objc  var ad_id:NSNumber?
    @objc  var type:String!
    @objc  var type_id:NSNumber!
    @objc  var user1Name:String?
    @objc  var user2Name:String?
    @objc  var user1Photo1:String?
    @objc  var user2Photo1:String?
    @objc  var createdDt:Date!
    @objc  var status:NSNumber?
    @objc  var lastMessageDt:Date!
    @objc  var action:String?
    @objc  var body:String?
    
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["id",
                                                "siteSub",
                                                "user1_id",
                                                "user2_id",
                                                "type",
                                                "type_id",
                                                "user1Name",
                                                "user1Photo1",
                                                "user2Name",
                                                "user2Photo1",
                                                "createdDt",
                                                "status",
                                                "body",
                                                "lastMessageDt",
                                                "action"])
            return mapping
        }else{
            return nil
        }
    }
    
    public func requestMessageSetID() ->Int{
        if let id = id {
            return id.intValue
        }
        return -1
    }
    
    public func requestADID() ->Int{
        if let id = ad_id{
            return id.intValue
        }
        return -1
    }
    
    public func requestUserOne() ->Int{
        if let id = user1_id {
            return id.intValue
        }
        return -1
    }
    
    public func requestUserName1() -> String{
        return user1Name ?? ""
    }
    
    public func requesUser1Photo1() -> String{
        return user1Photo1 ?? ""
    }
    
    public func requestUserName2() -> String{
        return user2Name ?? ""
    }
    
    public func requesUser2Photo1() -> String{
        return user2Photo1 ?? ""
    }
    
    public func requestBody() -> String{
        return body ?? ""
    }
    
    public func requestUserTwoID() ->Int{
        if let id = user2_id {
            return id.intValue
        }
        return -1
    }
    
    public func requestHowOldIsMessage() ->String{
        let calendar = Calendar.current
        let dateComponents = calendar.dateComponents([Calendar.Component.second],
                                                     from: lastMessageDt, to: Date() )
        
        var seconds = dateComponents.second ?? 0
        
        let days = seconds / ( 24 * 3600 )
        if days > 0 {
            return "\(days)d"
        }
        
        seconds = seconds % (24 * 36)
        let hours = seconds / 3600
        if days < 0 && hours > 0 {
            return "\(hours)h"
        }
        
        seconds = seconds / 60
        let minutes = seconds / 60
        if hours < 0 && minutes > 0 {
            return "\(minutes)m"
        }
        
        seconds = seconds % 60
        return "\(seconds)s"
    }
    
    public func requestUserOneID() ->Int{
        if let id = user1_id {
            return id.intValue
        }
        
        return -1
    }
}
