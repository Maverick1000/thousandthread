//
//  ChatMessageSetResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 7/31/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class ChatMessageSetResponse: NSObject {
    
    @objc var id:NSNumber!
    @objc var message_set_id:NSNumber!
    @objc var siteSub:NSNumber!
    @objc var sender_id:NSNumber!
    @objc var recipient_id:NSNumber!
    @objc var ad_id:NSNumber!
    @objc var body:String!
    @objc var createdDt:Date!
    @objc var notification_id:NSNumber!
    @objc var action:String!
    @objc var isRead:NSNumber!
    @objc var status:NSNumber!
    @objc var imagePaths:[String]!
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["id",
                                                "siteSub",
                                                "message_set_id",
                                                "sender_id",
                                                "recipient_id",
                                                "ad_id",
                                                "body",
                                                "createdDt",
                                                "notification_id",
                                                "isRead",
                                                "action",
                                                "status",
                                                "imagePaths"])
            return mapping
        }else{
            return nil
        }
    }
}
