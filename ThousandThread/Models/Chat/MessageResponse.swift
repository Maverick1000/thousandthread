//
// MessageSetResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 5/20/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class MessageResponse:NSObject{
    @objc private var rowsPerPage:NSNumber!
    @objc private var page:NSNumber!
    @objc private var totalRows:NSNumber!
    @objc private var totalPages:NSNumber!
    @objc private var prev:NSNumber!
    @objc private var next:NSNumber!
    @objc private var pageRows:[MessageRowResponse]!
    @objc var isEmpty:String!
    @objc var isError:String!
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["rowsPerPage",
                                                "page",
                                                "totalRows",
                                                "totalPages",
                                                "prev",
                                                "next",
                                                "isEmpty",
                                                "isError"])
            
            let relationShipMappping = RKRelationshipMapping(fromKeyPath: "pageRows", toKeyPath: "pageRows", with: MessageRowResponse().requestMapping())
            mapping.addPropertyMapping(relationShipMappping)
            
            return mapping
        }else{
            return nil
        }
    }
    
    public func requestNextPage()->Int
    {
        return next.intValue;
    }
    
    public func requestPageRowCount() -> Int{
        return pageRows.count
    }
    
    public func requestTotalPageCount() -> Int{
        return totalPages.intValue
    }
    
    public func totalItems() ->Int{
        return totalRows.intValue
    }
    
    public func requestItems() ->[MessageRowResponse]{
        return pageRows
    }
    
    //    public func requestMessageSetForUser(userProfile:UserProfile) ->Int{
    //        var messageSetID = -1
    //        for item in pageRows{
    //            if userProfile.requestUserID() == item.requestUserOne() ||
    //                userProfile.requestUserID() == item.requestUserTwo(){
    //                messageSetID = item.requestMessageSetID()
    //                break
    //            }
    //        }
    //        return messageSetID
    //    }
}
