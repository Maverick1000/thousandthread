//
//  NotificationResponse.swift
//  ThousandThread
//
//  Created by Paul Walker on 6/26/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class NotificationResponse:NSObject{
    @objc var total_notification: NSNumber!
    @objc var new_listing_requests: NSNumber!
    @objc var new_chat_messages: NSNumber!
    @objc var new_friend_requests: NSNumber!
    @objc var new_adreviews: NSNumber!
    @objc var new_admin_messages: NSNumber!
    @objc var new_order_status:NSNumber!
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["total notifications": "total_notification",
                                                "new listing requests":"new_listing_requests",
                                                "new chat messages":"new_chat_messages",
                                                "new admin messages":"new_admin_messages",
                                                "new friend requests":"new_friend_requests",
                                                "new ad reviews":"new_adreviews",
                                                "new order status":"new_order_status"])
            return mapping
        }else{
            return nil
        }
    }
    
    public func requestTotal() ->Int{
        return total_notification.intValue
    }
    
    public func requestNewListings() ->Int{
        return new_listing_requests.intValue
    }
    
    public func requestNewChatTotal() ->Int{
        return new_chat_messages.intValue
    }
    
    public func requestNewAdimTotal() ->Int{
        return new_admin_messages.intValue;
    }
    
    public func requestNewFriendsTotal() ->Int{
        return new_friend_requests.intValue
    }
    
    public func requestNewReviewsTotal() ->Int{
        return new_adreviews.intValue
    }
    
    public func requestNewOrderStatusTotal() ->Int{
        return new_order_status.intValue
    }
}

