//
//  UserAvailability.swift
//  ThousandThread
//
//  Created by Paul Walker on 2/10/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit

struct UserAvailability{
    var day:String!
    var startTime:String!
    var endTime:String!
    
    func requestStringEncoding() ->String{
        return day + "|" + startTime + "|" + endTime
    }
    
    static func == (lhs: UserAvailability, rhs: UserAvailability) -> Bool{
        return lhs.day == rhs.day && lhs.startTime == rhs.startTime && lhs.endTime == rhs.endTime
    }
    
}
