//
//  ReviewAPI.swift
//  ThousandThread
//
//  Created by Paul Walker on 4/22/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class ReviewAPI:RestKitManager{
    
    func getReviewComments(path:String,completionHandler:@escaping(Bool,[Review]?) ->()){
        
        if  let userResponseDescriptor =  RKResponseDescriptor(mapping: Review().requestMapping(), method: .GET, pathPattern: nil,
                                                               keyPath: nil, statusCodes: Utility.successStatusCode){
            addResponseDescriptor(descriptor: userResponseDescriptor)
        }
        
        manager.getObject(nil, path: path , parameters: nil, success: { (operation, result) in
            completionHandler(true, result?.array() as? [Review])
        }) { (operation, error) in
            completionHandler(false,nil)
        }
        
    }
}
