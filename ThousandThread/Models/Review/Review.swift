//
//  Review.swift
//  ThousandThread
//
//  Created by Paul Walker on 4/22/19.
//  Copyright © 2019 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

class Review:NSObject{
    @objc private var id:NSNumber!
    @objc private var ad_id:NSNumber!
    @objc private var rating:NSNumber!
    @objc private var userType:String?
    @objc private var user_id:NSNumber?
    @objc private var fullName:String?
    @objc private var emailAddress:String?
    @objc private var comment_parent_id:String?
    @objc private var comments:String?
    @objc private var createdDt:Date?
    @objc private var updatedDt:Date?
    @objc private var publish:NSNumber?
    @objc private var status:NSNumber?
    
    
    public func requestMapping() ->RKObjectMapping?{
        if let mapping = RKObjectMapping(for: self.classForCoder) {
            mapping.addAttributeMappings(from: ["id",
                                                "ad_id",
                                                "rating",
                                                "userType",
                                                "user_id",
                                                "fullName",
                                                "emailAddress",
                                                "comment_parent_id",
                                                "comments",
                                                "createdDt",
                                                "publish",
                                                "status"])
            return mapping
        }else{
            return nil
        }
    }
    
    public func requestRating() ->Double{
      return rating.doubleValue
    }
    
    public func requestComments() ->String{
        return comments ?? ""
    }
}
